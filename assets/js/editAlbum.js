/**
 * File : editInstagramTag.js
 *
 * This file contain the validation of edit instagram tag form
 *
 * @author Kamran Khan
 */
$(document).ready(function(){

    var editAlbumForm = $("#editAlbum");

    var validator = editAlbumForm.validate({

        rules:{
            file_name: {
                required: true,
                accept: "image/jpeg, image/pjpeg, image/png, image/gif"
            },
            title :{ required : true },
            description :{ required : true },
            //file_name :{ required : true },
        },
        messages:{
            title :{ required : "Please enter title" },
            description :{ required : "Please enter description" },
            file_name :{ required : "Please select an album image" }
        }
    });
});