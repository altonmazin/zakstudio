/**
 * File : addVideo.js
 *
 * This file contain the validation of add video form
 *
 * Using validation plugin : jquery.validate.js
 *
 * @author Kamran Khan
 */

$(document).ready(function(){

    var addLogoForm = $("#addVideo");

    var validator = addLogoForm.validate({

        rules:{
             url :{ required : true },
            // title :{ required : true },
            // description :{ required : true },

        },
        messages:{
              url :{ required : "Please enter URL" },
            //  description :{ required : "Please enter description" },
        }
    });
});
