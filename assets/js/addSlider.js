/**
 * File : editSliderImage.js
 *
 * This file contain the validation of edit Slider Image form
 *
 * @author Kamran Khan
 */

var imgWidth = false;
function GetFileSize() {
    document.getElementById('fileInfo').innerHTML = '';
    var fi = '';
    imgWidth = false;
    fi = document.getElementById('upload_files');
    if (fi.files.length > 0) {
        for (var i = 0; i <= fi.files.length - 1; i++) {
            var fileName, fileExtension, fileSize, fileType, dateModified;
            fileName = fi.files.item(i).name;
            fileExtension = fileName.replace(/^.*\./, '');
            if (fileExtension == 'png' || fileExtension == 'jpg' || fileExtension == 'jpeg')
            {
                readImageFile(fi.files.item(i));
            }

        }
        function readImageFile(file) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = new Image();
                img.src = e.target.result;
                img.onload = function () {
                    var w = this.width;
                    var h = this.height;

                    if(w < 600 || h < 400) {
                        document.getElementById('fileInfo').innerHTML = "Invalid width or height, file upload failed."
                        imgWidth = true;
                    }

                }
            };
            reader.readAsDataURL(file);
        }
    }
}



$(document).ready(function(){

    $(function() {
        $.validator.addMethod(
            "validate_file_type",
            function(val,elem) {
             //   console.log(elem.id);
                var files    =   $('#'+elem.id)[0].files;
             //   console.log(files);
                for(var i=0;i<files.length;i++){
                    var fname = files[i].name.toLowerCase();
                    var re = /(\.png|\.jpg|\.jpeg)$/i;
                    if(!re.exec(fname))
                    {
                      //  console.log("File extension not supported!");
                        return false;
                    }
                }
                return true;
            },
            "Please upload png or jpg or jpeg files"
        );
        $.validator.addMethod(
            "validate_file_size",
            function(val,elem) {
               // console.log(elem.id);
                var files    =   $('#'+elem.id)[0].files;
               // console.log(files);
                var sliderDesign = $("#sliderDesign option:selected").val();
               
                if(sliderDesign == 'Single Image') {
                    if(files.length < 1){
                        return false;
                    }
                }
                else {
                       if(files.length < 3){
                        return false;
                    } 
                }
                

                return true;
            },
            "Please upload slider images according to slider design,single Image design needs only one image other needs 3 images"
        );
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("form[name='formname']").validate({
            // Specify validation rules
            rules: {
                'uploaded_files[]':{
                    required: true,
                    validate_file_type : true,
                    validate_file_size : true
                }
            },
            // Specify validation error messages
            messages: {

                'uploaded_files[]':{
                    required : "Please upload slider images according to slider design",
                    /*accept:"Please upload .docx or .pdf files"*/
                }
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                if(imgWidth == false) {
                    $('#submitForm').hide();
                    $("#loading").show();
                    form.submit();
                } else {
                    return false;
                }

            }
        });

    });
});