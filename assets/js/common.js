/**
 * @author Kishor Mali
 */


jQuery(document).ready(function(){
	
	jQuery(document).on("click", ".deleteUser", function(){
		var userId = $(this).data("userid"),
			hitURL = baseURL + "admin/deleteUser",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this user ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { userId : userId } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("User successfully deleted"); }
				else if(data.status = false) { alert("User deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});

    jQuery(document).on("click", ".deleteTag", function(){
        var tagId = $(this).data("tagid"),
            hitURL = baseURL + "admin/deleteInstagramTag",
            currentRow = $(this);

        var confirmation = confirm("Are you sure to delete this instagram tag ?");

        if(confirmation)
        {
            jQuery.ajax({
                type : "POST",
                dataType : "json",
                url : hitURL,
                data : { tagId : tagId }
            }).done(function(data){
                console.log(data);
                currentRow.parents('tr').remove();
                if(data.status = true) { alert("Tag successfully deleted"); }
                else if(data.status = false) { alert("Tag deletion failed"); }
                else { alert("Access denied..!"); }
            });
        }
    });

    jQuery(document).on("click", ".deleteAlbum", function(){
        var albumId = $(this).data("albumid"),
            hitURL = baseURL + "admin/deleteAlbum",
            currentRow = $(this);

        var confirmation = confirm("Are you sure to delete this album ?");

        if(confirmation)
        {
            jQuery.ajax({
                type : "POST",
                dataType : "json",
                url : hitURL,
                data : { albumId : albumId }
            }).done(function(data){
                console.log(data);
                currentRow.parents('tr').remove();
                if(data.status = true) { alert("Album successfully deleted"); }
                else if(data.status = false) { alert("Album deletion failed"); }
                else { alert("Access denied..!"); }
            });
        }
    });

    jQuery(document).on("click", ".deleteLogo", function(){
        var logoId = $(this).data("logoid"),
            hitURL = baseURL + "admin/deleteLogo",
            currentRow = $(this);

        var confirmation = confirm("Are you sure to delete this logo ?");

        if(confirmation)
        {
            jQuery.ajax({
                type : "POST",
                dataType : "json",
                url : hitURL,
                data : { logoId : logoId }
            }).done(function(data){
                console.log(data);
                currentRow.parents('tr').remove();
                if(data.status = true) { alert("Logo successfully deleted"); }
                else if(data.status = false) { alert("Logo deletion failed"); }
                else { alert("Access denied..!"); }
            });
        }
    });

    jQuery(document).on("click", ".deleteVideo", function(){
        var videoId = $(this).data("videoid"),
            hitURL = baseURL + "admin/deleteVideo",
            currentRow = $(this);

        var confirmation = confirm("Are you sure to delete this video ?");

        if(confirmation)
        {
            jQuery.ajax({
                type : "POST",
                dataType : "json",
                url : hitURL,
                data : { videoId : videoId }
            }).done(function(data){
                console.log(data);
                currentRow.parents('tr').remove();
                if(data.status = true) { alert("Video successfully deleted"); }
                else if(data.status = false) { alert("Video deletion failed"); }
                else { alert("Access denied..!"); }
            });
        }
    });

    jQuery(document).on("click", ".deleteSlider", function(){
        var sliderId = $(this).data("sliderid"),
            hitURL = baseURL + "admin/deleteSlider",
            currentRow = $(this);

        var confirmation = confirm("Are you sure to delete this slider ?");

        if(confirmation)
        {
            jQuery.ajax({
                type : "POST",
                dataType : "json",
                url : hitURL,
                data : { sliderId : sliderId }
            }).done(function(data){
                console.log(data);
                currentRow.parents('tr').remove();
                if(data.status = true) { alert("Slider successfully deleted"); }
                else if(data.status = false) { alert("Slider deletion failed"); }
                else { alert("Access denied..!"); }
            });
        }
    });

    jQuery(document).on("click", ".disableSlider", function(){

        var currentObj = $(this);
        console.log($(this));
        var sliderId = $(this).data("sliderid"),
            hitURL = baseURL + "admin/disableSlider",
            currentRow = $(this);

        var confirmation = confirm("Are you sure to disable this slider ?");

        if(confirmation)
        {
            jQuery.ajax({
                type : "POST",
                dataType : "json",
                url : hitURL,
                data : { sliderId : sliderId }
            }).done(function(data){
                console.log(data);
             //   currentRow.parents('tr').remove();
                if(data.status = true) {
                    currentObj.removeClass('btn-danger disableSlider'); // Flag it with a new class
                    currentObj.addClass('btn-success enableSlider');
                    currentObj.text('Enable Slider');
                    alert("Slider successfully disabled"); }
                else if(data.status = false) { alert("Slider update failed"); }
                else { alert("Access denied..!"); }
            });
        }
    });

    jQuery(document).on("click", ".enableSlider", function(){

        var currentObj = $(this);
        console.log($(this));
        var sliderId = $(this).data("sliderid"),
            hitURL = baseURL + "admin/enableSlider",
            currentRow = $(this);

        var confirmation = confirm("Are you sure to enable this slider ?");

        if(confirmation)
        {
            jQuery.ajax({
                type : "POST",
                dataType : "json",
                url : hitURL,
                data : { sliderId : sliderId }
            }).done(function(data){
                console.log(data);
              //  currentRow.parents('tr').remove();
                if(data.status = true) {
                    currentObj.removeClass('btn-success enableSlider'); // Flag it with a new class
                    currentObj.addClass('btn-danger disableSlider');
                    currentObj.text('Disable Slider');
                    alert("Slider successfully enabled"); }
                else if(data.status = false) { alert("Slider update failed"); }
                else { alert("Access denied..!"); }
            });
        }
    });

    jQuery(document).on("click", ".searchList", function(){
		
	});
	
});
