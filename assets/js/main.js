/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

$(function () {
    'use strict';
    var albumId = $('#albumId').val();
    // Initialize the jQuery File Upload widget:
    $('#fileupload').bind('fileuploadadd', function(e,data) {

        var currentfiles = [];
        $(this).fileupload('option').filesContainer.children().each(function(){
            currentfiles.push($.trim($('.name', this).text()));
        });

        data.files = $.map(data.files, function(file,i){
            if ($.inArray(file.name,currentfiles) >= 0) {
                alert("The image file "+file.name+" is already uploaded, please select another image");
                return null;
            }
            return file;
        });
        $('.fileupload-process').hide();

    });
    $('#fileupload').fileupload({


        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},

        url: 'http://www.flexbittech.com/zak17/zak/admin/upload/json/'+ albumId
       // url: 'http://localhost/cms/admin/upload/json/'+ albumId
    });
    // Load existing files:
    $('#fileupload').addClass('fileupload-processing');
    $.ajax({

        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: $('#fileupload').fileupload('option', 'url'),
        dataType: 'json',
        context: $('#fileupload')[0]
    }).always(function () {
        $(this).removeClass('fileupload-processing');
    }).done(function (result) {
       // console.log(result);
        $(this).fileupload('option', 'done')
            .call(this, $.Event('done'), {result: result});
    });
});
