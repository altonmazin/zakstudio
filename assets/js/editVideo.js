/**
 * File : editLogo.js
 *
 * This file contain the validation of edit video form
 *
 * @author Kamran Khan
 */
$(document).ready(function(){

    var editVideoForm = $("#editVideo");

    var validator = editVideoForm.validate({

        rules:{
             url :{ required : true },
            // title :{ required : true },
            // description :{ required : true },
            //file_name :{ required : true },
        },
        messages:{
             url :{ required : "Please enter URL" },
        }
    });
});