/**
 * File : addInstagramTag.js
 *
 * This file contain the validation of add instagram Tag form
 *
 * Using validation plugin : jquery.validate.js
 *
 * @author Kamran Khan
 */

$(document).ready(function(){

    var addAlbumForm = $("#addAlbum");

    var validator = addAlbumForm.validate({

        rules:{
            title :{ required : true },
            description :{ required : true },
            file_name :{ required : true },
        },
        messages:{
            title :{ required : "Please enter title" },
            description :{ required : "Please enter description" },
            file :{ required : "Please enter file" }
        }
    });
});
