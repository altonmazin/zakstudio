/**
 * File : editInstagramTag.js
 *
 * This file contain the validation of edit instagram tag form
 *
 * @author Kamran Khan
 */
$(document).ready(function(){

    var editInstagramTagForm = $("#editInstagramTag");

    var validator = editInstagramTagForm.validate({

        rules:{
            tag :{ required : true }
        },
        messages:{
            tag :{ required : "Please enter valid instagram Tag" }
        }
    });
});