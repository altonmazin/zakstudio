/**
 * File : addInstagramTag.js
 *
 * This file contain the validation of add instagram Tag form
 *
 * Using validation plugin : jquery.validate.js
 *
 * @author Kamran Khan
 */

$(document).ready(function(){

    var addInstagramForm = $("#addInstagramTag");

    var validator = addInstagramForm.validate({

        rules:{
            tag :{ required : true }
        },
        messages:{
            tag :{ required : "Please enter valid instagram Tag" }
        }
    });
});
