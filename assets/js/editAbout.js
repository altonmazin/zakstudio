/**
 * File : editInstagramTag.js
 *
 * This file contain the validation of edit instagram tag form
 *
 * @author Kamran Khan
 */
$(document).ready(function(){

    var editAboutForm = $("#editAbout");

    $('select').on('change', function() {
        if(this.value == '1'){
            $('#top_text').attr("value", "");
            $('#title_text').attr("value", "");
            $('#button_text').attr("value", "");
            $('#button_url').attr("value", "");
            $('#description_text').text('');
        }
    });


    var validator = editAboutForm.validate({

        /*rules:{
          //  top_text :{ required : true },
         //   title_text :{ required : true },
        //    button_text :{ required : true },
        //    description_text :{ required : true },

            /!*file_name: {
                required: true,
                accept: "image/jpeg, image/pjpeg, image/png, image/gif"
            },*!/
            //file_name :{ required : true },
        },*/
        /*messages:{
            top_text :{ required : "Please enter top text" },
            title_text :{ required : "Please enter title text" },
            button_text :{ required : "Please enter button text" },
            description_text :{ required : "Please enter description text" },
            file_name :{ required : "Please select an about section image" }
        }*/
    });
});