/**
 * File : editLogo.js
 *
 * This file contain the validation of edit logo form
 *
 * @author Kamran Khan
 */
$(document).ready(function(){

    var editLogoForm = $("#editLogo");

    var validator = editLogoForm.validate({

        rules:{
            file_name: {
                required: true,
                accept: "image/jpeg, image/pjpeg, image/png, image/gif"
            },
            // url :{ required : true },
           // title :{ required : true },
           // description :{ required : true },
            //file_name :{ required : true },
        },
        messages:{
           // title :{ required : "Please enter title" },
          //  description :{ required : "Please enter description" },
            file_name :{ required : "Please select an logo image" }
        }
    });
});