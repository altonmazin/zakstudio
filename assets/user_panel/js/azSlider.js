var myPlayerState = 2;
var widthSlider = $(window).width();


$(document).ready(function () {
    function projectslider(id) {
        if ($(window).width() > 1280) {
            widthSlider = 800;
        } else if ($(window).width() > 768) {
            widthSlider = $(window).width() * 0.6;
        } else if ($(window).width() > 480) {
            widthSlider = $(window).width() * 0.6;
        } else {
            widthSlider = $(window).width();
        }

        $('.azSlider #' + id + ' .content .container .items ul li').css('width', widthSlider);
        var value = ($(window).width() - ( widthSlider + 30) * 3) / 2;

        //carosal
        $('.azSlider #' + id + ' .content .container .items ul').css("transform", 'translateX( ' + value + 'px)');
        var n = $('.azSlider #' + id + ' .content .container .items ul li').length;

        if (!$('.azSlider #' + id).hasClass('fixed')) {
            var interval;
            var timer = function () {
                interval = setInterval(function () {
                    if (myPlayerState == 2) {
                        $('.azSlider #' + id + ' .content .container .items .rightarrow').trigger('click');
                    }
                }, 5000);
            };
            timer();

            $('.azSlider #' + id + ' .content .container .items .rightarrow').click(function () {
                if (myPlayerState == 2) {
                    clearInterval(interval);
                    timer();

                    var number = parseInt($('.azSlider #' + id + ' .content .container .items ul li.active').attr('data-no')) + 1;

                    if (number == n + 1) {
                        number = 1;
                        $('.azSlider #' + id + ' .content .container .items ul li').removeClass('active');
                        $('.azSlider #' + id + ' .content .container .items ul li:first-child').addClass('active');
                        var x = value - ((number - 2) * (widthSlider + 30));
                        $('.azSlider #' + id + ' .content .container .items ul').css("transform", 'translateX( ' + x + 'px)');

                    } else {
                        $('.azSlider #' + id + ' .content .container .items ul li.active').next().addClass('active');
                        $('.azSlider #' + id + ' .content .container .items ul li.active').prev().removeClass('active');

                        var x = value - ((number - 2) * (widthSlider + 30));


                        $('.azSlider #' + id + ' .content .container .items ul').css("transform", 'translateX( ' + x + 'px)');
                    }
                }
            });

            $('.azSlider #' + id + ' .content .container .items .leftarrow').click(function () {
                if (myPlayerState == 2) {
                    clearInterval(interval);
                    timer();

                    var number = parseInt($('.azSlider #' + id + ' .content .container .items ul li.active').attr('data-no')) - 1;


                    if (number == 0) {
                        number = n;

                        $('.azSlider #' + id + ' .content .container .items ul li').removeClass('active');
                        $('.azSlider #' + id + ' .content .container .items ul li:last-child').addClass('active');

                        // var x = value - ((number - 2)*835);

                        var x = value - ((number - 2) * (widthSlider + 30));

                        $('.azSlider #' + id + ' .content .container .items ul').css("transform", 'translateX( ' + x + 'px)');

                        $('.azSlider #' + id + ' .content .container .items ul').css({
                            '-webkit-transform': 'translateX( ' + x + 'px)',
                            '-moz-transform': 'translateX( ' + x + 'px)',
                            '-o-transform': 'translateX( ' + x + 'px)',
                            'transform': 'translateX( ' + x + 'px)'
                        });


                    } else {

                        $('.azSlider #' + id + ' .content .container .items ul li.active').prev().addClass('active');
                        $('.azSlider #' + id + ' .content .container .items ul li.active').next().removeClass('active');

                        //var x = value - ((number - 2)*835);

                        var x = value - ((number - 2) * (widthSlider + 30));

                        $('.azSlider #' + id + ' .content .container .items ul').css({
                            '-webkit-transform': 'translateX( ' + x + 'px)',
                            '-moz-transform': 'translateX( ' + x + 'px)',
                            '-o-transform': 'translateX( ' + x + 'px)',
                            'transform': 'translateX( ' + x + 'px)'
                        });
                    }
                }
            });
        }


    }

    projectslider('projects');
    // tech slider
    no = $('.techslider .slider ul li').length;
    $('.techslider .slider ul').css('width', no * ($('.techslider .slider ul li').outerWidth(true) + 2) + 20);

    if ($(window).width() < $('.techslider .slider ul').width()) {
        i = 0;
        setInterval(function () {
            var t = -(i * ($('.techslider .slider ul li').outerWidth(true) + 2));
            $('.techslider .slider ul').css({
                transform: 'translateX( ' + t + 'px)',
                MozTransform: 'translateX( ' + t + 'px)',
                WebkitTransform: 'translateX( ' + t + 'px)',
                msTransform: 'translateX( ' + t + 'px)'
            });

            i++;

            if ($(window).width() - t > $('.techslider .slider ul').width()) {
                i = 0;
            }
        }, 2000);

    }
    $('header .menu ul li a').click(function () {

        $('#nav-icon2').removeClass('open');

        $('header .head').removeClass('mobileactive');

    });


});

var tag = document.createElement('script');
tag.src = "http://www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

function onYouTubePlayerAPIReady() {
    var players = document.querySelectorAll('.youTubeVideoContainer')
    for (var i = 0; i < players.length; i++) {
        new YT.Player(players[i], {
            playerVars: {
                'autoplay': 0,
                'modestbranding': 1
            },
            height: '100%',
            width: '100%',
            videoId: players[i].dataset.id,
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });
    }
}


function onPlayerReady() {

}
function onPlayerStateChange(event) {
    myPlayerState = event.data;
}

var rtime;
var timeout = false;
var delta = 500;
$(window).resize(function () {
    rtime = new Date();
    if (timeout === false) {
        timeout = true;
        setTimeout(resizeend, delta);
    }
});

function resizeend() {
    if (new Date() - rtime < delta) {
        setTimeout(resizeend, delta);
    } else {
        timeout = false;
        if ($(window).width() > 1280) {
            widthSlider = 800;
        } else if ($(window).width() > 768) {
            widthSlider = $(window).width() * 0.6;
        } else if ($(window).width() > 480) {
            widthSlider = $(window).width() * 0.6;
        } else {
            widthSlider = $(window).width();
        }

        $('.azSlider #projects' + ' .content .container .items ul li').css('width', widthSlider);
        var value = ($(window).width() - ( widthSlider + 30) * 3) / 2;

        //carosal
        $('.azSlider #projects' + ' .content .container .items ul').css("transform", 'translateX( ' + value + 'px)');
    }
}