$( document ).ready(function() {
	$('.navigation a').click(function(){
		var $el = $($(this).attr('href'));
		var destination = $el.offset().top;
	    $('body, html').animate({ scrollTop: destination }, 1100);
	    return false;
	});
});