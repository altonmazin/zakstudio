<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

//$route['default_controller'] = "login";
$route['default_controller'] = "home";
$route['404_override'] = 'error';


/*********** USER DEFINED ROUTES *******************/

/////////// START NEW ROUTES ///////////////////

$route['admin'] = 'admin/login';
$route['login'] = 'admin/login';
$route['admin/loginMe'] = 'admin/login/loginMe';
$route['admin/dashboard'] = 'admin/user';
$route['admin/logout'] = 'admin/user/logout';
$route['admin/userListing'] = 'admin/user/userListing';
$route['admin/userListing/(:num)'] = "admin/user/userListing/$1";
$route['admin/addNew'] = "admin/user/addNew";



$route['admin/addNewUser'] = "admin/user/addNewUser";
$route['admin/editOld'] = "admin/user/editOld";
$route['admin/editOld/(:num)'] = "admin/user/editOld/$1";
$route['admin/editUser'] = "admin/user/editUser";
$route['admin/deleteUser'] = "admin/user/deleteUser";
////// Instagram Tags Routing Start Here ////////////
$route['admin/instagramListing'] = 'admin/instagram/instagramListing';
$route['admin/instagramListing/(:num)'] = "admin/instagram/instagramListing/$1";
$route['admin/addInstagramTag'] = "admin/instagram/addInstagramTag";
$route['admin/processAddInstagramTag'] = "admin/instagram/processAddInstagramTag";

$route['admin/editInstagramTag'] = "admin/instagram/editInstagramTag";
$route['admin/editInstagramTag/(:num)'] = "admin/instagram/editInstagramTag/$1";
$route['admin/editprocessInstagramTag'] = "admin/instagram/editprocessInstagramTag";
$route['admin/deleteInstagramTag'] = "admin/instagram/deleteInstagramTag";

////// Instagram Tags Routing End Here ////////////


////// Album Routing Start Here ////////////
$route['admin/albumListing'] = 'admin/album/albumListing';
$route['admin/albumListing/(:num)'] = "admin/album/albumListing/$1";
$route['admin/addAlbum'] = "admin/album/addAlbum";
$route['admin/processAddAlbum'] = "admin/album/processAddAlbum";

$route['admin/editAlbum'] = "admin/album/editAlbum";
$route['admin/editAlbum/(:num)'] = "admin/album/editAlbum/$1";
$route['admin/editprocessAlbum'] = "admin/album/editprocessAlbum";
$route['admin/deleteAlbum'] = "admin/album/deleteAlbum";

////// Album Routing End Here ////////////

////// Slider Routing Start Here ////////////
$route['admin/sliderListing'] = 'admin/slider/sliderListing';
$route['admin/sliderListing/(:num)'] = "admin/slider/sliderListing/$1";
$route['admin/addSlider'] = "admin/slider/addSlider";
$route['admin/processAddSlider'] = "admin/slider/processAddSlider";
$route['admin/deleteSlider'] = "admin/slider/deleteSlider";

$route['testSlider/(:num)'] = "home/testSlider/$1";

$route['admin/editSlider'] = "admin/slider/editSlider";
$route['admin/editSlider/(:num)'] = "admin/slider/editSlider/$1";
$route['admin/editprocessSlider'] = "admin/slider/editprocessSlider";
$route['admin/enableSlider'] = "admin/slider/enableSlider";
$route['admin/disableSlider'] = "admin/slider/disableSlider";

////// Slider Routing End Here ////////////


////// Slider Images Routing Start Here ////////////
$route['admin/sliderImageListing'] = 'admin/sliderImage/sliderImageListing';
$route['admin/sliderImageListing/(:num)'] = "admin/sliderImage/sliderImageListing/$1";
$route['admin/addSliderImage'] = "admin/sliderImage/addSliderImage";
$route['admin/processAddSliderImage'] = "admin/sliderImage/processAddSliderImage";

$route['admin/editSliderImage'] = "admin/sliderImage/editSliderImage";
$route['admin/editSliderImage/(:num)'] = "admin/sliderImage/editSliderImage/$1";
$route['admin/editprocessSliderImage'] = "admin/sliderImage/editprocessSliderImage";
$route['admin/deleteSliderImage'] = "admin/sliderImage/deleteSliderImage";

////// Slider Images Routing End Here ////////////

///////// Album Image Upload Routing Start Here /////
$route['admin/upload/'] = "admin/upload/index";
$route['admin/upload/do_upload'] = "admin/upload/do_upload";
$route['admin/uploadAlbumImages/upload/index'] = "admin/upload";
$route['admin/uploadAlbumImages'] = "admin/upload/index";
$route['admin/uploadAlbumImages/(:num)'] = "admin/upload/index/$1";
$route['admin/upload/json/(:num)'] = "admin/upload/json/$1";


///////// Album Image Upload Routing End Here /////


///////// Logo Image Upload Routing Start Here /////
$route['admin/logoListing'] = 'admin/logo/logoListing';
$route['admin/logoListing/(:num)'] = "admin/logo/logoListing/$1";
$route['admin/addLogo'] = "admin/logo/addLogo";
$route['admin/processAddLogo'] = "admin/logo/processAddLogo";

$route['admin/editLogo'] = "admin/logo/editLogo";
$route['admin/editLogo/(:num)'] = "admin/logo/editLogo/$1";
$route['admin/editprocessLogo'] = "admin/logo/editprocessLogo";
$route['admin/deleteLogo'] = "admin/logo/deleteLogo";
///////// Logo Image Upload Routing End Here /////
///

///////// Video  Upload Routing Start Here /////
$route['admin/videoListing'] = 'admin/video/videoListing';
$route['admin/videoListing/(:num)'] = "admin/video/videoListing/$1";
$route['admin/addVideo'] = "admin/video/addVideo";
$route['admin/processAddVideo'] = "admin/video/processAddVideo";

$route['admin/editVideo'] = "admin/video/editVideo";
$route['admin/editVideo/(:num)'] = "admin/video/editVideo/$1";
$route['admin/editprocessVideo'] = "admin/video/editprocessVideo";
$route['admin/deleteVideo'] = "admin/video/deleteVideo";
///////// Logo Image Upload Routing End Here /////
///
///////// Page CMS Section Routing Start Here /////
$route['admin/pageListing'] = 'admin/page/pageListing';

$route['admin/editAbout'] = "admin/about/editAbout";
$route['admin/editprocessAbout'] = "admin/about/editprocessAbout";

$route['admin/editReady'] = "admin/ready/editReady";
$route['admin/editprocessReady'] = "admin/ready/editprocessReady";

///////// Logo Image Upload Routing End Here /////
///
///
$route['admin/loadChangePass'] = "admin/user/loadChangePass";
$route['admin/changePassword'] = "admin/user/changePassword";
$route['admin/pageNotFound'] = "admin/user/pageNotFound";
$route['admin/checkEmailExists'] = "admin/user/checkEmailExists";

$route['admin/forgotPassword'] = "admin/login/forgotPassword";
$route['admin/resetPasswordUser'] = "admin/login/resetPasswordUser";
$route['admin/resetPasswordConfirmUser'] = "admin/login/resetPasswordConfirmUser";
$route['admin/resetPasswordConfirmUser/(:any)'] = "admin/login/resetPasswordConfirmUser/$1";
$route['admin/resetPasswordConfirmUser/(:any)/(:any)'] = "admin/login/resetPasswordConfirmUser/$1/$2";
$route['admin/createPasswordUser'] = "admin/login/createPasswordUser";

/////////// END NEW ROUTES ///////////////////


/* End of file routes.php */
/* Location: ./application/config/routes.php */