<?php

$aboutId = '';
$top_text = '';
$button_text = '';
$button_url = '';
$hide_text = '';
$title_text = '';
$description_text = '';



if(!empty($aboutInfo))
{
    foreach ($aboutInfo as $about)
    {
        $aboutId = $about->aboutId;
        $title_text = $about->title_text;
        $button_text = $about->button_text;
        $button_url = $about->button_url;
        $hide_text = $about->hide_text;
        $description_text = $about->description_text;
        $top_text = $about->top_text;
    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> About Section Management
            <small>Add / Edit About Section </small>
        </h1>
    </section>

    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->



                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter About Section Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->

                    <form role="form" action="<?php echo base_url() ?>admin/editprocessAbout" enctype="multipart/form-data" method="post" id="editAbout" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Enter Top Text</label>
                                        <input type="text" class="form-control" value="<?php echo $top_text; ?>" id="top_text" name="top_text" >
                                        <input type="hidden" value="<?php echo $aboutId; ?>" name="aboutId" id="aboutId" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Enter Title Text</label>
                                        <input type="text" class="form-control" value="<?php echo $title_text; ?>" id="title_text" name="title_text" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Enter Button Text</label>
                                        <input type="text" class="form-control" value="<?php echo $button_text; ?>" id="button_text" name="button_text" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Enter Button URL</label>
                                        <input type="text" class="form-control" value="<?php echo $button_url; ?>" id="button_url" name="button_url" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Show Image Only (Hide Everything Else)</label>
                                        <select class="form-control" name="hide_text" id="hide_text">
                                            <option value="0" <?php echo ($hide_text == 0) ? 'selected': '';?>>No</option>
                                            <option value="1" <?php echo ($hide_text == 1) ? 'selected': '';?>>Yes</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Enter Description Text</label>
                                        <textarea placeholder="Enter Section Description" name="description_text" id="description_text" rows="5" cols="30" class="form-control"><?php echo $description_text;?></textarea>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Select About Image</label>
                                        <input type="file" class="form-control" id="file_name" name="file_name">
                                    </div>

                                </div>
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="button" class="btn btn-default" value="Cancel" onclick="document.location.href='<?php echo base_url();?>admin/pageListing/'" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                $this->load->helper('form');
                $error = $this->session->flashdata('error');
                if($error)
                {
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php } ?>
                <?php
                $success = $this->session->flashdata('success');
                if($success)
                {
                    ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url(); ?>assets/js/editAbout.js" type="text/javascript"></script>
