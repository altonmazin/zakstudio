<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Slider Image Management
            <small>Upload Slider Images, Delete</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>admin/sliderListing"><i class="fa fa-plus"></i> Manage Sliders</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Slider Image List</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Slider Image Date</th>
                                <th>Slider Image</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            <?php
                            if(!empty($sliderImageRecords))
                            {
                                foreach($sliderImageRecords as $record)
                                {
                                    ?>
                                    <tr>
                                        <td><?php echo $record->imgId; ?></td>
                                        <td><?php echo $record->createdDtm; ?></td>
                                        <td><img width="60" height="60" src="<?php echo base_url().'sliderUploads/'.$record->file_name; ?>"></td>
                                        <td class="text-center">
                                            <a class="btn btn-sm btn-info" href="<?php echo base_url().'admin/editSliderImage/'.$record->imgId; ?>"><i class="fa fa-pencil"></i> Edit Slider Image</a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </table>

                    </div><!-- /.box-body -->
                    <!-- <div class="box-footer clearfix">
                        <?php /*echo $this->pagination->create_links(); */?>
                    </div>-->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>

