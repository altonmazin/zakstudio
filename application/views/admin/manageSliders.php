<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Slider Management
            <small>Add, Upload Slider Images, Delete</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>admin/addSlider"><i class="fa fa-plus"></i> Add New</a>
                </div>
            </div>
            <div class="col-xs-12 text-right">
                <?php
                $success = $this->session->flashdata('success');
                if($success)
                {
                    ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Slider List</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Slider Date</th>
                                <th>Slider Design</th>
                                <th>Slider Status</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            <?php
                            if(!empty($sliderRecords))
                            {
                                foreach($sliderRecords as $record)
                                {
                                    ?>
                                    <tr>
                                        <td><?php echo $record->sliderId; ?></td>
                                        <td><?php echo $record->createdDtm; ?></td>
                                        <td><?php echo $record->sliderDesign; ?></td>
                                        <td><?php echo $record->status == '1' ? 'Enable' : 'Disable'; ?></td>
                                        <?php
                                         $btn_class = $record->status == '1' ? 'btn-danger' : 'btn-success';
                                         $btn_action = $record->status == '1' ? 'disableSlider' : 'enableSlider';
                                         $btn_text = $record->status == '1' ? 'Disable Slider' : 'Enable Slider';
                                         ?>


                                        <td class="text-center">
                                            <a class="btn btn-sm btn-info" href="<?php echo base_url().'admin/sliderImageListing/'.$record->sliderId; ?>"><i class="fa fa-image"></i> View Slider Images</a>
                                            <a target="_blank" class="btn btn-sm btn-warning" href="<?php echo base_url().'testSlider/'.$record->sliderId; ?>"><i class="fa fa-image"></i> Test Slider Images</a>
                                            <a class="btn btn-sm <?php echo $btn_class;?> <?php echo $btn_action;?>" href="#" data-sliderid="<?php echo $record->sliderId; ?>"><?php echo $btn_text;?></i></a>
                                            <a class="btn btn-sm btn-info" href="<?php echo base_url().'admin/editSlider/'.$record->sliderId; ?>"><i class="fa fa-pencil"></i></a>
                                            <a class="btn btn-sm btn-danger deleteSlider" href="#" data-sliderid="<?php echo $record->sliderId; ?>"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </table>

                    </div><!-- /.box-body -->
                     <div class="box-footer clearfix">
                        <?php echo $this->pagination->create_links(); ?>
                    </div>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();
            var link = jQuery(this).get(0).href;
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "admin/slider/sliderListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>
