<?php

$readyId = '';
$top_text = '';
$button_text = '';
$button_url = '';
$hide_text = '';
$title_text = '';
$description_text = '';



if(!empty($readyInfo))
{
    foreach ($readyInfo as $ready)
    {
        $readyId = $ready->readyId;
        $upper_text = $ready->upper_text;
        $subtitle_text = $ready->subtitle_text;
        $button_text = $ready->button_text;
        $title_text = $ready->title_text;
        $button_url = $ready->button_url;
        $hide_text = $ready->hide_text;
    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Ready Section Management
            <small>Add / Edit Ready Section </small>
        </h1>
    </section>

    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->



                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Ready Section Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->

                    <form role="form" action="<?php echo base_url() ?>admin/editprocessReady" enctype="multipart/form-data" method="post" id="editReady" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Enter Upper Text</label>
                                        <input placeholder="" type="text" class="form-control" value="<?php echo $upper_text; ?>" id="upper_text" name="upper_text" >
                                        <input type="hidden" value="<?php echo $readyId; ?>" name="readyId" id="readyId" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Enter Title Text</label>
                                        <input type="text" class="form-control" value="<?php echo $title_text; ?>" id="title_text" name="title_text" >
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Enter Subtitle Text</label>
                                        <input type="text" class="form-control" value="<?php echo $subtitle_text; ?>" id="subtitle_text" name="subtitle_text" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Enter Button Text</label>
                                        <input type="text" class="form-control" value="<?php echo $button_text; ?>" id="button_text" name="button_text" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Enter Button URL</label>
                                        <input type="text" class="form-control" value="<?php echo $button_url; ?>" id="button_url" name="button_url" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Show Image Only (Hide Everything Else)</label>
                                        <select class="form-control" name="hide_text" id="hide_text">
                                            <option value="0" <?php echo ($hide_text == 0) ? 'selected': '';?>>No</option>
                                            <option value="1" <?php echo ($hide_text == 1) ? 'selected': '';?>>Yes</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Select Ready Section Image</label>
                                        <input type="file" class="form-control" readonly="readonly" id="file_name" name="file_name">
                                    </div>

                                </div>
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="button" class="btn btn-default" value="Cancel" onclick="document.location.href='<?php echo base_url();?>admin/pageListing/'" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                $this->load->helper('form');
                $error = $this->session->flashdata('error');
                if($error)
                {
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php } ?>
                <?php
                $success = $this->session->flashdata('success');
                if($success)
                {
                    ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url(); ?>assets/js/editReady.js" type="text/javascript"></script>
