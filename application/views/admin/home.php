<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zak</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link id="favicon" rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon144.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon114.png">
    <link rel="stylesheet" href="http://www.flexbittech.com/zak17/zak/assets/user_panel/css/fonts.css">
    <link rel="stylesheet" href="http://www.flexbittech.com/zak17/zak/assets/user_panel/css/icon-font.css">
    <link rel="stylesheet" href="http://www.flexbittech.com/zak17/zak/assets/user_panel/css/social-icons.css">
    <link rel="stylesheet" href="http://www.flexbittech.com/zak17/zak/assets/user_panel/css/frontend-grid.css">
    <link rel="stylesheet" href="http://www.flexbittech.com/zak17/zak/assets/user_panel/js/owl.carousel/owl.carousel.css">
    <link rel="stylesheet" href="http://www.flexbittech.com/zak17/zak/assets/user_panel/css/style.css">
    <link rel="stylesheet" href="http://www.flexbittech.com/zak17/zak/assets/user_panel/css/mobile.css">
    <link rel="stylesheet" href="http://www.flexbittech.com/zak17/zak/assets/user_panel/css/slider.css">
    <!--[if IE]>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/mootools/1.4.0/mootools-yui-compressed.js"></script>
    <script type="text/javascript" src="js/selectivizr-min.js"></script>
    <script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<style>
    div#masonry {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        height: 100vw;
        max-height: 800px;
        font-size: 0;
    }
    div#masonry img {
        width: 33.3%;
        -webkit-transition: .8s opacity;
        transition: .8s opacity;
        margin:2px;
    }

    div#masonry:hover img { opacity: 0.3; }
    div#masonry:hover img:hover { opacity: 1; }

    /* fallback for earlier versions of Firefox */

    @supports not (flex-wrap: wrap) {
        div#masonry { display: block; }
        div#masonry img {
            display: inline-block;
            vertical-align: top;
        }
    }

</style>
<div id="page">
    <!-- Page preloader -->
    <div id="page-preloader">
        <div class="spinner centered-container"></div>
    </div>
    <!-- END Page preloader -->
    <!-- Page header -->
    <header class="header dark-bg clearfix">
        <div class="fw-container">
            <div class="fw-main-row">
                <div class="fl logo-area"><a href="/"><img src="#" class="logo-dark" alt="zak logo"><img src="#" class="logo-light" alt="zak logo"></a></div>
                <div class="fr">
                    <!-- Site navigation -->
                    <nav class="navigation">
                        <ul>
                            <li class="current-menu-item menu-item-has-children">
                                <a href="/">HOME</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                        <li class="current-menu-item"><a href="photographer.html">Photographer</a></li>
                                        <li><a href="photostudio.html">Photo Studio</a></li>
                                        <li><a href="photoschool.html">Photo School</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="javascript:void(0);">PAGES</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                        <li><a href="about.html">About</a></li>
                                        <li><a href="typography.html">Typography</a></li>
                                        <li><a href="404.html">404 error</a></li>
                                        <li><a href="coming-soon.html">Coming soon</a></li>
                                        <li class="menu-item-has-children">
                                            <a href="javascript:void(0);">Menu level</a>
                                            <div class="sub-nav">
                                                <ul class="sub-menu">
                                                    <li class="menu-item-has-children">
                                                        <a href="javascript:void(0);">Hyperlink 1</a>
                                                        <div class="sub-nav">
                                                            <ul class="sub-menu">
                                                                <li><a href="javascript:void(0);">Hyperlink 1</a></li>
                                                                <li><a href="javascript:void(0);">Hyperlink 2</a></li>
                                                                <li><a href="javascript:void(0);">Hyperlink 3</a></li>
                                                                <li><a href="javascript:void(0);">Hyperlink 4</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item-has-children">
                                                        <a href="javascript:void(0);">Hyperlink 2</a>
                                                        <div class="sub-nav">
                                                            <ul class="sub-menu">
                                                                <li><a href="javascript:void(0);">Hyperlink 1</a></li>
                                                                <li><a href="javascript:void(0);">Hyperlink 2</a></li>
                                                                <li><a href="javascript:void(0);">Hyperlink 3</a></li>
                                                                <li><a href="javascript:void(0);">Hyperlink 4</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="javascript:void(0);">Hyperlink 3</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li><a href="services.html">Services</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="javascript:void(0);">Gallery</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                        <li class="menu-item-has-children">
                                            <a href="javascript:void(0);">Grid</a>
                                            <div class="sub-nav">
                                                <ul class="sub-menu">
                                                    <li><a href="grid-col2.html">Col 2</a></li>
                                                    <li><a href="grid-col3.html">Col 3</a></li>
                                                    <li><a href="grid-col4.html">Col 4</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="javascript:void(0);">Masonry</a>
                                            <div class="sub-nav">
                                                <ul class="sub-menu">
                                                    <li><a href="masonry-col2.html">Col 2</a></li>
                                                    <li><a href="masonry-col3.html">Col 3</a></li>
                                                    <li><a href="masonry-col4.html">Col 4</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li><a href="gallery-horizontal.html">Horizontal</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="javascript:void(0);">Blog</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                        <li><a href="blog1.html">Variant 1</a></li>
                                        <li><a href="blog2.html">Variant 2</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li><a href="contact.html">CONTACT</a></li>
                        </ul>
                    </nav>
                    <!-- END Site navigation -->
                    <!-- Search module -->
                    <div class="search-top">
                        <form class="st-wrap">
                            <input type="text" placeholder="Type and hit enter" name="search" class="input"><button class="st-button icon-font icon-search" type="submit"></button>
                        </form>
                    </div>
                    <!-- END Search module -->
                    <!-- Side area button -->
                    <div class="side-area-btn">
                        <div></div>
                    </div>
                    <!-- END Side area button -->
                </div>
            </div>
        </div>
    </header>
    <!-- END Page header -->
    <!-- Side panel -->
    <section class="side-panel dark-bg">
        <div class="close icon-font icon-cancel-1"></div>
        <header class="sp-top">
            <div class="logo-area"><a href="#"><img src="images/logo-light.png" class="logo-light" alt="Crop It"></a></div>
        </header>
        <div class="sp-wrap">
            <div>
                <h5 class="heading-decor2">gallery</h5>
                <!-- Gallery thumbnails -->
                <div class="gallery-module fw-row">
                    <div class="fw-col-xs-6 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 1"></a></div>
                    <div class="fw-col-xs-6 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 2"></a></div>
                    <div class="fw-col-xs-6 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 3"></a></div>
                    <div class="fw-col-xs-6 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 4"></a></div>
                    <div class="fw-col-xs-6 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 5"></a></div>
                    <div class="fw-col-xs-6 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 6"></a></div>
                    <div class="fw-col-xs-6 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 7"></a></div>
                    <div class="fw-col-xs-6 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 8"></a></div>
                    <div class="fw-col-xs-6 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 9"></a></div>
                </div>
                <h5 class="heading-decor2">contacts</h5>
                <!-- Contact rows -->
                <div class="contact-row">
                    <div class="label">Address:</div>
                    <div class="value">346A State Street, Brooklyn, NY 11217</div>
                </div>
                <div class="contact-row">
                    <div class="label">Phone:</div>
                    <div class="value">+1 (800) 123 45 67 - Office</div>
                </div>
                <div class="contact-row">
                    <div class="label">Email:</div>
                    <div class="value"><a href="mailto:support@promo-theme.com">support@promo-theme.com</a></div>
                </div>
                <!-- Social links -->
                <div class="social-links">
                    <span>follow us:</span>
                    <a href="javascript:void(0);"><i class="social-icons icon-facebook-logo"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-instagram-social-network-logo-of-photo-camera"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-twitter-logo"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-pinterest-social-logo"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-youtube-logotype"></i></a>
                </div>
                <h5 class="heading-decor2">stay in touch</h5>
                <!-- Subscribe form -->
                <form action="javascript:void(0);" class="subscribe-form"><input type="text" placeholder="enter your e-mail" class="input"><button type="submit" class="button-style1 white"><span>subscribe</span></button></form>
            </div>
        </div>
        <footer class="sp-bottom">Copyright © 2016 Promo-themes. All Rights Reserved.</footer>
    </section>
    <!-- END Side panel -->
    <!-- Full scren slider -->
    <section class="full-screen-slider">
        <!-- slider design 1 -->
        <div class="item dark-bg htmlNoPages" >
            <div class="gwd-div-1m5d gwd-gen-f5uugwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg');
					}"></div>
            <div class="gwd-div-1xq9 gwd-gen-z3cbgwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/6.jpg');
					}"></div>
            <div class="gwd-div-1kvs gwd-gen-1okwgwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/7.jpg');
					}"></div>
            <div class="gwd-div-19e4 gwd-gen-1qp0gwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/1.jpg');
					}"></div>

        </div>
        <!-- slider design 2 -->
        <div class="item dark-bg htmlNoPages" >
            <div class="gwd-div-1m5d-design2 gwd-gen-f5uugwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg');
					}"></div>
            <div class="gwd-div-1xq9-design2 gwd-gen-z3cbgwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/6.jpg');
					}"></div>
            <div class="gwd-div-1kvs-design2 gwd-gen-1okwgwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/7.jpg');
					}"></div>
            <div class="gwd-div-19e4-design2 gwd-gen-1qp0gwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/1.jpg');
					}"></div>

        </div>
        <!-- slider design 1 -->
        <div class="item dark-bg htmlNoPages" >
            <div class="gwd-div-1m5d gwd-gen-f5uugwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg');
					}"></div>
            <div class="gwd-div-1xq9 gwd-gen-z3cbgwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/6.jpg');
					}"></div>
            <div class="gwd-div-1kvs gwd-gen-1okwgwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/7.jpg');
					}"></div>
            <div class="gwd-div-19e4 gwd-gen-1qp0gwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/1.jpg');
					}"></div>

        </div>
        <!-- slider design 2 -->
        <div class="item dark-bg htmlNoPages" >
            <div class="gwd-div-1m5d-design2 gwd-gen-f5uugwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg');
					}"></div>
            <div class="gwd-div-1xq9-design2 gwd-gen-z3cbgwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/6.jpg');
					}"></div>
            <div class="gwd-div-1kvs-design2 gwd-gen-1okwgwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/7.jpg');
					}"></div>
            <div class="gwd-div-19e4-design2 gwd-gen-1qp0gwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/1.jpg');
					}"></div>

        </div>
    </section>
    <!-- END Full scren slider -->
    <!-- Info section -->
    <section class="fw-main-row padding-on info-section" style="background: #ededed;">
        <div class="fw-container full">
            <!-- Info section image -->
            <div class="fw-col-xs-12 fw-col-md-4 fw-col-lg-6 is-col">
                <div class="side-image" style="background-image: url(http://www.flexbittech.com/zak17/zak/assets/uploads/about/images.jpg);">
                    <img src="http://www.flexbittech.com/zak17/zak/assets/uploads/about/images.jpg" alt="jennifer light">
                </div>
            </div>
            <!-- END Info section image -->
            <!-- Info section text -->
            <div class="fw-col-xs-12 fw-col-md-8 fw-col-lg-6 is-col tac">
                <div class="is-text clearfix">
                    <h5 style="margin-bottom:60px;">top text data</h5>
                    <h1 class="heading-decor">title text data</h1>
                    <p>description text</p>
                    <a href="javascript:void(0);" class="button-style1" style="margin-top: 45px;margin-bottom: 65px;"><span>read all about</span></a>
                </div>
            </div>
            <!-- END Info section text -->
        </div>
    </section>
    <!-- END Info section -->
    <!-- Portfolio categories -->
    <div class="fw-main-row" style="padding: 15px 0;">
        <div class="fw-container full">
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-4 fw-col-lg-3">
                <div class="portfolio-cat-item" style="background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg);"><a href="javascript:void(0);"><span>fashion</span></a></div>
            </div>
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-4 fw-col-lg-3">
                <div class="portfolio-cat-item" style="background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg);"><a href="javascript:void(0);"><span>portrait</span></a></div>
            </div>
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-4 fw-col-lg-3">
                <div class="portfolio-cat-item" style="background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg);"><a href="javascript:void(0);"><span>people</span></a></div>
            </div>
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-4 fw-col-lg-3">
                <div class="portfolio-cat-item" style="background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg);"><a href="javascript:void(0);"><span>nature</span></a></div>
            </div>
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-4 fw-col-lg-3">
                <div class="portfolio-cat-item" style="background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg);"><a href="javascript:void(0);"><span>beauty</span></a></div>
            </div>
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-4 fw-col-lg-3">
                <div class="portfolio-cat-item" style="background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg);"><a href="javascript:void(0);"><span>world</span></a></div>
            </div>
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-4 fw-col-lg-3">
                <div class="portfolio-cat-item" style="background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg);"><a href="javascript:void(0);"><span>lifestyle</span></a></div>
            </div>
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-4 fw-col-lg-3">
                <div class="portfolio-cat-item" style="background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg);"><a href="javascript:void(0);"><span>for shops</span></a></div>
            </div>
        </div>
    </div>
    <!-- END Portfolio categories -->
    <!-- Instagram photo slider -->
    <section class="fw-main-row dark-bg" style="padding: 30px 0 0;">
        <div class="fw-container full">
            <div class="fw-col-xs-12">
                <h2 class="heading-decor2 normal"><span>my instagrsdfsfam</span> <a href="javascript:void(0);" class="button-style1 fr hd-btn"><i class="social-icons icon-instagram-social-network-logo-of-photo-camera"></i><span>follow us</span></a></h2>
            </div>
            <div class="fw-row">
                <div class="ig-carousel">
                    <div class="item" style="background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg);"><a href="javascript:void(0);"></a></div>
                    <div class="item" style="background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg);"><a href="javascript:void(0);"></a></div>
                    <div class="item" style="background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg);"><a href="javascript:void(0);"></a></div>
                    <div class="item" style="background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg);"><a href="javascript:void(0);"></a></div>
                    <div class="item" style="background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg);"><a href="javascript:void(0);"></a></div>
                    <div class="item" style="background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg);"><a href="javascript:void(0);"></a></div>
                    <div class="item" style="background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg);"><a href="javascript:void(0);"></a></div>
                </div>
            </div>
        </div>
    </section>
    <!-- END Instagram photo slider -->

    <!-- Big banner -->
    <section class="fw-main-row dark-bg" style="height:470px; margin: 20px 0 20px 0; background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg);">
        <div class="fw-container centered-container tal">
            <h5>Small Text</h5>
            <h1 class="heading-decor">Title Text</h1>
            <p><i>Subtitle Text</i></p>
            <a href="javascript:void(0);" class="button-style1 white"></a>
        </div>
    </section>
    <!-- END Big banner -->

    <!-- Logos -->
    <div class="fw-main-row" style="padding: 0 0 50px;">
        <div class="fw-container">
            <div class="fw-row">
                <div class="ig-carousel">
                    <div class="item" style="background-image: url(http://www.flexbittech.com/zak17/zak/assets/uploads/business_logo/images.jpg);">
                    </div>
                    <div class="item" style="background-image: url(http://www.flexbittech.com/zak17/zak/assets/uploads/business_logo/9998823.jpg);">
                    </div>
                    <div class="item" style="background-image: url(http://www.flexbittech.com/zak17/zak/assets/uploads/business_logo/cairn-fog-mystical-background-158607.jpeg);">
                    </div>
                    <div class="item" style="background-image: url(http://www.flexbittech.com/zak17/zak/assets/uploads/business_logo/gems_page.jpg);">
                    </div>
                    <div class="item" style="background-image: url(http://www.flexbittech.com/zak17/zak/assets/uploads/business_logo/download.png);">
                    </div>
                    <div class="item" style="background-image: url(http://www.flexbittech.com/zak17/zak/assets/uploads/business_logo/images.png);">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Logos -->
    <!-- Footer -->
    <footer class="footer dark-bg">
        <div class="fw-container">
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-3">
                <div class="h5 heading-decor2">contacts</div>
                <!-- Contact rows -->
                <div class="contact-row">
                    <div class="label">Address:</div>
                    <div class="value">346A State Street, Brooklyn, NY 11217</div>
                </div>
                <div class="contact-row">
                    <div class="label">Phone:</div>
                    <div class="value">+1 (800) 123 45 67 - Office</div>
                </div>
                <div class="contact-row">
                    <div class="label">Email:</div>
                    <div class="value"><a href="mailto:support@promo-theme.com">support@promo-theme.com</a></div>
                </div>
            </div>
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-3">
                <div class="h5 heading-decor2">gallery</div>
                <!-- Gallery thumbnails -->
                <div class="gallery-module fw-row">
                    <div class="fw-col-xs-4 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 1"></a></div>
                    <div class="fw-col-xs-4 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 2"></a></div>
                    <div class="fw-col-xs-4 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 3"></a></div>
                    <div class="fw-col-xs-4 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 4"></a></div>
                    <div class="fw-col-xs-4 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 5"></a></div>
                    <div class="fw-col-xs-4 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 6"></a></div>
                </div>
            </div>
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-2">
                <div class="h5 heading-decor2">LINKS</div>
                <!-- Footer nav -->
                <nav class="footer-nav">
                    <ul>
                        <li><a href="photographer.html">Home Page</a></li>
                        <li><a href="about.html">About Us</a></li>
                        <li><a href="services.html">Services</a></li>
                        <li><a href="gallery1.html">Gallery</a></li>
                        <li><a href="blog1.html">Blog</a></li>
                        <li><a href="contact.html">Contact</a></li>
                    </ul>
                </nav>
            </div>
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-4">
                <div class="h5 heading-decor2">stay in touch</div>
                <!-- Subscribe form -->
                <form action="javascript:void(0);" class="subscribe-form"><input type="text" placeholder="Enter your e-mail" class="input"><button type="submit" class="button-style1"><span>subscribe</span></button></form>
                <!-- Social links -->
                <div class="social-links">
                    <span>follow us:</span>
                    <a href="javascript:void(0);"><i class="social-icons icon-facebook-logo"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-instagram-social-network-logo-of-photo-camera"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-twitter-logo"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-pinterest-social-logo"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-youtube-logotype"></i></a>
                </div>
            </div>
        </div>
    </footer>
    <!-- END Footer -->
</div>		<script src="http://www.flexbittech.com/zak17/zak/assets/user_panel/js/jquery-2.1.3.min.js"></script>
<script src="http://www.flexbittech.com/zak17/zak/assets/user_panel/js/owl.carousel/owl.carousel.min.js"></script>
<script src="http://www.flexbittech.com/zak17/zak/assets/user_panel/js/jquery.animateNumber.min.js"></script>
<script src="http://www.flexbittech.com/zak17/zak/assets/user_panel/js/script.js"></script>
</body>
</html>