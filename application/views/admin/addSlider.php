<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Slider Management
            <small>Add / Edit Slider Images</small>
        </h1>
    </section>

    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Slider Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->

                    <form role="form" name="formname" action="<?php echo base_url() ?>admin/processAddSlider" enctype="multipart/form-data" method="post" >
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Select Slider Design</label>
                                        <select class="form-control required" id="sliderDesign" name="sliderDesign">
                                            <option value="Now or Never">Now or Never</option>
                                            <option value="Caged Birds">Caged Birds</option>
                                            <option value="Single Image">Full Single Image</option>
                                        </select>
                                       </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Select Slider Status</label>
                                        <select class="form-control required" id="status" name="status">
                                            <option value="1">Enable</option>
                                            <option value="0">Disable</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Enter Slide Order </label>
                                        <input placeholder="Enter Numbers Only" class="form-control required" type="text" name="sliderOrder" id="sliderOrder"><span id="errmsg" style="color: red"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="fname">Browse Images (Max Width = 600px , Max Height =400px, 3 Images Mandatory)
                                            <input type="file" id="upload_files" name="uploaded_files[]" onchange="GetFileSize()" required="required" multiple>
                                            <p style="color: red;" id="fileInfo"></p>

                                    </div>
                                </div>
                            </div>

                        </div><!-- /.box-body -->

                        <div class="box-footer" id="submitForm">
                            <input type="submit"  class="btn btn-primary" value="Submit"  />
                            <input type="button" class="btn btn-default" value="Cancel" onclick="document.location.href='<?php echo base_url();?>admin/sliderListing/'" />
                        </div>
                        <div id="loading" style="display: none"><p style="color: red; font-weight: bold;">Uploading please wait....</p></div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                $this->load->helper('form');
                $error = $this->session->flashdata('error');
                if($error)
                {
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php } ?>
                <?php
                $success = $this->session->flashdata('success');
                if($success)
                {
                    ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
<script>
    $(document).ready(function () {
        //called when key is pressed in textbox
        $("#sliderOrder").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                $("#errmsg").html("Numbers Only").show().fadeOut("slow");
                return false;
            }
        });
    });
</script>
<script src="<?php echo base_url(); ?>assets/js/addSlider.js" type="text/javascript"></script>