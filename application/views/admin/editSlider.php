<?php

$sliderId = '';
$sliderDesign = '';
$sliderOrder = '';
$status = '';

if(!empty($sliderInfo))
{
    foreach ($sliderInfo as $slider)
    {
        $sliderId = $slider->sliderId;
        $sliderDesign = $slider->sliderDesign;
        $sliderOrder = $slider->sliderOrder;
        $status = $slider->status;
    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Slider Management
            <small>Add / Edit Slider </small>
        </h1>
    </section>

    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->



                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Slider Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->

                    <form role="form" action="<?php echo base_url() ?>admin/editprocessSlider"  method="post" id="editSlider" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Select Slider Design</label>
                                        <select class="form-control required" id="sliderDesign" name="sliderDesign">
                                            <option value="Now or Never" <?php echo $sliderDesign == 'Now or Never' ? 'selected': '';?>>Now or Never</option>
                                            <option value="Caged Birds" <?php echo $sliderDesign == 'Caged Birds' ? 'selected': '';?>>Caged Birds</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Select Slider Status</label>
                                        <select class="form-control required" id="status" name="status">
                                            <option value="1" <?php echo $status == '1' ? 'selected': '';?> >Enable</option>
                                            <option value="0" <?php echo $status == '0' ? 'selected': '';?> >Disable</option>
                                        </select>
                                        <input type="hidden" value="<?php echo $sliderId;?>" name="sliderId" id="sliderId">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Enter Slider Order </label>
                                        <input placeholder="Enter Numbers Only" class="form-control required" value="<?php echo $sliderOrder;?>"  maxlength="11" type="number" name="sliderOrder" id="sliderOrder"><span id="errmsg" style="color: red"></span>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="button" class="btn btn-default" onclick="document.location.href='<?php echo base_url();?>admin/sliderListing/'" value="Cancel" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                $this->load->helper('form');
                $error = $this->session->flashdata('error');
                if($error)
                {
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php } ?>
                <?php
                $success = $this->session->flashdata('success');
                if($success)
                {
                    ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(document).ready(function () {
        //called when key is pressed in textbox
        $("#sliderOrder").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });
    });
</script>