<?php

$logoId = '';
$title = '';
$description = '';
$url = '';


if(!empty($logoInfo))
{
    foreach ($logoInfo as $logo)
    {
        $logoId = $logo->logoId;
        $title = $logo->title;
        $description = $logo->description;
        $url = $logo->url;
    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Logo Management
            <small>Add / Edit Logo </small>
        </h1>
    </section>

    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->



                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Logo Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->

                    <form role="form" action="<?php echo base_url() ?>admin/editprocessLogo" enctype="multipart/form-data" method="post" id="editLogo" role="form">
                        <div class="box-body">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Enter Logo Title</label>
                                        <input type="text" class="form-control" value="<?php echo $title; ?>" id="title" name="title" maxlength="128">
                                        <input type="hidden" value="<?php echo $logoId; ?>" name="logoId" id="logoId" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="fname">Enter Logo Description</label>
                                        <textarea rows="5" cols="50" name="description" id="description"><?php echo $description;?></textarea>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Enter Site Logo URL</label>
                                        <input type="text" class="form-control" value="<?php echo $url; ?>" id="url" name="url" >

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Enter Logo Image</label>
                                        <input type="file" class="form-control required" readonly="readonly" id="file_name" name="file_name">
                                    </div>

                                </div>
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="button" class="btn btn-default" value="Cancel" onclick="document.location.href='<?php echo base_url();?>admin/logoListing/'" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                $this->load->helper('form');
                $error = $this->session->flashdata('error');
                if($error)
                {
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php } ?>
                <?php
                $success = $this->session->flashdata('success');
                if($success)
                {
                    ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editLogo.js" type="text/javascript"></script>
