<?php

$imgId = '';
$sliderId = '';


if(!empty($sliderImageInfo))
{
    foreach ($sliderImageInfo as $image)
    {
        $imgId = $image->imgId;
        $sliderId = $image->sliderId;
    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Slider Image Management
            <small>Add / Edit Slider Image </small>
        </h1>
    </section>

    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->



                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Slider Image Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->

                    <form role="form" action="<?php echo base_url() ?>admin/editprocessSliderImage" method="post" enctype="multipart/form-data" id="editSliderImage" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Upload Slider Image</label>
                                        <input type="file" class="form-control required" readonly="readonly" id="file_name" name="file_name">
                                        <input type="hidden" name="imgId" id="imgId" value="<?php echo $imgId;?>">
                                        <input type="hidden" name="sliderId" id="sliderId" value="<?php echo $sliderId;?>">
                                     </div>

                                </div>
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="button"  onclick="document.location.href='<?php echo base_url();?>admin/sliderImageListing/<?php echo $sliderId;?>'" class="btn btn-default" value="Cancel" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                $this->load->helper('form');
                $error = $this->session->flashdata('error');
                if($error)
                {
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php } ?>
                <?php
                $success = $this->session->flashdata('success');
                if($success)
                {
                    ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

