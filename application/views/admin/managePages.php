<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Page Management
            <small>Add, Edit, Delete</small>
        </h1>
    </section>
    <section class="content">

        <div class="row">
            <div class="col-xs-12 text-right">
                <?php
                $success = $this->session->flashdata('success');
                if($success)
                {
                    ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php } ?>
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Page Section List</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Page Title Description</th>
                                <th>Page Section Description</th>                                
                                <th class="text-center">Actions</th>
                            </tr>
                            <?php
                            if(!empty($pageRecords))
                            {
                                $i=1;
                                $url = '';
                                foreach($pageRecords as $record)
                                {

                                    if($i == 1)
                                        $url = base_url().'admin/editAbout/';
                                    else
                                        $url = base_url().'admin/editReady/';
                                    ?>
                                    <tr>
                                        <td><?php echo $record->pageId; ?></td>
                                        <td><?php echo $record->title; ?></td>
                                        <td><?php echo $record->description; ?></td>

                                        <td class="text-center">
                                            <a class="btn btn-sm btn-info" href="<?php echo $url;?>"><i class="fa fa-pencil"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                        </table>

                    </div><!-- /.box-body -->
                    <!-- <div class="box-footer clearfix">
                        <?php /*echo $this->pagination->create_links(); */?>
                    </div>-->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
