<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zak</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link id="favicon" rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon144.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon114.png">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/icon-font.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/social-icons.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/frontend-grid.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/js/owl.carousel/owl.carousel.css"><!--
    <link rel="stylesheet" href="<?php /*echo base_url('assets/user_panel'); */?>/js/owl.carousel/owl.theme.css">-->
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/mobile.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/slider.css">


    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/slider'); ?>/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/slider'); ?>/css/demo.css" />

    <link rel="stylesheet" href="<?php echo base_url('assets/slider'); ?>/css/slideshow.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/slider'); ?>/css/slideshow_layouts.css">

    <!--[if IE]>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/mootools/1.4.0/mootools-yui-compressed.js"></script>
    <script type="text/javascript" src="js/selectivizr-min.js"></script>
    <script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script>document.documentElement.className = 'js';</script>

</head>
<body>
<style>
    /*.owl-carousel .item-video{height:700px}*/
    /* Instagram CSS Styles */
    .owl-controls {
        text-align: center;
    }
    .owl-next, .owl-prev {
        margin: 5px;
        font-size: 14px;
        text-decoration: none;
        display: inline-block;
        background: #3c3c3b;
        color: #fff;
        text-transform: uppercase;
        /* border-radius: 2px; */
        overflow: hidden;
        line-height: 1.2em;
        padding: 11px 20px 11px;
        position: relative;
        z-index: 1;
        font-weight: bold;
        -webkit-transition: all 0.3s ease;
        -moz-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        -ms-transition: all 0.3s ease;
        transition: all 0.3s ease;
        cursor: pointer;
        /* min-width: 170px; */
        text-align: center;
        border: 2px solid #3c3c3b;
        -moz-box-sizing: border-box;
        -ms-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -o-box-sizing: border-box;
        box-sizing: border-box;
    }

    .owl-next:hover, .owl-prev:hover {
        color: #3c3c3b;
        background: #fff;
    }
    }
    .owl-dot {
        /*display: inline-block;*/
        display: none !important;
        border: solid 2px #9E5A5A;
        width: 15px;
        height: 15px;
        margin: 5px;

        border-radius: 50%;
    }
    .owl-dot.active {
        background: #CAB4B4;
    }
    .owl-carousel .owl-video-wrapper {
        background-color: #fff;
    }
</style>
<style>
    .owl-wrapper {
        margin: 0 auto;
    }
    div#masonry {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        height: 100vw;
        max-height: 800px;
        font-size: 0;
    }
    div#masonry img {
        width: 33.3%;
        -webkit-transition: .8s opacity;
        transition: .8s opacity;
        margin:2px;
    }

    div#masonry:hover img { opacity: 0.3; }
    div#masonry:hover img:hover { opacity: 1; }

    /* fallback for earlier versions of Firefox */

    @supports not (flex-wrap: wrap) {
        div#masonry { display: block; }
        div#masonry img {
            display: inline-block;
            vertical-align: top;
        }
    }
    .owl-centered{
        margin: auto;
    }
</style>
<div id="page">
    <!-- Page preloader -->
    <div id="page-preloader">
        <div class="spinner centered-container"></div>
    </div>
    <!-- END Page preloader -->
    <!-- Page header -->
    <header class="header dark-bg clearfix">
        <div class="fw-container">
            <div class="fw-main-row">
                <div class="fl logo-area"><a href="/"><img src="#" class="logo-dark" alt="zak logo"><img src="#" class="logo-light" alt="zak logo"></a></div>
                <div class="fr">
                    <!-- Site navigation -->
                    <nav class="navigation">
                        <ul>
                            <li class="current-menu-item menu-item-has-children">
                                <a href="<?php echo site_url();?>home">HOME</a>
                                <!--<div class="sub-nav">
                                    <ul class="sub-menu">
                                        <li class="current-menu-item"><a href="photographer.html">Photographer</a></li>
                                        <li><a href="photostudio.html">Photo Studio</a></li>
                                        <li><a href="photoschool.html">Photo School</a></li>
                                    </ul>
                                </div>-->
                            </li>
                            <!-- <li class="menu-item-has-children">
                                 <a href="javascript:void(0);">PAGES</a>
                                 <div class="sub-nav">
                                     <ul class="sub-menu">
                                         <li><a href="about.html">About</a></li>
                                         <li><a href="typography.html">Typography</a></li>
                                         <li><a href="404.html">404 error</a></li>
                                         <li><a href="coming-soon.html">Coming soon</a></li>
                                         <li class="menu-item-has-children">
                                             <a href="javascript:void(0);">Menu level</a>
                                             <div class="sub-nav">
                                                 <ul class="sub-menu">
                                                     <li class="menu-item-has-children">
                                                         <a href="javascript:void(0);">Hyperlink 1</a>
                                                         <div class="sub-nav">
                                                             <ul class="sub-menu">
                                                                 <li><a href="javascript:void(0);">Hyperlink 1</a></li>
                                                                 <li><a href="javascript:void(0);">Hyperlink 2</a></li>
                                                                 <li><a href="javascript:void(0);">Hyperlink 3</a></li>
                                                                 <li><a href="javascript:void(0);">Hyperlink 4</a></li>
                                                             </ul>
                                                         </div>
                                                     </li>
                                                     <li class="menu-item-has-children">
                                                         <a href="javascript:void(0);">Hyperlink 2</a>
                                                         <div class="sub-nav">
                                                             <ul class="sub-menu">
                                                                 <li><a href="javascript:void(0);">Hyperlink 1</a></li>
                                                                 <li><a href="javascript:void(0);">Hyperlink 2</a></li>
                                                                 <li><a href="javascript:void(0);">Hyperlink 3</a></li>
                                                                 <li><a href="javascript:void(0);">Hyperlink 4</a></li>
                                                             </ul>
                                                         </div>
                                                     </li>
                                                     <li><a href="javascript:void(0);">Hyperlink 3</a></li>
                                                 </ul>
                                             </div>
                                         </li>
                                         <li><a href="services.html">Services</a></li>
                                     </ul>
                                 </div>
                             </li>-->
                            <li class="menu-item-has-children">
                                <a href="<?php echo site_url();?>home/getAllAlbumImages">Gallery</a>
                                <!--<div class="sub-nav">
                                    <ul class="sub-menu">
                                        <li class="menu-item-has-children">
                                            <a href="javascript:void(0);">Grid</a>
                                            <div class="sub-nav">
                                                <ul class="sub-menu">
                                                    <li><a href="grid-col2.html">Col 2</a></li>
                                                    <li><a href="grid-col3.html">Col 3</a></li>
                                                    <li><a href="grid-col4.html">Col 4</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="javascript:void(0);">Masonry</a>
                                            <div class="sub-nav">
                                                <ul class="sub-menu">
                                                    <li><a href="masonry-col2.html">Col 2</a></li>
                                                    <li><a href="masonry-col3.html">Col 3</a></li>
                                                    <li><a href="masonry-col4.html">Col 4</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li><a href="gallery-horizontal.html">Horizontal</a></li>
                                    </ul>
                                </div>-->
                            </li>
                            <!-- <li class="menu-item-has-children">
                                 <a href="javascript:void(0);">Blog</a>
                                 <div class="sub-nav">
                                     <ul class="sub-menu">
                                         <li><a href="blog1.html">Variant 1</a></li>
                                         <li><a href="blog2.html">Variant 2</a></li>
                                     </ul>
                                 </div>
                             </li>-->
                            <li><a href="#">CONTACT</a></li>
                        </ul>
                    </nav>
                    <!-- END Site navigation -->
                    <!-- Search module -->
                    <div class="search-top">
                        <form class="st-wrap">
                            <input type="text" placeholder="Type and hit enter" name="search" class="input"><button class="st-button icon-font icon-search" type="submit"></button>
                        </form>
                    </div>
                    <!-- END Search module -->
                    <!-- Side area button -->
                    <div class="side-area-btn">
                        <div></div>
                    </div>
                    <!-- END Side area button -->
                </div>
            </div>
        </div>
    </header>
    <!-- END Page header -->
    <!-- Side panel -->
    <?php $this->view('sidebar'); ?>
    <!-- END Side panel -->
    <!-- Full scren slider -->

    <?php if(!empty($sliders)) { ?>
    <section class="fw-main-row">
        <svg class="hidden">
        <defs>
            <symbol id="icon-arrow" viewBox="0 0 24 24">
                <title>arrow</title>
                <polygon points="6.3,12.8 20.9,12.8 20.9,11.2 6.3,11.2 10.2,7.2 9,6 3.1,12 9,18 10.2,16.8 "/>
            </symbol>
            <symbol id="icon-drop" viewBox="0 0 24 24">
                <title>drop</title>
                <path d="M12,21c-3.6,0-6.6-3-6.6-6.6C5.4,11,10.8,4,11.4,3.2C11.6,3.1,11.8,3,12,3s0.4,0.1,0.6,0.3c0.6,0.8,6.1,7.8,6.1,11.2C18.6,18.1,15.6,21,12,21zM12,4.8c-1.8,2.4-5.2,7.4-5.2,9.6c0,2.9,2.3,5.2,5.2,5.2s5.2-2.3,5.2-5.2C17.2,12.2,13.8,7.3,12,4.8z"/><path d="M12,18.2c-0.4,0-0.7-0.3-0.7-0.7s0.3-0.7,0.7-0.7c1.3,0,2.4-1.1,2.4-2.4c0-0.4,0.3-0.7,0.7-0.7c0.4,0,0.7,0.3,0.7,0.7C15.8,16.5,14.1,18.2,12,18.2z"/>
            </symbol>
            <symbol id="icon-prev" viewBox="0 0 100 50">
                <title>prev</title>
                <polygon points="5.4,25 18.7,38.2 22.6,34.2 16.2,27.8 94.6,27.8 94.6,22.2 16.2,22.2 22.6,15.8 18.7,11.8"/>
            </symbol>
            <symbol id="icon-next" viewBox="0 0 100 50">
                <title>next</title>
                <polygon points="81.3,11.8 77.4,15.8 83.8,22.2 5.4,22.2 5.4,27.8 83.8,27.8 77.4,34.2 81.3,38.2 94.6,25 "/>
            </symbol>
            <symbol id="icon-octicon" viewBox="0 0 24 24">
                <title>octicon</title>
                <path d="M12,2.2C6.4,2.2,1.9,6.7,1.9,12.2c0,4.4,2.9,8.2,6.9,9.6c0.5,0.1,0.7-0.2,0.7-0.5c0-0.2,0-0.9,0-1.7c-2.8,0.6-3.4-1.4-3.4-1.4C5.6,17.1,5,16.8,5,16.8C4.1,16.2,5,16.2,5,16.2c1,0.1,1.5,1,1.5,1c0.9,1.5,2.4,1.1,2.9,0.8c0.1-0.7,0.4-1.1,0.6-1.3c-2.2-0.3-4.6-1.1-4.6-5c0-1.1,0.4-2,1-2.7C6.5,8.8,6.2,7.7,6.7,6.4c0,0,0.8-0.3,2.8,1C10.3,7.2,11.1,7.1,12,7c0.9,0,1.7,0.1,2.5,0.3c1.9-1.3,2.8-1,2.8-1c0.5,1.4,0.2,2.4,0.1,2.7c0.6,0.7,1,1.6,1,2.7c0,3.9-2.4,4.7-4.6,5c0.4,0.3,0.7,0.9,0.7,1.9c0,1.3,0,2.4,0,2.8c0,0.3,0.2,0.6,0.7,0.5c4-1.3,6.9-5.1,6.9-9.6C22.1,6.7,17.6,2.2,12,2.2z" />
            </symbol>
            <!-- From Karen Menezes: https://www.smashingmagazine.com/2015/05/creating-responsive-shapes-with-clip-path/ -->
            <clipPath id="polygon-clip-rhomboid" clipPathUnits="objectBoundingBox">
                <polygon points="0 1, 0.3 0, 1 0, 0.7 1" />
            </clipPath>
        </defs>
    </svg>
    <main style="background-color: #302D2A;">
        <div class="slideshow" tabindex="0">
            <?php
            $count = count($sliders);
            foreach ($sliders as $slider) {
                if($slider->sliderDesign == 'Now or Never') {
                $class = 'slide slide--layout-1';
                $layout = 'layout1';
                $singleImgStyle = '';
                } elseif($slider->sliderDesign == 'Caged Birds') {
                    $class = 'slide slide--layout-7';
                    $layout = 'layout7';
                    $singleImgStyle = '';
                }else {
                    $class = 'slide slide--layout-1';
                    $layout = 'layout1';
                    $singleImgStyle = 'style="width: 100%"';
                } ?>
                <div class="<?php echo $class;?>" data-layout="<?php echo $layout;?>">
                    <div class="slide-imgwrap">
                        <?php foreach ($sliderImages[$slider->sliderId] as $image) { ?>
                            <div class="slide__img" <?php echo $singleImgStyle;?>><div class="slide__img-inner" style="background-position: top center;background-image: url('<?php echo base_url().'sliderUploads/'.$image->file_name;?>');"></div></div>
                        <?php } ?>

                    </div>
                    <div class="slide__title">
                        <h3 class="slide__title-main"></h3>
                        <p class="slide__title-sub"></p>
                    </div>
                </div><!-- /slide -->
                <?php if($count == 1) { ?>
                    <div class="<?php echo $class;?>" data-layout="<?php echo $layout;?>">
                        <div class="slide-imgwrap">
                            <?php foreach ($sliderImages[$slider->sliderId] as $image) { ?>
                                <div class="slide__img" <?php echo $singleImgStyle;?>><div class="slide__img-inner" style="background-position: top center;background-image: url('<?php echo base_url().'sliderUploads/'.$image->file_name;?>');"></div></div>
                            <?php } ?>

                        </div>
                        <div class="slide__title">
                            <h3 class="slide__title-main"></h3>
                            <p class="slide__title-sub"></p>
                        </div>
                    </div><!-- /slide -->
                <?php } ?>
                <?php
            }
            ?>
            <nav class="slideshow__nav slideshow__nav--arrows">
                <button id="prev-slide" class="btn btn--arrow" aria-label="Previous slide"><svg class="icon icon--prev"><use xlink:href="#icon-prev"></use></svg></button>
                <button id="next-slide" class="btn btn--arrow" aria-label="Next slide"><svg class="icon icon--next"><use xlink:href="#icon-next"></use></svg></button>
            </nav>
        </div>
    </main>
    </section>
    
    <?php } ?>

    <!-- END Full scren slider -->
    <!-- Info section -->
    <section class="fw-main-row padding-on info-section" style="background: #ededed;">
        <div class="fw-container full">
            <!-- Info section image -->
            <?php
                $abtSection = $aboutSec['hide_text'];
                if($abtSection == '0') {
                    $class = "fw-col-xs-12 fw-col-md-4 fw-col-lg-6 is-col";
                    $style = '';
                } else {
                    $class = "fw-col-xs-12 fw-col-md-12 fw-col-lg-12";
                    $style = 'style = "height: 550px;"';
                }

            ?>
            <div class="<?php echo $class;?>" <?php echo $style;?> >
                <div class="side-image" style="background-image: url(<?php echo base_url('uploads/pages/' . $aboutSec['file_name']); ?>);">
                    <img src="<?php echo base_url('uploads/pages/' . $aboutSec['file_name']); ?>" alt="About Me">
                </div>
            </div>
            <!-- END Info section image -->
            <!-- Info section text -->
            <?php if($abtSection == '0' ) { ?>
            <div class="fw-col-xs-12 fw-col-md-8 fw-col-lg-6 is-col tac">
                <div class="is-text clearfix">
                    <h5 style="margin-bottom:60px;"><?php echo $aboutSec['top_text']; ?></h5>
                    <h1 class="heading-decor"><?php echo $aboutSec['title_text']; ?></h1>
                    <p><?php echo $aboutSec['description_text']; ?></p>
                    <?php if ($aboutSec['button_text'] != '') { ?>
                         <a href="<?php echo ($aboutSec['button_url'] != '') ? $aboutSec['button_url'] : '#' ;?>" class="button-style1" style="margin-top: 45px;margin-bottom: 65px;"><span><?php echo $aboutSec['button_text'] != '' ? $aboutSec['button_text'] : 'read all about';?></span></a>
                   <?php  } ?>

                </div>
            </div>
          <?php  } ?>

            <!-- END Info section text -->
        </div>
    </section>
    <!-- END Info section -->
    <!-- Portfolio categories -->
    <div class="fw-main-row" style="padding: 15px 0;">
        <div class="fw-container full">
            <?php
            if(($albumRecords)) {

                foreach($albumRecords as $record) { ?>
                    <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-4 fw-col-lg-3">
                        <div class="portfolio-cat-item" style="background-image: url('<?php echo base_url('albumUploads/albumImages/' . $record->file_name); ?>');"><a href="home/albumGallery/<?php echo $record->albumId;?>"><span><?php echo $record->title;?></span></a></div>
                    </div>
                <?php }
            }?>



        </div>
    </div>
    <!-- END Portfolio categories -->
    <!-- Instagram photo slider -->
    <div class="owl-video owl-carousel owl-theme">
        <?php foreach($videos as $record) { ?>
            <div class="item-video" data-merge="3"><a class="owl-video" href="<?php echo $record->url;?>"></a></div>
        <?php } ?>

    </div>
    <!-- Instagram photo slider -->
    <section class="fw-main-row dark-bg" style="padding: 30px 0 0;">
        <div class="fw-container full">
            <div class="fw-col-xs-12">
                <h2 class="heading-decor2 normal"><span>Our Instagram</span> <a href="https://www.instagram.com/beambooth/" target="_blank" class="button-style1 fr hd-btn"><i class="social-icons icon-instagram-social-network-logo-of-photo-camera"></i><span>follow us</span></a></h2>
            </div>
            <div class="fw-row">
                <!--<div class="ig-carousel" id="instagramSlides" >

                </div>-->


            </div>
        </div>
        <input type="hidden" name="instagramTag" id="instagramTag" value="<?php echo $instagramObj[0]->tag;?>">
        <div id="instafeed" class="instafeed owl-carousel-feed"></div>
    </section>
    <!-- END Instagram photo slider -->

    <!-- Big banner -->
    <section class="fw-main-row dark-bg" style="height:470px; margin: 20px 0 20px 0; background-image: url('<?php echo base_url('uploads/pages/' . $readySec['file_name']); ?>');">
        <div class="fw-container centered-container tal">
            <?php if($readySec['hide_text'] == '0') { ?>
                <?php if($readySec['upper_text'] != '') {?>
                <h5><?php echo $readySec['upper_text'] ?></h5>
               <?php } ?>
                <?php if($readySec['title_text'] != '') {?>
                <h1 class="heading-decor"><?php echo $readySec['title_text'] ?></h1>
                <?php } ?>
                <?php if($readySec['subtitle_text'] != '') {?>
                     <p><i><?php echo $readySec['subtitle_text'] ?></i></p>
                <?php } ?>
                <?php if($readySec['button_text'] != '') {?>
                     <a href="<?php echo ($readySec['button_url'] != '') ? $readySec['button_url'] : '#' ;?>" class="button-style1 white"><span><?php echo $readySec['button_text'] !='' ? $readySec['button_text']: 'Submit'; ?></span></a>
                <?php } ?>
            <?php } ?>
        </div>
    </section>
    <!-- END Big banner -->

    <!-- Logos -->
    <div class="fw-main-row" style="padding: 0 0 0px;">
        <div class="fw-container">
            <div class="fw-row" >
                <?php if ($busniesslogos){ ?>
                    <?php if(count($busniesslogos) <= 7) { ?>
                        <div style="text-align:center; margin: 5% 0 5% 0; padding: 0px">
                            <?php foreach ($busniesslogos as $row){  ?>
                                <a href="<?php echo $row['url'] == '' ? '#' : $row['url'];?>" target="_blank">
                                    <img style="margin: 15px" width="" src="<?php echo base_url('uploads/logos/' . $row['file_name']); ?>">
                                </a>

                            <?php } ?>
                        </div>
                    <?php } else { ?>
                        <div class="owl-logo owl-carousel" id="logoImg">
                            <?php foreach ($busniesslogos as $row): ?>
                                <div>
                                    <a href="<?php echo $row['url'] == '' ? '#' : $row['url'];?>" target="_blank">
                                        <img width="120px" src="<?php echo base_url('uploads/logos/' . $row['file_name']); ?>">
                                    </a>
                                </div>
                            <?php endforeach;?>

                        </div>
                    <?php } }?>

            </div>
        </div>
    </div>
</div>
<!-- END Logos -->
<!-- Footer -->
<?php $this->view('footer'); ?>
<!-- END Footer -->
</div>
<script src="<?php echo base_url('assets/user_panel'); ?>/js/jquery-2.1.3.min.js"></script>
<script src="<?php echo base_url('assets/user_panel'); ?>/js/instafeed.min.js"></script>
<script src="<?php echo base_url('assets/user_panel'); ?>/js/owl.carousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url('assets/user_panel'); ?>/js/jquery.animateNumber.min.js"></script>
<script src="<?php echo base_url('assets/user_panel'); ?>/js/script.js"></script>

<script src="<?php echo base_url('assets/slider'); ?>/js/imagesloaded.pkgd.min.js"></script>
<script src="<?php echo base_url('assets/slider'); ?>/js/anime.min.js"></script>
<script src="<?php echo base_url('assets/slider'); ?>/js/main.js"></script>
<script>
    (function() {
        var slideshow = new MLSlideshow(document.querySelector('.slideshow'));

        document.querySelector('#next-slide').addEventListener('click', function() {
            slideshow.next();
        });

        document.querySelector('#prev-slide').addEventListener('click', function() {
            slideshow.prev();
        });
    })();
</script>

<script>
    jQuery( document ).ready(function($) {
        //$('#logoItem > div.owl-stage-outer > div.owl-stage > div.owl-item').css('width', '200px');
        var tag = $('#instagramTag').val();
        var feed = new Instafeed({
            get: 'tagged',
            tagName: tag,
            clientId: 'd2cb9e2c87414d6c89ea797b308234dd',
            accessToken: '3316781744.d2cb9e2.9d5f8bab14a447658840aa594554a230',

            /*get: 'user',
             userId: 3722752,
             accessToken: '6738730252.d48c86a.2247d521b6644df9962ec01877f9c0c3',*/
            resolution: 'low_resolution',

            success: function (data) {
                /// console.log(data);
            },
            /*  before: function () {
             console.log('before');
             },
             after: function () {
             console.log('after');
             },*/

            template: '<div class="item"><a href="{{link}}" target="_blank"><img width="320" height="320"  src="{{image}}" /></a></div>',
            target: 'instafeed',
            after: function() {
                $('.owl-carousel-feed').owlCarousel({
                    items:4,
                    loop:true,
                    margin:0,
                    dots: false,
                    nav: true,
                    /*responsive:{
                     0:{
                     items:4
                     }
                     },*/
                    center: true,
                    autoWidth:true,
                    autoplay:true,
                    autoplayTimeout:1000,
                    autoplayHoverPause:true,
                });

            }
        });
        feed.run();
    });
</script>
<script>

    $('.loop').owlCarousel({
        center: true,
        items:2,
        loop:true,
        dots: false,
        nav: false,
        margin:5,
        responsive:{
            600:{
                items:4
            }
        }
    });
    $('.nonloop').owlCarousel({
        center: false,
        items:12,
        loop:false,
        dots: false,
        nav: false,
        margin:90,
        autoWidth:true,

        responsive:{
            600:{
                items:12
            }
        }
    });
    $('.owl-logo').owlCarousel({
        items:8,
        loop:true,
        margin:5,
        dots: false,
        nav: false,
        0:{
            items:2
        },
        768:{
            items:6
        },
        990:{
            items:8
        },
        1024:{
            items:8
        },
        center: true,
        autoWidth:true,
        autoplay:true,
        autoplayTimeout:1000,
        autoplayHoverPause:true,
    });

    $('.owl-video').owlCarousel({
        items:1,
        merge:true,
        loop:true,
        margin:10,
        dots: true,
        autoWidth:true,
        nav: false,
        video: true,
        singleItem:true,
        autoHeight:true,
        videoHeight: 500,
        videoWidth: 600,
        lazyLoad:true,
        center:true,
        responsive:{
            480:{
                items:2
            },
            600:{
                items:4
            }
        }
    });
</script>

</body>
</html>
