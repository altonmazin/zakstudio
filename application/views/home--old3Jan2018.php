<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zak</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link id="favicon" rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon144.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon114.png">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/icon-font.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/social-icons.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/frontend-grid.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/js/owl.carousel/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/mobile.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/slider.css">
    <!--[if IE]>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/mootools/1.4.0/mootools-yui-compressed.js"></script>
    <script type="text/javascript" src="js/selectivizr-min.js"></script>
    <script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>
<style>

    /* Instagram CSS Styles */
    .owl-controls {
        text-align: center;
    }
    .owl-next, .owl-prev {
        margin: 5px;
        font-size: 14px;
        text-decoration: none;
        display: inline-block;
        background: #3c3c3b;
        color: #fff;
        text-transform: uppercase;
        /* border-radius: 2px; */
        overflow: hidden;
        line-height: 1.2em;
        padding: 11px 20px 11px;
        position: relative;
        z-index: 1;
        font-weight: bold;
        -webkit-transition: all 0.3s ease;
        -moz-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        -ms-transition: all 0.3s ease;
        transition: all 0.3s ease;
        cursor: pointer;
        /* min-width: 170px; */
        text-align: center;
        border: 2px solid #3c3c3b;
        -moz-box-sizing: border-box;
        -ms-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -o-box-sizing: border-box;
        box-sizing: border-box;
    }

    .owl-next:hover, .owl-prev:hover {
        color: #3c3c3b;
        background: #fff;
    }
    }
    .owl-dot {
        /*display: inline-block;*/
        display: none !important;
        border: solid 2px #9E5A5A;
        width: 15px;
        height: 15px;
        margin: 5px;

        border-radius: 50%;
    }
    .owl-dot.active {
        background: #CAB4B4;
    }
</style>
<style>
    div#masonry {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        height: 100vw;
        max-height: 800px;
        font-size: 0;
    }
    div#masonry img {
        width: 33.3%;
        -webkit-transition: .8s opacity;
        transition: .8s opacity;
        margin:2px;
    }

    div#masonry:hover img { opacity: 0.3; }
    div#masonry:hover img:hover { opacity: 1; }

    /* fallback for earlier versions of Firefox */

    @supports not (flex-wrap: wrap) {
        div#masonry { display: block; }
        div#masonry img {
            display: inline-block;
            vertical-align: top;
        }
    }

</style>
<div id="page">
    <!-- Page preloader -->
    <div id="page-preloader">
        <div class="spinner centered-container"></div>
    </div>
    <!-- END Page preloader -->
    <!-- Page header -->
    <header class="header dark-bg clearfix">
        <div class="fw-container">
            <div class="fw-main-row">
                <div class="fl logo-area"><a href="/"><img src="#" class="logo-dark" alt="zak logo"><img src="#" class="logo-light" alt="zak logo"></a></div>
                <div class="fr">
                    <!-- Site navigation -->
                    <nav class="navigation">
                        <ul>
                            <li class="current-menu-item menu-item-has-children">
                                <a href="/">HOME</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                        <li class="current-menu-item"><a href="photographer.html">Photographer</a></li>
                                        <li><a href="photostudio.html">Photo Studio</a></li>
                                        <li><a href="photoschool.html">Photo School</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="javascript:void(0);">PAGES</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                        <li><a href="about.html">About</a></li>
                                        <li><a href="typography.html">Typography</a></li>
                                        <li><a href="404.html">404 error</a></li>
                                        <li><a href="coming-soon.html">Coming soon</a></li>
                                        <li class="menu-item-has-children">
                                            <a href="javascript:void(0);">Menu level</a>
                                            <div class="sub-nav">
                                                <ul class="sub-menu">
                                                    <li class="menu-item-has-children">
                                                        <a href="javascript:void(0);">Hyperlink 1</a>
                                                        <div class="sub-nav">
                                                            <ul class="sub-menu">
                                                                <li><a href="javascript:void(0);">Hyperlink 1</a></li>
                                                                <li><a href="javascript:void(0);">Hyperlink 2</a></li>
                                                                <li><a href="javascript:void(0);">Hyperlink 3</a></li>
                                                                <li><a href="javascript:void(0);">Hyperlink 4</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item-has-children">
                                                        <a href="javascript:void(0);">Hyperlink 2</a>
                                                        <div class="sub-nav">
                                                            <ul class="sub-menu">
                                                                <li><a href="javascript:void(0);">Hyperlink 1</a></li>
                                                                <li><a href="javascript:void(0);">Hyperlink 2</a></li>
                                                                <li><a href="javascript:void(0);">Hyperlink 3</a></li>
                                                                <li><a href="javascript:void(0);">Hyperlink 4</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="javascript:void(0);">Hyperlink 3</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li><a href="services.html">Services</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="javascript:void(0);">Gallery</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                        <li class="menu-item-has-children">
                                            <a href="javascript:void(0);">Grid</a>
                                            <div class="sub-nav">
                                                <ul class="sub-menu">
                                                    <li><a href="grid-col2.html">Col 2</a></li>
                                                    <li><a href="grid-col3.html">Col 3</a></li>
                                                    <li><a href="grid-col4.html">Col 4</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="javascript:void(0);">Masonry</a>
                                            <div class="sub-nav">
                                                <ul class="sub-menu">
                                                    <li><a href="masonry-col2.html">Col 2</a></li>
                                                    <li><a href="masonry-col3.html">Col 3</a></li>
                                                    <li><a href="masonry-col4.html">Col 4</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li><a href="gallery-horizontal.html">Horizontal</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="javascript:void(0);">Blog</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                        <li><a href="blog1.html">Variant 1</a></li>
                                        <li><a href="blog2.html">Variant 2</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li><a href="contact.html">CONTACT</a></li>
                        </ul>
                    </nav>
                    <!-- END Site navigation -->
                    <!-- Search module -->
                    <div class="search-top">
                        <form class="st-wrap">
                            <input type="text" placeholder="Type and hit enter" name="search" class="input"><button class="st-button icon-font icon-search" type="submit"></button>
                        </form>
                    </div>
                    <!-- END Search module -->
                    <!-- Side area button -->
                    <div class="side-area-btn">
                        <div></div>
                    </div>
                    <!-- END Side area button -->
                </div>
            </div>
        </div>
    </header>
    <!-- END Page header -->
    <!-- Side panel -->
    <section class="side-panel dark-bg">
        <div class="close icon-font icon-cancel-1"></div>
        <header class="sp-top">
            <div class="logo-area"><a href="#"><img src="images/logo-light.png" class="logo-light" alt="Crop It"></a></div>
        </header>
        <div class="sp-wrap">
            <div>
                <h5 class="heading-decor2">gallery</h5>
                <!-- Gallery thumbnails -->
                <div class="gallery-module fw-row">
                    <div class="fw-col-xs-6 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 1"></a></div>
                    <div class="fw-col-xs-6 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 2"></a></div>
                    <div class="fw-col-xs-6 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 3"></a></div>
                    <div class="fw-col-xs-6 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 4"></a></div>
                    <div class="fw-col-xs-6 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 5"></a></div>
                    <div class="fw-col-xs-6 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 6"></a></div>
                    <div class="fw-col-xs-6 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 7"></a></div>
                    <div class="fw-col-xs-6 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 8"></a></div>
                    <div class="fw-col-xs-6 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 9"></a></div>
                </div>
                <h5 class="heading-decor2">contacts</h5>
                <!-- Contact rows -->
                <div class="contact-row">
                    <div class="label">Address:</div>
                    <div class="value">346A State Street, Brooklyn, NY 11217</div>
                </div>
                <div class="contact-row">
                    <div class="label">Phone:</div>
                    <div class="value">+1 (800) 123 45 67 - Office</div>
                </div>
                <div class="contact-row">
                    <div class="label">Email:</div>
                    <div class="value"><a href="mailto:support@promo-theme.com">support@promo-theme.com</a></div>
                </div>
                <!-- Social links -->
                <div class="social-links">
                    <span>follow us:</span>
                    <a href="javascript:void(0);"><i class="social-icons icon-facebook-logo"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-instagram-social-network-logo-of-photo-camera"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-twitter-logo"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-pinterest-social-logo"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-youtube-logotype"></i></a>
                </div>
                <h5 class="heading-decor2">stay in touch</h5>
                <!-- Subscribe form -->
                <form action="javascript:void(0);" class="subscribe-form"><input type="text" placeholder="enter your e-mail" class="input"><button type="submit" class="button-style1 white"><span>subscribe</span></button></form>
            </div>
        </div>
        <footer class="sp-bottom">Copyright © 2016 Promo-themes. All Rights Reserved.</footer>
    </section>
    <!-- END Side panel -->
    <!-- Full scren slider -->
    <section class="full-screen-slider">
        <!-- slider design 1 -->
        <div class="item dark-bg htmlNoPages" >
            <div class="gwd-div-1m5d gwd-gen-f5uugwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://keyassets.timeincuk.net/inspirewp/live/wp-content/uploads/sites/12/2015/03/Canon-EF-11-24mm-f4L-USM-wide-angle-view.jpg');
					}"></div>
            <div class="gwd-div-1xq9 gwd-gen-z3cbgwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/6.jpg');
					}"></div>
            <div class="gwd-div-1kvs gwd-gen-1okwgwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/7.jpg');
					}"></div>
            <div class="gwd-div-19e4 gwd-gen-1qp0gwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/1.jpg');
					}"></div>

        </div>
        <!-- slider design 2 -->
        <div class="item dark-bg htmlNoPages" >
            <div class="gwd-div-1m5d-design2 gwd-gen-f5uugwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://keyassets.timeincuk.net/inspirewp/live/wp-content/uploads/sites/12/2015/03/Canon-EF-11-24mm-f4L-USM-wide-angle-view.jpg');
					}"></div>
            <div class="gwd-div-1xq9-design2 gwd-gen-z3cbgwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/6.jpg');
					}"></div>
            <div class="gwd-div-1kvs-design2 gwd-gen-1okwgwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/7.jpg');
					}"></div>
            <div class="gwd-div-19e4-design2 gwd-gen-1qp0gwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/1.jpg');
					}"></div>

        </div>
        <!-- slider design 1 -->
        <div class="item dark-bg htmlNoPages" >
            <div class="gwd-div-1m5d gwd-gen-f5uugwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://keyassets.timeincuk.net/inspirewp/live/wp-content/uploads/sites/12/2015/03/Canon-EF-11-24mm-f4L-USM-wide-angle-view.jpg');
					}"></div>
            <div class="gwd-div-1xq9 gwd-gen-z3cbgwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/6.jpg');
					}"></div>
            <div class="gwd-div-1kvs gwd-gen-1okwgwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/7.jpg');
					}"></div>
            <div class="gwd-div-19e4 gwd-gen-1qp0gwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/1.jpg');
					}"></div>

        </div>
        <!-- slider design 2 -->
        <div class="item dark-bg htmlNoPages" >
            <div class="gwd-div-1m5d-design2 gwd-gen-f5uugwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg');
					}"></div>
            <div class="gwd-div-1xq9-design2 gwd-gen-z3cbgwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/6.jpg');
					}"></div>
            <div class="gwd-div-1kvs-design2 gwd-gen-1okwgwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/7.jpg');
					}"></div>
            <div class="gwd-div-19e4-design2 gwd-gen-1qp0gwdanimation" style="
						background-position: center center;
						background-clip: border-box;
						background-attachment: local;
						background-blend-mode: normal;
						background-size: cover;
						background-image: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/12.jpg');
					}"></div>

        </div>
    </section>
    <!-- END Full scren slider -->
    <!-- Info section -->
    <section class="fw-main-row padding-on info-section" style="background: #ededed;">
        <div class="fw-container full">
            <!-- Info section image -->
            <div class="fw-col-xs-12 fw-col-md-4 fw-col-lg-6 is-col">
                <div class="side-image" style="background-image: url(<?php echo base_url('uploads/pages/' . $aboutSec['file_name']); ?>);">
                    <img src="<?php echo base_url('uploads/pages/' . $aboutSec['file_name']); ?>" alt="About Me">
                </div>
            </div>
            <!-- END Info section image -->
            <!-- Info section text -->
            <div class="fw-col-xs-12 fw-col-md-8 fw-col-lg-6 is-col tac">
                <div class="is-text clearfix">
                    <h5 style="margin-bottom:60px;"><?php echo $aboutSec['top_text']; ?></h5>
                    <h1 class="heading-decor"><?php echo $aboutSec['title_text']; ?></h1>
                    <p><?php echo $aboutSec['description_text']; ?></p>
                    <a href="javascript:void(0);" class="button-style1" style="margin-top: 45px;margin-bottom: 65px;"><span><?php echo $aboutSec['button_text'] != '' ? $aboutSec['button_text'] : 'read all about';?></span></a>
                </div>
            </div>
            <!-- END Info section text -->
        </div>
    </section>
    <!-- END Info section -->
    <!-- Portfolio categories -->
    <div class="fw-main-row" style="padding: 15px 0;">
        <div class="fw-container full">
            <?php
            if(($albumRecords)) {

               foreach($albumRecords as $record) { ?>
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-4 fw-col-lg-3">
                <div class="portfolio-cat-item" style="background-image: url('<?php echo base_url('albumUploads/albumImages/' . $record->file_name); ?>');"><a href="javascript:void(0);"><span><?php echo $record->title;?></span></a></div>
            </div>
            <?php }
            }?>



        </div>
    </div>
    <!-- END Portfolio categories -->
    <!-- Instagram photo slider -->
    <section class="fw-main-row dark-bg" style="padding: 30px 0 0;">
        <div class="fw-container full">
            <div class="fw-col-xs-12">
                <h2 class="heading-decor2 normal"><span>my instagram</span> <a href="javascript:void(0);" class="button-style1 fr hd-btn"><i class="social-icons icon-instagram-social-network-logo-of-photo-camera"></i><span>follow us</span></a></h2>
            </div>
            <div class="fw-row">
                <!--<div class="ig-carousel" id="instagramSlides" >

                </div>-->


            </div>
        </div>
        <input type="hidden" name="instagramTag" id="instagramTag" value="<?php echo $instagramObj[0]->tag;?>">
        <div id="instafeed" class="instafeed owl-carousel"></div>
    </section>
    <!-- END Instagram photo slider -->

    <!-- Big banner -->
    <section class="fw-main-row dark-bg" style="height:470px; margin: 20px 0 20px 0; background-image: url('<?php echo base_url('uploads/pages/' . $readySec['file_name']); ?>');">
        <div class="fw-container centered-container tal">
            <h5><?php echo $readySec['upper_text'] ?></h5>
            <h1 class="heading-decor"><?php echo $readySec['title_text'] ?></h1>
            <p><i><?php echo $readySec['subtitle_text'] ?></i></p>
            <a href="javascript:void(0);" class="button-style1 white"><span><?php echo $readySec['button_text'] !='' ? $readySec['button_text']: 'Submit'; ?></span></a>
        </div>
    </section>
    <!-- END Big banner -->

    <!-- Logos -->
    <div class="fw-main-row" style="padding: 0 0 50px;">
        <div class="fw-container">
            <div class="fw-row">
                <div class="ig-carousel">
                    <?php if ($busniesslogos): ?>
                        <?php foreach ($busniesslogos as $row): ?>
                            <div class="item" style=" margin: 10px; background-image: url(<?php echo base_url('uploads/logos/' . $row['file_name']); ?>);">
                                <?php if($row['url'] != '') {  ?>
                                        <a target="_blank" style="cursor: pointer" href="<?php echo $row['url'];?>"></a>
                                <?php } ?>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
    <!-- END Logos -->
    <!-- Footer -->
    <footer class="footer dark-bg">
        <div class="fw-container">
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-3">
                <div class="h5 heading-decor2">contacts</div>
                <!-- Contact rows -->
                <div class="contact-row">
                    <div class="label">Address:</div>
                    <div class="value">346A State Street, Brooklyn, NY 11217</div>
                </div>
                <div class="contact-row">
                    <div class="label">Phone:</div>
                    <div class="value">+1 (800) 123 45 67 - Office</div>
                </div>
                <div class="contact-row">
                    <div class="label">Email:</div>
                    <div class="value"><a href="mailto:support@promo-theme.com">support@promo-theme.com</a></div>
                </div>
            </div>
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-3">
                <div class="h5 heading-decor2">gallery</div>
                <!-- Gallery thumbnails -->
                <div class="gallery-module fw-row">
                    <div class="fw-col-xs-4 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 1"></a></div>
                    <div class="fw-col-xs-4 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 2"></a></div>
                    <div class="fw-col-xs-4 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 3"></a></div>
                    <div class="fw-col-xs-4 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 4"></a></div>
                    <div class="fw-col-xs-4 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 5"></a></div>
                    <div class="fw-col-xs-4 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="http://placehold.it/100x100" alt="gallery item 6"></a></div>
                </div>
            </div>
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-2">
                <div class="h5 heading-decor2">LINKS</div>
                <!-- Footer nav -->
                <nav class="footer-nav">
                    <ul>
                        <li><a href="photographer.html">Home Page</a></li>
                        <li><a href="about.html">About Us</a></li>
                        <li><a href="services.html">Services</a></li>
                        <li><a href="gallery1.html">Gallery</a></li>
                        <li><a href="blog1.html">Blog</a></li>
                        <li><a href="contact.html">Contact</a></li>
                    </ul>
                </nav>
            </div>
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-4">
                <div class="h5 heading-decor2">stay in touch</div>
                <!-- Subscribe form -->
                <form action="javascript:void(0);" class="subscribe-form"><input type="text" placeholder="Enter your e-mail" class="input"><button type="submit" class="button-style1"><span>subscribe</span></button></form>
                <!-- Social links -->
                <div class="social-links">
                    <span>follow us:</span>
                    <a href="javascript:void(0);"><i class="social-icons icon-facebook-logo"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-instagram-social-network-logo-of-photo-camera"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-twitter-logo"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-pinterest-social-logo"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-youtube-logotype"></i></a>
                </div>
            </div>
        </div>
    </footer>
    <!-- END Footer -->
</div>
<script src="<?php echo base_url('assets/user_panel'); ?>/js/jquery-2.1.3.min.js"></script>
<script src="<?php echo base_url('assets/user_panel'); ?>/js/instafeed.min.js"></script>
<script src="<?php echo base_url('assets/user_panel'); ?>/js/owl.carousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url('assets/user_panel'); ?>/js/jquery.animateNumber.min.js"></script>
<script src="<?php echo base_url('assets/user_panel'); ?>/js/script.js"></script>

<script>
    jQuery( document ).ready(function($) {
        var tag = $('#instagramTag').val();
        var feed = new Instafeed({
            get: 'tagged',
            tagName: tag,
            clientId: 'd48c86a9f5f94644a2c776e526dda0a4',
            accessToken: '6738730252.d48c86a.2247d521b6644df9962ec01877f9c0c3',

            /*get: 'user',
            userId: 3722752,
            accessToken: '6738730252.d48c86a.2247d521b6644df9962ec01877f9c0c3',*/
            resolution: 'standard_resolution',

            success: function (data) {
                //console.log(data);
            },
          /*  before: function () {
                console.log('before');
            },
            after: function () {
                console.log('after');
            },*/

            template: '<div class="item"><img src="{{image}}" /></div>',
            target: 'instafeed',
            after: function() {
                $('.owl-carousel').owlCarousel({
                    items:10,
                    loop:true,
                    margin:10,
                    nav: true,
                    autoplay:true,
                    autoplayTimeout:1000,
                    autoplayHoverPause:true,
                });

            }
        });
        feed.run();
    });
</script>
<script>
   /* $(document).ready(function() {
        var token = '6738730252.d48c86a.2247d521b6644df9962ec01877f9c0c3',
            hashtag='random', // hashtag without # symbol
            num_photos = 20;

        $.ajax({
            url: 'https://api.instagram.com/v1/tags/'+  hashtag + '/media/recent',
            dataType: 'jsonp',
            type: 'GET',
            data: {access_token: token, count: num_photos},
            success: function(data){
                console.log(data);
                /!*for(x in data.data){
                    $('#instagramSlides').append('<div class="item"><img src="'+data.data[x].images.thumbnail.url+'"></div>');
                    console.log(data.data[x].images.thumbnail.url);
                }*!/
                $('#instagramSlides').append(' <div class="item" ><img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/2.jpg"> </div>');
            },
            error: function(data){
                console.log(data);
            }
        });
    })*/

</script>
</body>
</html>
