<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zak</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link id="favicon" rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon144.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon114.png">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/icon-font.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/social-icons.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/frontend-grid.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/js/owl.carousel/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/mobile.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/slider.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/lightGallery'); ?>/css/lightgallery.css">

    <!--[if IE]>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/mootools/1.4.0/mootools-yui-compressed.js"></script>
    <script type="text/javascript" src="js/selectivizr-min.js"></script>
    <script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>
<style>

    /* Instagram CSS Styles */
    .owl-controls {
        text-align: center;
    }
    .owl-next, .owl-prev {
        margin: 5px;
        font-size: 14px;
        text-decoration: none;
        display: inline-block;
        background: #3c3c3b;
        color: #fff;
        text-transform: uppercase;
        /* border-radius: 2px; */
        overflow: hidden;
        line-height: 1.2em;
        padding: 11px 20px 11px;
        position: relative;
        z-index: 1;
        font-weight: bold;
        -webkit-transition: all 0.3s ease;
        -moz-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        -ms-transition: all 0.3s ease;
        transition: all 0.3s ease;
        cursor: pointer;
        /* min-width: 170px; */
        text-align: center;
        border: 2px solid #3c3c3b;
        -moz-box-sizing: border-box;
        -ms-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -o-box-sizing: border-box;
        box-sizing: border-box;
    }

    .owl-next:hover, .owl-prev:hover {
        color: #3c3c3b;
        background: #fff;
    }
    }
    .owl-dot {
        /*display: inline-block;*/
        display: none !important;
        border: solid 2px #9E5A5A;
        width: 15px;
        height: 15px;
        margin: 5px;

        border-radius: 50%;
    }
    .owl-dot.active {
        background: #CAB4B4;
    }
    /* Load more */
    .load-more{
        background: #302D2A;
        text-align: center;
        color: white;
        padding: 10px 0px;
        font-family: sans-serif;
    }

    #inner {
        display: table;
        margin: 0 auto;
        width: 210px;
        margin-top: 100px;
        border-radius: 36px;
    }

    #innerAll {
        display: table;
        margin: 0 auto;
        width: 210px;
        margin-top: 100px;
        border-radius: 36px;
    }

    .load-more:hover{
        cursor: pointer;
    }

    /* more link */
    .more{
        color: blue;
        text-decoration: none;
        letter-spacing: 1px;
        font-size: 16px;
    }
</style>
<style>
    div#masonry {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        height: 100vw;
        max-height: 800px;
        font-size: 0;
    }
    div#masonry img {
        width: 33.3%;
        -webkit-transition: .8s opacity;
        transition: .8s opacity;
        margin:2px;
    }

    div#masonry:hover img { opacity: 0.3; }
    div#masonry:hover img:hover { opacity: 1; }

    /* fallback for earlier versions of Firefox */

    @supports not (flex-wrap: wrap) {
        div#masonry { display: block; }
        div#masonry img {
            display: inline-block;
            vertical-align: top;
        }
    }

    /*img.lazy {
        width: 700px;
        height: 467px;
        display: block;

        !* optional way, set loading as background *!
        background-image: url('../../assets/images/loading.gif');
        background-repeat: no-repeat;
        background-position: 50% 50%;
    }*/
    
    .side-panel {
        width: 365px;
    }

</style>
<div id="page">
    <!-- Page preloader -->
    <div id="page-preloader">
        <div class="spinner centered-container"></div>
    </div>
    <!-- END Page preloader -->
    <!-- Page header -->
    <header class="header clearfix" style="position: static">
        <div class="fw-container">
            <div class="fw-main-row">
                <div class="fl logo-area"><a href="/"><img src="#" class="logo-dark" alt="zak logo"><img src="#" class="logo-light" alt="zak logo"></a></div>
                <div class="fr">
                    <!-- Site navigation -->
                    <nav class="navigation">
                        <ul>
                            <li class="current-menu-item menu-item-has-children">
                                <a href="<?php echo base_url();?>">HOME</a>
                               <!-- <div class="sub-nav">
                                    <ul class="sub-menu">
                                        <li class="current-menu-item"><a href="photographer.html">Photographer</a></li>
                                        <li><a href="photostudio.html">Photo Studio</a></li>
                                        <li><a href="photoschool.html">Photo School</a></li>
                                    </ul>
                                </div>-->
                            </li>
                            <!--<li class="menu-item-has-children">
                                <a href="javascript:void(0);">PAGES</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                        <li><a href="about.html">About</a></li>
                                        <li><a href="typography.html">Typography</a></li>
                                        <li><a href="404.html">404 error</a></li>
                                        <li><a href="coming-soon.html">Coming soon</a></li>
                                        <li class="menu-item-has-children">
                                            <a href="javascript:void(0);">Menu level</a>
                                            <div class="sub-nav">
                                                <ul class="sub-menu">
                                                    <li class="menu-item-has-children">
                                                        <a href="javascript:void(0);">Hyperlink 1</a>
                                                        <div class="sub-nav">
                                                            <ul class="sub-menu">
                                                                <li><a href="javascript:void(0);">Hyperlink 1</a></li>
                                                                <li><a href="javascript:void(0);">Hyperlink 2</a></li>
                                                                <li><a href="javascript:void(0);">Hyperlink 3</a></li>
                                                                <li><a href="javascript:void(0);">Hyperlink 4</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item-has-children">
                                                        <a href="javascript:void(0);">Hyperlink 2</a>
                                                        <div class="sub-nav">
                                                            <ul class="sub-menu">
                                                                <li><a href="javascript:void(0);">Hyperlink 1</a></li>
                                                                <li><a href="javascript:void(0);">Hyperlink 2</a></li>
                                                                <li><a href="javascript:void(0);">Hyperlink 3</a></li>
                                                                <li><a href="javascript:void(0);">Hyperlink 4</a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li><a href="javascript:void(0);">Hyperlink 3</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li><a href="services.html">Services</a></li>
                                    </ul>
                                </div>
                            </li>-->
                            <li class="menu-item-has-children">
                                <a href="<?php echo site_url();?>home/getAllAlbumImages">Gallery</a>
                                <!--<div class="sub-nav">
                                    <ul class="sub-menu">
                                        <li class="menu-item-has-children">
                                            <a href="javascript:void(0);">Grid</a>
                                            <div class="sub-nav">
                                                <ul class="sub-menu">
                                                    <li><a href="grid-col2.html">Col 2</a></li>
                                                    <li><a href="grid-col3.html">Col 3</a></li>
                                                    <li><a href="grid-col4.html">Col 4</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="javascript:void(0);">Masonry</a>
                                            <div class="sub-nav">
                                                <ul class="sub-menu">
                                                    <li><a href="masonry-col2.html">Col 2</a></li>
                                                    <li><a href="masonry-col3.html">Col 3</a></li>
                                                    <li><a href="masonry-col4.html">Col 4</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li><a href="gallery-horizontal.html">Horizontal</a></li>
                                    </ul>
                                </div>-->
                            </li>
                            <!--<li class="menu-item-has-children">
                                <a href="javascript:void(0);">Blog</a>
                                <div class="sub-nav">
                                    <ul class="sub-menu">
                                        <li><a href="blog1.html">Variant 1</a></li>
                                        <li><a href="blog2.html">Variant 2</a></li>
                                    </ul>
                                </div>
                            </li>-->
                            <li><a href="#">CONTACT</a></li>
                        </ul>
                    </nav>
                    <!-- END Site navigation -->


                </div>
            </div>
        </div>
    </header>
    <!-- END Page header -->
    <!-- Side panel -->
    <?php $this->view('sidebar'); ?>
    <!-- END Side panel -->

    <!-- Gallery section starts here -->
    <section class="fw-main-row" style="padding: 40px 0;">
        <div class="fw-container full">
            <h1 class="heading-decor">Gallery</h1>
            <!-- Filter buttons -->
            <div class="button-group filter-button-group">
                <button id="allAlbum" onclick="showAlbum('')" class="<?php echo ($allAlbums === true) ? 'active-tab': '';?>" >all posts</button>
                <?php if(!empty($albumRecords)) {
                    foreach ($albumRecords as $record) {  ?>

                    
            <button id="<?php echo $record->albumId;?>" onclick="showAlbum(<?php echo $record->albumId;?>)"  <?php if($albumId === $record->albumId) { echo "class='active-tab'"; } ?>><?php echo $record->title;?></button>
                <?php } } ?>

            </div>
            <!-- END Filter buttons -->

            <!-- END Filter buttons -->

                <!-- Gallery item -->
                <!--<div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-4 fashion people">
                    <a href="single-gallery.html">
                        <img src="http://placehold.it/1000x600" alt="Gallery item">
                        <div>
                            <div class="name">Angelina Stair</div>
                            <div class="text">Image strip with disabled pop-up functionality</div>
                        </div>
                    </a>
                </div>-->
                <div id="albumSection">
          <div id="albumDiv">
               <?php if(!empty($albumImageRecords)) {  ?>
                       <div class="fw-row masonry" id="galleryImages">
                           <div id="selector1">
                             <?php
                                foreach($albumImageRecords as $record){ ?>

                                        <div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-4 item" data-src="<?php echo base_url().'uploads/files/'.$record->albumId.'/'.$record->file_name;?>">


                                                <img style="cursor: pointer" src="<?php echo base_url().'uploads/files/thumb/'.$record->albumId.'/'.$record->file_name;?>" alt="">


                                        </div>
                                    <?php  }?>
                           </div>

                                 </div>

                 <?php   }  else { ?>

                      <div class="" style=" margin: 80px; text-align: center" ><h4>No album images availble at the moment.</h4></div>


            <?php } ?>
                <!-- END Gallery item -->

        
        <?php if($allcount > 6) {?>
        <div id="outer" style="width:100%">
            <div class="load-more" id="<?php echo ($allAlbums == false) ? 'inner' : 'innerAll';?>">Load More....</div>
        </div>
        <?php } ?>

        <input type="hidden" id="row" value="0">
        <?php if($allAlbums === false) { ?>
        <input type="hidden" id="albumId" value="<?php echo $albumId; ?>">
              <?php } ?>
        <input type="hidden" id="all" value="<?php echo $allcount; ?>">
      </div>
            </div>
                <div id="loadingGif" style="display: none; text-align: center;"><img src="<?php echo base_url('assets'); ?>/images/loading.gif"> </div>
          </div>
             </section>
    <!-- Gallery section end here -->
    <!-- Footer -->
    <?php $this->view('footer'); ?>
    <!-- END Footer -->
</div>

<script src="<?php echo base_url('assets/user_panel'); ?>/js/jquery-2.1.3.min.js"></script>

<script src="<?php echo base_url('assets/user_panel'); ?>/js/owl.carousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url('assets/user_panel'); ?>/js/jquery.animateNumber.min.js"></script>
<script src="<?php echo base_url('assets/user_panel'); ?>/js/script.js"></script>

<script src="<?php echo base_url('assets/user_panel'); ?>/js/isotope.pkgd.min.js"></script>
<script>
    
    function showAlbum(id) {
        $(".button-group").find(":button").each(function(){
            $(this).removeClass("active-tab");
        });
        if(id != '') {
            $('#'+id).addClass("active-tab");
        } else {
            $('#allAlbum').addClass("active-tab");
        }

       // $(this).addClass("active-tab");

         $('#loadingGif').css('display','block');
        
                $.ajax({
                    url: "<?php echo site_url('home/ajax_get_album_images') ?>",
                    type: 'post',
                    data: {albumId: id},
                    success: function(response){
                        $('#albumDiv').remove();
                       // console.log(response);
                        $('#loadingGif').css('display','none');

                       $('#albumSection').append(response);
                        $('#albumDiv').show().fadeIn("slow");
                        secondBindClick();
                        firstBindClick
                        if(response != '') {
                             $('#selector1').lightGallery({
                                selector: '.item'
                            });
                             $lg = $('#selector1');// destroy
                            $lg.data('lightGallery').destroy(true);
                            $("#selector1").lightGallery({
                                selector: '.item'
                            });

                            d = new Date();

                            $("img").each(function() {
                                imgsrc = this.src;
                                $(this).attr("src", imgsrc+"?"+d.getTime());
                            });
                        }
                       
                            
                    }

                });
        return false;
    }

    $(document).ready(function(){
        // bind event handlers when the page loads.
        firstBindClick();
        secondBindClick();
    });

    function secondBindClick(){
        $('#innerAll').click(function(){
            //console.log('innerAll called');

            var row = Number($('#row').val());
            var allcount = Number($('#all').val());
            row = row + 6;

            if(row <= allcount){
               // console.log('Row', row);
                $("#row").val(row);

                $.ajax({
                    url: "<?php echo site_url('home/ajax_all_album_images') ?>",
                    type: 'post',
                    data: {row:row},
                    beforeSend:function(){
                        $(".load-more").text("Loading...");
                    },
                    success: function(response){

                        // Setting little delay while displaying new content
                        setTimeout(function() {

                            // appending posts after last post with class="post"

                            $lg = $('#selector1');// destroy
                            $lg.data('lightGallery').destroy(true);
                            $("#selector1").append(response).show().fadeIn("slow").lightGallery({
                                selector: '.item'
                            });

                            d = new Date();

                            $("img").each(function() {
                                imgsrc = this.src;
                                $(this).attr("src", imgsrc+"?"+d.getTime());
                            });

                            var rowno = row + 6;

                            // checking row value is greater than allcount or not
                            if(rowno > allcount){

                                // Change the text and background
                                $('.load-more').css('display', 'none');
                                $('.load-more').css("background","darkorchid");
                            }else{
                                $(".load-more").text("Load more");
                            }
                        }, 1000);


                    }

                });
            }else{
               // console.log('Row', row);
                $('.load-more').text("Loading...");

                // Setting little delay while removing contents
                setTimeout(function() {

                    // When row is greater than allcount then remove all class='post' element after 3 element
                    // $("#selector1").append(response).show().fadeIn("slow");

                    // Reset the value of row
                    $("#row").val(0);

                    // Change the text and background
                    $('.load-more').text("Load more");
                    $('.load-more').css("background","#15a9ce");

                }, 2000);


            }

        });
    }

    function firstBindClick(){
        // Load more data
        $('#inner').click(function(){
            var row = Number($('#row').val());
            var albumId = Number($('#albumId').val());
            var allcount = Number($('#all').val());
            row = row + 6;

            if(row <= allcount){
                $("#row").val(row);

                $.ajax({
                    url: "<?php echo site_url('home/ajax_album_images') ?>",
                    type: 'post',
                    data: {row:row, albumId: albumId},
                    beforeSend:function(){
                        $(".load-more").text("Loading...");
                    },
                    success: function(response){

                        // Setting little delay while displaying new content
                        setTimeout(function() {

                            // appending posts after last post with class="post"

                            $lg = $('#selector1');// destroy
                            $lg.data('lightGallery').destroy(true);
                            $("#selector1").append(response).show().fadeIn("slow").lightGallery({
                                selector: '.item'
                            });

                            d = new Date();

                            $("img").each(function() {
                                imgsrc = this.src;
                                $(this).attr("src", imgsrc+"?"+d.getTime());
                            });

                            var rowno = row + 6;

                            // checking row value is greater than allcount or not
                            if(rowno > allcount){

                                // Change the text and background
                                $('.load-more').css('display', 'none');
                                $('.load-more').css("background","darkorchid");
                            }else{
                                $(".load-more").text("Load more");
                            }
                        }, 1000);


                    }

                });
            }else{
                $('.load-more').text("Loading...");

                // Setting little delay while removing contents
                setTimeout(function() {

                    // When row is greater than allcount then remove all class='post' element after 3 element
                    // $("#selector1").append(response).show().fadeIn("slow");

                    // Reset the value of row
                    $("#row").val(0);

                    // Change the text and background
                    $('.load-more').text("Load more");
                    $('.load-more').css("background","#15a9ce");

                }, 2000);


            }

        });

        // Load more data

    }


</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#selector1').lightGallery({
            selector: '.item'
        });
        /*$('*[class^="itemGallery"]').lightGallery({

        });*/
       /* $("div[class$='-itemGallery'],div[class*='-itemGallery ']").lightGallery({

        });*/
    });
</script>
<script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
<script src="<?php echo base_url('assets/lightGallery'); ?>/js/lightgallery-all.min.js"></script>
<script src="<?php echo base_url('assets/lightGallery'); ?>/lib/jquery.mousewheel.min.js"></script>
</body>
</html>
