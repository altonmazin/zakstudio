<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zak</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link id="favicon" rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon144.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon114.png">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/icon-font.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/social-icons.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/frontend-grid.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/js/owl.carousel/owl.carousel.css"><!--
    <link rel="stylesheet" href="<?php /*echo base_url('assets/user_panel'); */?>/js/owl.carousel/owl.theme.css">-->
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/mobile.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/user_panel'); ?>/css/slider.css">


    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/slider'); ?>/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/slider'); ?>/css/demo.css" />

    <link rel="stylesheet" href="<?php echo base_url('assets/slider'); ?>/css/slideshow.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/slider'); ?>/css/slideshow_layouts.css">

    <!--[if IE]>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/mootools/1.4.0/mootools-yui-compressed.js"></script>
    <script type="text/javascript" src="js/selectivizr-min.js"></script>
    <script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script>document.documentElement.className = 'js';</script>

</head>
<body>
<style>
    /*.owl-carousel .item-video{height:700px}*/
    /* Instagram CSS Styles */
    .owl-controls {
        text-align: center;
    }
    .owl-next, .owl-prev {
        margin: 5px;
        font-size: 14px;
        text-decoration: none;
        display: inline-block;
        background: #3c3c3b;
        color: #fff;
        text-transform: uppercase;
        /* border-radius: 2px; */
        overflow: hidden;
        line-height: 1.2em;
        padding: 11px 20px 11px;
        position: relative;
        z-index: 1;
        font-weight: bold;
        -webkit-transition: all 0.3s ease;
        -moz-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        -ms-transition: all 0.3s ease;
        transition: all 0.3s ease;
        cursor: pointer;
        /* min-width: 170px; */
        text-align: center;
        border: 2px solid #3c3c3b;
        -moz-box-sizing: border-box;
        -ms-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -o-box-sizing: border-box;
        box-sizing: border-box;
    }

    .owl-next:hover, .owl-prev:hover {
        color: #3c3c3b;
        background: #fff;
    }
    }
    .owl-dot {
        /*display: inline-block;*/
        display: none !important;
        border: solid 2px #9E5A5A;
        width: 15px;
        height: 15px;
        margin: 5px;

        border-radius: 50%;
    }
    .owl-dot.active {
        background: #CAB4B4;
    }
    .owl-carousel .owl-video-wrapper {
        background-color: #fff;
    }
</style>
<style>
    .owl-wrapper {
        margin: 0 auto;
    }
    div#masonry {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        height: 100vw;
        max-height: 800px;
        font-size: 0;
    }
    div#masonry img {
        width: 33.3%;
        -webkit-transition: .8s opacity;
        transition: .8s opacity;
        margin:2px;
    }

    div#masonry:hover img { opacity: 0.3; }
    div#masonry:hover img:hover { opacity: 1; }

    /* fallback for earlier versions of Firefox */

    @supports not (flex-wrap: wrap) {
        div#masonry { display: block; }
        div#masonry img {
            display: inline-block;
            vertical-align: top;
        }
    }
    .owl-centered{
        margin: auto;
    }
</style>
<div id="page">
    <!-- Page preloader -->
    <div id="page-preloader">
        <div class="spinner centered-container"></div>
    </div>
    <!-- END Page preloader -->

    <!-- Full scren slider -->

    <svg class="hidden">
        <defs>
            <symbol id="icon-arrow" viewBox="0 0 24 24">
                <title>arrow</title>
                <polygon points="6.3,12.8 20.9,12.8 20.9,11.2 6.3,11.2 10.2,7.2 9,6 3.1,12 9,18 10.2,16.8 "/>
            </symbol>
            <symbol id="icon-drop" viewBox="0 0 24 24">
                <title>drop</title>
                <path d="M12,21c-3.6,0-6.6-3-6.6-6.6C5.4,11,10.8,4,11.4,3.2C11.6,3.1,11.8,3,12,3s0.4,0.1,0.6,0.3c0.6,0.8,6.1,7.8,6.1,11.2C18.6,18.1,15.6,21,12,21zM12,4.8c-1.8,2.4-5.2,7.4-5.2,9.6c0,2.9,2.3,5.2,5.2,5.2s5.2-2.3,5.2-5.2C17.2,12.2,13.8,7.3,12,4.8z"/><path d="M12,18.2c-0.4,0-0.7-0.3-0.7-0.7s0.3-0.7,0.7-0.7c1.3,0,2.4-1.1,2.4-2.4c0-0.4,0.3-0.7,0.7-0.7c0.4,0,0.7,0.3,0.7,0.7C15.8,16.5,14.1,18.2,12,18.2z"/>
            </symbol>
            <symbol id="icon-prev" viewBox="0 0 100 50">
                <title>prev</title>
                <polygon points="5.4,25 18.7,38.2 22.6,34.2 16.2,27.8 94.6,27.8 94.6,22.2 16.2,22.2 22.6,15.8 18.7,11.8"/>
            </symbol>
            <symbol id="icon-next" viewBox="0 0 100 50">
                <title>next</title>
                <polygon points="81.3,11.8 77.4,15.8 83.8,22.2 5.4,22.2 5.4,27.8 83.8,27.8 77.4,34.2 81.3,38.2 94.6,25 "/>
            </symbol>
            <symbol id="icon-octicon" viewBox="0 0 24 24">
                <title>octicon</title>
                <path d="M12,2.2C6.4,2.2,1.9,6.7,1.9,12.2c0,4.4,2.9,8.2,6.9,9.6c0.5,0.1,0.7-0.2,0.7-0.5c0-0.2,0-0.9,0-1.7c-2.8,0.6-3.4-1.4-3.4-1.4C5.6,17.1,5,16.8,5,16.8C4.1,16.2,5,16.2,5,16.2c1,0.1,1.5,1,1.5,1c0.9,1.5,2.4,1.1,2.9,0.8c0.1-0.7,0.4-1.1,0.6-1.3c-2.2-0.3-4.6-1.1-4.6-5c0-1.1,0.4-2,1-2.7C6.5,8.8,6.2,7.7,6.7,6.4c0,0,0.8-0.3,2.8,1C10.3,7.2,11.1,7.1,12,7c0.9,0,1.7,0.1,2.5,0.3c1.9-1.3,2.8-1,2.8-1c0.5,1.4,0.2,2.4,0.1,2.7c0.6,0.7,1,1.6,1,2.7c0,3.9-2.4,4.7-4.6,5c0.4,0.3,0.7,0.9,0.7,1.9c0,1.3,0,2.4,0,2.8c0,0.3,0.2,0.6,0.7,0.5c4-1.3,6.9-5.1,6.9-9.6C22.1,6.7,17.6,2.2,12,2.2z" />
            </symbol>
            <!-- From Karen Menezes: https://www.smashingmagazine.com/2015/05/creating-responsive-shapes-with-clip-path/ -->
            <clipPath id="polygon-clip-rhomboid" clipPathUnits="objectBoundingBox">
                <polygon points="0 1, 0.3 0, 1 0, 0.7 1" />
            </clipPath>
        </defs>
    </svg>
    <main style="background-color: #302D2A;">
        <div class="slideshow" tabindex="0">
            <?php
            if($sliderDesign == 'Now or Never') {
                $class = 'slide slide--layout-1';
                $layout = 'layout1';
                $singleImgStyle = '';
            } elseif($sliderDesign == 'Caged Birds') {
                $class = 'slide slide--layout-7';
                $layout = 'layout7';
                $singleImgStyle = '';
            }else {
                $class = 'slide slide--layout-1';
                $layout = 'layout1';
                $singleImgStyle = 'style="width: 100%"';
            } 
            ?>
            <div class="<?php echo $class;?>" data-layout="<?php echo $layout;?>">
                <div class="slide-imgwrap">
                    <?php foreach ($sliderInfo as $slider) { ?>
                        <div class="slide__img" <?php echo $singleImgStyle;?>><div class="slide__img-inner" style="background-position: top center;background-image: url('<?php echo base_url().'sliderUploads/'.$slider->file_name;?>');"></div></div>
                    <?php } ?>

                </div>
                <div class="slide__title">
                    <h3 class="slide__title-main"></h3>
                    <p class="slide__title-sub"></p>
                </div>
            </div><!-- /slide -->
            <div class="<?php echo $class;?>" data-layout="<?php echo $layout;?>">
                <div class="slide-imgwrap">
                    <?php foreach ($sliderInfo as $slider) { ?>
                        <div class="slide__img" <?php echo $singleImgStyle;?>><div class="slide__img-inner" style="background-position: top center; background-image: url('<?php echo base_url().'sliderUploads/'.$slider->file_name;?>');"></div></div>
                    <?php } ?>

                </div>
                <div class="slide__title">
                    <h3 class="slide__title-main"></h3>
                    <p class="slide__title-sub"></p>
                </div>
            </div><!-- /slide -->



            <nav class="slideshow__nav slideshow__nav--arrows">
                <button id="prev-slide" class="btn btn--arrow" aria-label="Previous slide"><svg class="icon icon--prev"><use xlink:href="#icon-prev"></use></svg></button>
                <button id="next-slide" class="btn btn--arrow" aria-label="Next slide"><svg class="icon icon--next"><use xlink:href="#icon-next"></use></svg></button>
            </nav>
        </div>
    </main>


</div>
<!-- END Logos -->

</div>
<script src="<?php echo base_url('assets/user_panel'); ?>/js/jquery-2.1.3.min.js"></script>
<script src="<?php echo base_url('assets/user_panel'); ?>/js/script.js"></script>

<script src="<?php echo base_url('assets/slider'); ?>/js/imagesloaded.pkgd.min.js"></script>
<script src="<?php echo base_url('assets/slider'); ?>/js/anime.min.js"></script>
<script src="<?php echo base_url('assets/slider'); ?>/js/main.js"></script>
<script>
    (function() {
        var slideshow = new MLSlideshow(document.querySelector('.slideshow'));

        document.querySelector('#next-slide').addEventListener('click', function() {
            slideshow.next();
        });

        document.querySelector('#prev-slide').addEventListener('click', function() {
            slideshow.prev();
        });
    })();
</script>


</body>
</html>
