<!-- Footer -->
<footer class="footer dark-bg">
    <div class="fw-container">
        <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-3">
            <div class="h5 heading-decor2">contacts</div>
            <!-- Contact rows -->
            <div class="contact-row">
                <div class="label">Address:</div>
                <div class="value">346A State Street, Brooklyn, NY 11217</div>
            </div>
            <div class="contact-row">
                <div class="label">Phone:</div>
                <div class="value">+1 (800) 123 45 67 - Office</div>
            </div>
            <div class="contact-row">
                <div class="label">Email:</div>
                <div class="value"><a href="mailto:support@promo-theme.com">support@promo-theme.com</a></div>
            </div>
        </div>
        <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-3">
            <div class="h5 heading-decor2">gallery</div>
            <!-- Gallery thumbnails -->
            <div class="gallery-module fw-row">
                <?php
                if(($albumRecords)) {
                    $limit = 1;
                    foreach($albumRecords as $record) {?>
                        <div class="fw-col-xs-4 fw-col-sm-4 item">
                            <a href="<?php echo base_url(); ?>home/albumGallery/<?php echo $record->albumId; ?>"><img width="100px" height="100px" src="<?php echo base_url('albumUploads/albumImages/' . $record->file_name); ?>" alt="<?php echo $record->title;?>"></a>
                        </div>
                        <?php $limit++;
                        if ($limit === 7) {
                            break;
                        }
                    }
                }?>
            </div>
        </div>
        <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-2">
            <div class="h5 heading-decor2">LINKS</div>
            <!-- Footer nav -->
            <nav class="footer-nav">
                <ul>
                    <li><a href="<?php echo base_url();?>">Home Page</a></li>
                    <li><a href="#">About Us</a></li>
                    <!--<li><a href="services.html">Services</a></li>-->
                    <li><a href="<?php echo site_url();?>home/getAllAlbumImages">Gallery</a></li>
                    <!--<li><a href="blog1.html">Blog</a></li>-->
                    <li><a href="contact.html">Contact</a></li>
                </ul>
            </nav>
        </div>
        <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-4">
            <div class="h5 heading-decor2">stay in touch</div>
            <!-- Subscribe form -->
            <form action="javascript:void(0);" class="subscribe-form"><input type="text" placeholder="Enter your e-mail" class="input"><button type="submit" class="button-style1"><span>subscribe</span></button></form>
            <!-- Social links -->
            <div class="social-links">
                <span>follow us:</span>
                <a href="javascript:void(0);"><i class="social-icons icon-facebook-logo"></i></a>
                <a href="javascript:void(0);"><i class="social-icons icon-instagram-social-network-logo-of-photo-camera"></i></a>
                <a href="javascript:void(0);"><i class="social-icons icon-twitter-logo"></i></a>
                <a href="javascript:void(0);"><i class="social-icons icon-pinterest-social-logo"></i></a>
                <a href="javascript:void(0);"><i class="social-icons icon-youtube-logotype"></i></a>
            </div>
        </div>
    </div>
</footer>
<!-- END Footer -->