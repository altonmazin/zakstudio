<!-- Side panel -->
<section class="side-panel dark-bg">
    <div class="close icon-font icon-cancel-1"></div>
    <header class="sp-top">
        <div class="logo-area"><a href="#"><img src="" class="logo-light" alt="Zak Logo"></a></div>
    </header>
    <div class="sp-wrap">
        <div>
            <h5 class="heading-decor2">gallery</h5>
            <!-- Gallery thumbnails -->
            <div class="gallery-module fw-row">
                <?php
                if(($albumRecords)) {
                    $limit = 1;
                    foreach($albumRecords as $record) {?>
                        <div class="fw-col-xs-4 fw-col-sm-4 item">
                            <a href="<?php echo base_url(); ?>home/albumGallery/<?php echo $record->albumId; ?>"><img width="100px" height="100px" src="<?php echo base_url('albumUploads/albumImages/' . $record->file_name); ?>" alt="<?php echo $record->title;?>"></a>
                        </div>
                        <?php $limit++;
                        if ($limit === 10) {
                            break;
                        }
                    }
                }?>
            </div>
            <h5 class="heading-decor2">contacts</h5>
            <!-- Contact rows -->
            <div class="contact-row">
                <div class="label">Address:</div>
                <div class="value">346A State Street, Brooklyn, NY 11217</div>
            </div>
            <div class="contact-row">
                <div class="label">Phone:</div>
                <div class="value">+1 (800) 123 45 67 - Office</div>
            </div>
            <div class="contact-row">
                <div class="label">Email:</div>
                <div class="value"><a href="mailto:support@promo-theme.com">support@promo-theme.com</a></div>
            </div>
            <!-- Social links -->
            <div class="social-links">
                <span>follow us:</span>
                <a href="javascript:void(0);"><i class="social-icons icon-facebook-logo"></i></a>
                <a href="javascript:void(0);"><i class="social-icons icon-instagram-social-network-logo-of-photo-camera"></i></a>
                <a href="javascript:void(0);"><i class="social-icons icon-twitter-logo"></i></a>
                <a href="javascript:void(0);"><i class="social-icons icon-pinterest-social-logo"></i></a>
                <a href="javascript:void(0);"><i class="social-icons icon-youtube-logotype"></i></a>
            </div>
            <h5 class="heading-decor2">stay in touch</h5>
            <!-- Subscribe form -->
            <form action="javascript:void(0);" class="subscribe-form"><input type="text" placeholder="enter your e-mail" class="input"><button type="submit" class="button-style1 white"><span>subscribe</span></button></form>
        </div>
    </div>
    <footer class="sp-bottom">Copyright © <?php echo date('Y');?> Zak Studio. All Rights Reserved.</footer>
</section>
<!-- END Side panel -->