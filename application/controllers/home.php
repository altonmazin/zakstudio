<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Home extends CI_Controller
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('page_model');
        $this->load->model('admin/instagram_model');
        $this->load->model('admin/video_model');
        $this->load->model('admin/slider_model');
    }

    function testSlider($sliderId){

        $this->load->model('admin/slider_model');

        $data['pageTitle'] = 'Zak : Test Slider';
        $data['sliderDesign'] = $this->slider_model->getSliderDesign($sliderId);
        $data['sliderInfo'] = $this->slider_model->getAllSliderImages($sliderId);

        $this->load->view("slider", $data);

    }

    function albumGallery($albumId){
        $this->load->model('admin/album_model');

        $data['pageTitle'] = 'Zak : Album Gallery';
        $data['albumId'] = $albumId;

        $data['albumRecords'] = $this->album_model->getAlbums();



        $data['albumImageRecords'] = $this->album_model->getAlbumImages($albumId);
        $data['allcount'] = $this->album_model->countAlbumImages($albumId);
        $data['allAlbums'] = false;
        $this->load->view("gallery", $data);
    }

    function getAllAlbumImages(){
        $this->load->model('admin/album_model');

        $data['pageTitle'] = 'Zak : All Albums Gallery';
        $data['albumRecords'] = $this->album_model->getAlbums();
        $data['albumId'] = '';


        $data['albumImageRecords'] = $this->album_model->getAllAlbumImages();
        $data['allcount'] = $this->album_model->countAllAlbumImages();
        $data['allAlbums'] = true;
        $this->load->view("gallery", $data);
    }

    function ajax_album_images() {
        $this->load->model('admin/album_model');
        $row = $this->input->post('row');
        $albumId = $this->input->post('albumId');
        $rowperpage = 6;
        $records = $this->album_model->getAlbumImagesBylimit($albumId, $row, $rowperpage);
        $html = '';

        if($records) {
            foreach($records as $data) {
                $html .= '<div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-4 item" data-src="'.base_url().'uploads/files/'.$data->albumId.'/'.$data->file_name.'">
                            <img style="cursor: pointer" src="'.base_url().'uploads/files/thumb/'.$data->albumId.'/'.$data->file_name.'" >
                           </div>';
            }
        }
        echo $html;
    }
    
    function ajax_get_album_images() {
        $records = '';
        $allcount = '';
        $this->load->model('admin/album_model');
       
        $albumId = $this->input->post('albumId');
        
        if($albumId != '') {
            $records = $this->album_model->getAlbumImages($albumId);
            $allcount = $this->album_model->countAlbumImages($albumId);

            $html = '';
            $html .= '<div id = "albumDiv">';
            if($records) {
                $html .= '<div class="fw-row masonry" id="galleryImages">
                           <div id="selector1">';

                foreach($records as $data) {
                    $html .= '<div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-4 item" data-src="'.base_url().'uploads/files/'.$data->albumId.'/'.$data->file_name.'">
                            <img style="cursor: pointer" src="'.base_url().'uploads/files/thumb/'.$data->albumId.'/'.$data->file_name.'" >
                           </div>';
                }
                $html.= '</div></div>';
                $html .= '<input type="hidden" id="row" value="0"><input type="hidden" id="albumId" value="'.$albumId.'"><input type="hidden" id="all" value="'.$allcount.'">';
            } else {
                $html .='<div class="" style=" margin: 80px; text-align: center" ><h4>No album images availble at the moment.</h4></div>';
            }
            if($allcount > 6) {
                $html .='     
        <div id="outer" style="width:100%">
            <div class="load-more" id="inner">Load More....</div>
        </div>';
            }
            $html .='</div>';
        } else {
            $records = $this->album_model->getAllAlbumImages();
            $allcount = $this->album_model->countAllAlbumImages();


            $html = '';
            $html .= '<div id = "albumDiv">';
            if($records) {
                $html .= '<div class="fw-row masonry" id="galleryImages">
                           <div id="selector1">';

                foreach($records as $data) {
                    $html .= '<div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-4 item" data-src="'.base_url().'uploads/files/'.$data->albumId.'/'.$data->file_name.'">
                            <img style="cursor: pointer" src="'.base_url().'uploads/files/thumb/'.$data->albumId.'/'.$data->file_name.'" >
                           </div>';
                }
                $html.= '</div></div>';
                $html .= '<input type="hidden" id="row" value="0"><input type="hidden" id="all" value="'.$allcount.'">';
            } else {
                $html .='<div class="" style=" margin: 80px; text-align: center" ><h4>No album images availble at the moment.</h4></div>';
            }
            if($allcount > 6) {
                $html .='     
        <div id="outer" style="width:100%">
            <div class="load-more" id="innerAll">Load More....</div>
        </div>';
            }
            $html .='</div>';

        }
        
        

        echo $html;
        exit;
    }

    function ajax_all_album_images() {
        $this->load->model('admin/album_model');
        $row = $this->input->post('row');
        $rowperpage = 6;
        $records = '';
        $records = $this->album_model->getAllAlbumImagesBylimit($row, $rowperpage);
        $html = '';

        if($records) {
            foreach($records as $data) {
                $html .= '<div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-4 item" data-src="'.base_url().'uploads/files/'.$data->albumId.'/'.$data->file_name.'">
                            <img style="cursor: pointer" src="'.base_url().'uploads/files/thumb/'.$data->albumId.'/'.$data->file_name.'" >
                           </div>';
            }
        }
        echo $html;
    }
    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $getaboutsec = new Page_model();
        $aboutSec = $getaboutsec->getaboutsec();
        //READY TO START
        $getreadysec = new Page_model();
        $readySec = $getreadysec->getreadysec();
        //busniess logo
        $getbusinesssec = new Page_model();
        $busniessSec = $getbusinesssec->getbusniesssec();

        $slider_obj = new Slider_model();
        $sliders = $slider_obj->getAllSliders();
        $sliders_arr = array();
        foreach($sliders as $slider) {

            $sliders_arr[$slider->sliderId] = $slider_obj->getAllSliderImages($slider->sliderId);

        }
       //  var_dump($sliders_arr);exit;

        // Album Section

        $album = new Page_model();
        $albumRecords = $album->getAlbums();

        // Instagram section
        $instagram = new Instagram_model();
        $instagramObj = $instagram->getInstagramTag();

        $videos = new Video_model();
        $videoRecords = $videos->getVideos();

        $footer = $this->load->view('footer', NULL, TRUE);
        $sidebar = $this->load->view('sidebar', NULL, TRUE);

        $this->load->view('home',
            array(
                'body' => '',
                'aboutSec' => $aboutSec,
                'readySec' => $readySec,
                'busniesslogos' => $busniessSec,
                'instagramObj' => $instagramObj,
                'albumRecords' => $albumRecords,
                'footer' => $footer,
                'sidebar' => $sidebar,
                'videos' => $videoRecords,
                'sliders' => $sliders,
                'sliderImages' => $sliders_arr

            )
        );

    }

}

?>