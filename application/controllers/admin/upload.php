<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Upload extends BaseController {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/album_model');
        $this->load->helper('url');
        $this->isLoggedIn();
    }

    public function index($id)
    {
        $this->global['pageTitle'] = 'Zak : Manage Albums';
        $data['albumId'] = $id;

        $this->loadViews("admin/uploadAlbumImages", $this->global, $data, NULL , NULL);
       // $this->load->view('admin/manageAlbums');
    }

    public function json($albumId)
    {
        $options = [
            'script_url' => site_url('admin/upload/json/'.$albumId.'/'),
            'upload_dir' => APPPATH . '../uploads/files/'.$albumId.'/',
            'upload_url' => site_url('uploads/files/'.$albumId.'/'),
        ];
        $this->load->library('UploadHandler', $options);
    }
}