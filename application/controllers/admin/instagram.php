<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Instagram (InstagramController)
 * Instagram Class to control all instagram related operations.
 * @author : Kamran Khan
 * @version : 1.1
 * @since : 17 December 2017
 */
class Instagram extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/instagram_model');
        $this->isLoggedIn();
    }

    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Zak : Manage Instagram Tags';

        $this->loadViews("admin/manageInstagram", $this->global, NULL , NULL);
    }

    /**
     * This function is used to load the user list
     */
    function instagramListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('admin/instagram_model');
            $data['instagramRecords'] = $this->instagram_model->instagramListing();

            $this->global['pageTitle'] = 'Zak : Manage Instagram Tags';
            $this->loadViews("admin/manageInstagram", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addInstagramTag()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('admin/instagram_model');

            $this->global['pageTitle'] = 'Zak : Add New Instagram Hash Tag';

            $this->loadViews("admin/addInstagramTag", $this->global, '', NULL);
        }
    }



    /**
     * This function is used to add new user to the system
     */
    function processAddInstagramTag()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('tag','Instagram Hash Tag','trim|required|max_length[128]|xss_clean');

            if($this->form_validation->run() == FALSE)
            {
                $this->addInstagramTag();
            }
            else
            {
                $tag = $this->input->post('tag');

                $instagramInfo = array('tag'=>$tag,'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));

                $this->load->model('admin/instagram_model');
                $result = $this->instagram_model->addNewInstagramTag($instagramInfo);

                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Instagram Hash Tag created successfully');
                    redirect('/admin/instagramListing');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Instagram Hash Tag creation failed');
                }

                redirect('/admin/addInstagramTag');
            }
        }
    }


    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editInstagramTag($tagInstagramId = NULL)
    {

        if($this->isAdmin() == TRUE )
        {
            $this->loadThis();
        }
        else
        {
            if($tagInstagramId == null)
            {
                redirect('/admin/instagramListing');
            }


            $data['instagramTagInfo'] = $this->instagram_model->getInstagramInfo($tagInstagramId);

            $this->global['pageTitle'] = 'Zak : Edit Instagram Tag';

            $this->loadViews("admin/editInstagramTag", $this->global, $data, NULL);
        }
    }


    /**
     * This function is used to edit the user information
     */
    function editprocessInstagramTag()
    {

        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $instagramTagId = $this->input->post('instagramTagId');

            $this->form_validation->set_rules('tag', 'Instagram Hash Tag', 'trim|required|max_length[128]|xss_clean');

            if ($this->form_validation->run() == FALSE) {

                $this->editInstagramTag($instagramTagId);
            } else {
                $tag = $this->input->post('tag');

                $instagramTagInfo = array();

                $instagramTagInfo = array('tag' => $tag, 'updatedBy' => $this->vendorId,
                    'updatedDtm' => date('Y-m-d H:i:s'));
            }

            $result = $this->instagram_model->editInstagramTag($instagramTagInfo, $instagramTagId);

            if ($result == true) {
                $this->session->set_flashdata('success', 'Instagram Tag updated successfully');
                redirect('/admin/instagramListing');
            } else {
                $this->session->set_flashdata('error', 'Instagram Tag updation failed');
            }

            redirect('/admin/instagramListing');
        }
    }



    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteInstagramTag()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $instagramTagId = $this->input->post('tagId');
            $instagramTagInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));

            $result = $this->instagram_model->deleteInstagramTag($instagramTagId, $instagramTagInfo);

            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }


}

?>