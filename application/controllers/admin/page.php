<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Instagram (InstagramController)
 * Instagram Class to control all instagram related operations.
 * @author : Kamran Khan
 * @version : 1.1
 * @since : 17 December 2017
 */
class Page extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/page_model');
        $this->isLoggedIn();
    }

    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Zak : Manage Pages';

        $this->loadViews("admin/managePages", $this->global, NULL , NULL);
    }

    /**
     * This function is used to load the user list
     */
    function pageListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('admin/page_model');

            $data['pageRecords'] = $this->page_model->pageListing();

            $this->global['pageTitle'] = 'Zak : Page Section Listing';

            $this->loadViews("admin/managePages", $this->global, $data, NULL);
        }
    }


}

?>