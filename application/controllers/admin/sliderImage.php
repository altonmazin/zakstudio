<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Instagram (InstagramController)
 * Instagram Class to control all instagram related operations.
 * @author : Kamran Khan
 * @version : 1.1
 * @since : 17 December 2017
 */
class SliderImage extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/sliderimage_model');
        $this->isLoggedIn();
    }

    /**
     * This function used to load the first screen of the user
     */
    public function index($sliderId)
    {
        $this->global['pageTitle'] = 'Zak : Manage Slider Images';

        $this->loadViews("admin/manageSliderImages", $this->global, NULL , NULL);
    }

    /**
     * This function is used to load the user list
     */
    function sliderImageListing($sliderId)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('admin/sliderimage_model');
            $data['sliderImageRecords'] = $this->sliderimage_model->sliderImageListing($sliderId);

            $this->global['pageTitle'] = 'Zak : Slider Image Listing';

            $this->loadViews("admin/manageSliderImages", $this->global, $data, NULL);
        }
    }



    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editSliderImage($imgId = NULL)
    {

        if($this->isAdmin() == TRUE )
        {
            $this->loadThis();
        }
        else
        {
            if($imgId == null)
            {
                redirect('/admin/sliderListing');
            }
            $data['sliderImageInfo'] = $this->sliderimage_model->getSliderImageInfo($imgId);

            $this->global['pageTitle'] = 'Zak : Edit Slider Image';

            $this->loadViews("admin/editSliderImage", $this->global, $data, NULL);
        }
    }


    /**
     * This function is used to edit the user information
     */
    function editprocessSliderImage()
    {

        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $imgId = $this->input->post('imgId');
            $sliderId = $this->input->post('sliderId');


            if ($_FILES) {
                //set preferences
                if($_FILES['file_name']['name'] == '') {
                    $this->session->set_flashdata('error', 'Please select slider image');
                    redirect('/admin/editSliderImage/'.$imgId);
                    exit;
                }
                //file upload destination
                $config['upload_path'] = './sliderUploads/';

                $config['allowed_types'] = 'gif|jpg|png';
                //allowed max file size. 0 means unlimited file size
                $config['max_size'] = '0';
                //max file name size
                $config['max_filename'] = '255';
                //whether file name should be encrypted or not
                $config['encrypt_name'] = TRUE;
                // minimun width
                $config['min_width'] = '600';
                $config['min_height'] = '400';


                //store file info once uploaded
                $file_data = array();
                //check for errors
                $is_file_error = FALSE;
                //check if file was selected for upload

                //if file was selected then proceed to upload
                if (!$is_file_error) {
                    //load the preferences
                    $this->load->library('upload', $config);
                    //check file successfully uploaded. 'file_name' is the name of the input
                    if (!$this->upload->do_upload('file_name')) {
                        //if file upload failed then catch the errors
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        $this->editSliderImage($imgId);
                        $is_file_error = TRUE;
                    } else {
                        //store the file info
                        $file_data = $this->upload->data();
                    }
                }
                // There were errors, we have to delete the uploaded files
                if ($is_file_error) {
                    if ($file_data) {
                        $file = './sliderUploads/' . $file_data['file_name'];
                        if (file_exists($file)) {
                            unlink($file);
                        }
                    }
                    redirect('/admin/editSliderImage/'.$imgId);
                    exit;

                }
                if (!$is_file_error) {
                    //save the file info in the database

                    $sliderImageInfo = array('file_name' => $file_data['file_name'],  'file_orig_name' => $file_data['orig_name'],
                        'file_path' => $file_data['full_path'], 'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));

                    $this->load->model('admin/sliderimage_model');
                    $result = $this->sliderimage_model->editSliderImage($sliderImageInfo, $imgId);

                    if($result)
                    {
                        $this->session->set_flashdata('success', 'Slider Image updated successfully');
                        redirect('/admin/sliderImageListing/'.$sliderId);
                    } else {
                        //if file info save in database was not successful then delete from the destination folder
                        $file = './sliderUploads/' . $file_data['file_name'];
                        if (file_exists($file)) {
                            unlink($file);
                        }

                        $this->session->set_flashdata('error', 'Slider image updation failed');
                        redirect('/admin/editSliderImage/'.$imgId);
                        exit;
                    }
                }
            }
            redirect('/admin/sliderImageListing/'.$sliderId);
        }
    }
}

?>