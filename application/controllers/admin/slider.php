<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Instagram (InstagramController)
 * Instagram Class to control all instagram related operations.
 * @author : Kamran Khan
 * @version : 1.1
 * @since : 17 December 2017
 */
class Slider extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/slider_model');
        $this->isLoggedIn();
    }

    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Zak : Manage Header Sliders';

        $this->loadViews("admin/manageSlider", $this->global, NULL , NULL);
    }

    /**
     * This function is used to load the user list
     */
    function sliderListing()
    {

        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('admin/slider_model');

            $this->load->library('pagination');

            $count = $this->slider_model->sliderListingCount();

            $returns = $this->paginationCompress ( "admin/sliderListing/", $count, 10 );

            $data['sliderRecords'] = $this->slider_model->sliderListing($returns["page"], $returns["segment"]);

            $this->global['pageTitle'] = 'Zak : Slider Listing';

            $this->loadViews("admin/manageSliders", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addSlider()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('admin/slider_model');

            $this->global['pageTitle'] = 'Zak : Add New Slider';

            $this->loadViews("admin/addSlider", $this->global, '', NULL);
        }
    }



    /**
     * This function is used to add new user to the system
     */
    function processAddSlider()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('sliderOrder', 'Slider Order', 'trim|required|numeric');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', 'Please enter slider order');
                redirect('/admin/addSlider');
                exit;
            }
            $sliderDesign = $this->input->post('sliderDesign');
            $status = $this->input->post('status');
            $sliderOrder = $this->input->post('sliderOrder');
            //file upload destination
            $config['upload_path'] = './sliderUploads/';
            //allowed file types. * means all types
            $config['allowed_types'] = 'gif|jpg|png';
            //allowed max file size. 0 means unlimited file size
            $config['max_size'] = '0';
            //max file name size
            $config['max_filename'] = '255';
            //whether file name should be encrypted or not
            $config['encrypt_name'] = TRUE;
            // minimun width
            $config['min_width'] = '600';
            $config['min_height'] = '400';


            //store file info once uploaded
            $file_data = array();
            //check for errors
            $is_file_error = FALSE;
            //check if file was selected for upload
            if (!$_FILES) {
                $is_file_error = TRUE;
                $this->session->set_flashdata('error', 'Please upload slider images according to slider design,single Image design needs only one image other needs 3 images');
                redirect('/admin/addSlider');
                exit;
            }

            }
            //if file was selected then proceed to upload
            if (!$is_file_error) {
                //load the preferences
                $this->load->library('upload', $config);
                //check file successfully uploaded. 'file_name' is the name of the input

                $dataInfo = array();
                $files = $_FILES;
                $cpt = count($_FILES['uploaded_files']['name']);
                if($sliderDesign != 'Single Image'){
                    if($cpt != 3) {
                        $this->session->set_flashdata('error', 'Please upload slider images according to slider design,single Image design needs only one image other needs 3 images');
                        redirect('/admin/addSlider');
                        exit;
                    }
                }  else if($sliderDesign == 'Single Image'){
                    if($cpt != 1) {
                        $this->session->set_flashdata('error', 'Please upload slider images according to slider design,single Image design needs only one image other needs 3 images');
                        redirect('/admin/addSlider');
                        exit;
                    }
                } 
                
                for ($i = 0; $i < $cpt; $i++) {
                    $_FILES['uploaded_files']['name'] = $files['uploaded_files']['name'][$i];
                    $_FILES['uploaded_files']['type'] = $files['uploaded_files']['type'][$i];
                    $_FILES['uploaded_files']['tmp_name'] = $files['uploaded_files']['tmp_name'][$i];
                    $_FILES['uploaded_files']['error'] = $files['uploaded_files']['error'][$i];
                    $_FILES['uploaded_files']['size']= $files['uploaded_files']['size'][$i];

                    /*$image_info = getimagesize($_FILES["uploaded_files"]["tmp_name"][$i]);
                    $image_width = $image_info[0];
                    $image_height = $image_info[1];

                    if ($image_width < 600 || $image_height < 700) {
                        $this->session->set_flashdata('error', 'Please select valid min width or min height images');
                        $this->addSlider();
                    }
                    var_dump($_FILES);exit;*/
                    if (!$this->upload->do_upload('uploaded_files')) {
                        //if file upload failed then catch the errors
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        $is_file_error = TRUE;
                    } else {
                        $dataInfo[] = $this->upload->data();
                    }
                }
               // var_dump($dataInfo);exit;
                if ($is_file_error) {
                    if ($dataInfo) {
                        for ($i = 0; $i < count($dataInfo); $i++) {
                            $file = './sliderUploads/' . $dataInfo[$i]['file_name'];
                            if (file_exists($file)) {
                                unlink($file);
                            }
                        }
                        redirect('/admin/addSlider');
                    }
                }
                if (!$is_file_error) {
                    $this->load->model('admin/slider_model');
                    /// save the slider first
                    $sliderInfo = array('sliderDesign' => $sliderDesign, 'status' => $status, 'sliderOrder' => $sliderOrder, 'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));
                    $sliderId = $this->slider_model->addNewSlider($sliderInfo);
                    /// save Slier images
                    if($sliderId) {
                        for($i=0; $i<count($dataInfo); $i++) {
                            $sliderImageInfo = array('sliderId' => $sliderId, 'file_name' => $dataInfo[$i]['file_name'], 'file_orig_name' => $dataInfo[$i]['orig_name'],
                                'file_path' => $dataInfo[$i]['full_path'], 'imgOrder' => $i + 1, 'createdBy' => $this->vendorId, 'createdDtm' => date('Y-m-d H:i:s'));
                            $data_result[] = $this->slider_model->addNewSliderImage($sliderImageInfo);
                        }
                        foreach($data_result as $result) {
                            if($result === false) {
                                $result = $this->slider_model->deleteSlider($sliderId);
                                $is_file_error = true;
                                break;
                            }
                        }
                        if($is_file_error) {
                            for ($i = 0; $i < $dataInfo; $i++) {
                                $file = './sliderUploads/' . $dataInfo[$i]['file_name'];
                                if (file_exists($file)) {
                                    unlink($file);
                                }
                            }
                            $this->session->set_flashdata('error', 'Slider image creation failed');
                            redirect('/admin/addSlider');
                        } else {
                            $this->session->set_flashdata('success', 'Slider images uploaded successfully');
                            redirect('/admin/sliderListing');
                        }
                    } else {
                        $this->session->set_flashdata('error', 'Slider image creation failed');
                        redirect('/admin/addSlider');
                    }
                }
            }
            redirect('/admin/addSlider');
    }

    /**
     * This function is used load slider edit information
     * @param number $sliderId : Optional : This is user id
     */
    function editSlider($sliderId = NULL)
    {

        if($this->isAdmin() == TRUE )
        {
            $this->loadThis();
        }
        else
        {
            if($sliderId == null)
            {
                redirect('/admin/sliderListing');
            }


            $data['sliderInfo'] = $this->slider_model->getSliderInfo($sliderId);

            $this->global['pageTitle'] = 'Zak : Edit Slider';

            $this->loadViews("admin/editSlider", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to edit the slider information
     */
    function editprocessSlider()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');

            $sliderId = $this->input->post('sliderId');

            $this->form_validation->set_rules('sliderOrder','Slider Order','trim|required|numeric|xss_clean');

            if($this->form_validation->run() == FALSE)
            {
                $this->editSlider($sliderId);
            }
            else
            {
                $sliderDesign = $this->input->post('sliderDesign');
                $status = $this->input->post('status');
                $sliderOrder = $this->input->post('sliderOrder');

                $sliderInfo = array();


                $sliderInfo = array('sliderDesign'=>$sliderDesign, 'status'=> $status, 'sliderOrder'=>$sliderOrder,
                   'updatedBy'=>$this->vendorId,'updatedDtm'=>date('Y-m-d H:i:s'));


                $result = $this->slider_model->editSlider($sliderInfo, $sliderId);

                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Slider updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Slider updation failed');
                }

                redirect('/admin/sliderListing');
            }
        }
    }



    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteSlider()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $sliderId = $this->input->post('sliderId');
            $sliderInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));

            $result = $this->slider_model->deleteSlider($sliderId, $sliderInfo);

            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }

    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function disableSlider()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $sliderId = $this->input->post('sliderId');
            $sliderInfo = array('status'=>0,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));

            $result = $this->slider_model->disableSlider($sliderId, $sliderInfo);

            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }

    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function enableSlider()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $sliderId = $this->input->post('sliderId');
            $sliderInfo = array('status'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));

            $result = $this->slider_model->disableSlider($sliderId, $sliderInfo);

            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }

}

?>