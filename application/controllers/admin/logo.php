<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Instagram (InstagramController)
 * Instagram Class to control all instagram related operations.
 * @author : Kamran Khan
 * @version : 1.1
 * @since : 17 December 2017
 */
class Logo extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/logo_model');
        $this->isLoggedIn();
    }

    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Zak : Manage_logos';

        $this->loadViews("admin/manageLogos", $this->global, NULL , NULL);
    }

    /**
     * This function is used to load the user list
     */
    function logoListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('admin/logo_model');

            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;

            $this->load->library('pagination');

            $count = $this->logo_model->logoListingCount($searchText);

            $returns = $this->paginationCompress ( "admin/logoListing/", $count, 10 );

            $data['logoRecords'] = $this->logo_model->logoListing($searchText, $returns["page"], $returns["segment"]);

            $this->global['pageTitle'] = 'Zak : Logo Listing';

            $this->loadViews("admin/manageLogos", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addLogo()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('admin/logo_model');

            $this->global['pageTitle'] = 'Zak : Add New Logo';

            $this->loadViews("admin/addLogo", $this->global, '', NULL);
        }
    }



    /**
     * This function is used to add new user to the system
     */
    function processAddLogo()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('title','Logo Title','trim|max_length[128]|xss_clean');
            $this->form_validation->set_rules('description','Logo Description','trim|xss_clean');
            $this->form_validation->set_rules('url','Site URL','trim|xss_clean');



            if($this->form_validation->run() == FALSE)
            {
                $this->addLogo();
            }
            else
            {
                $title = $this->input->post('title');
                $description = $this->input->post('description');
                $url = $this->input->post('url');

                //file upload destination
                $config['upload_path'] = './uploads/logos';
                //allowed file types. * means all types
                $config['allowed_types'] = '*';
                //allowed max file size. 0 means unlimited file size
                $config['max_size'] = '0';
                //max file name size
                $config['max_filename'] = '255';
                //whether file name should be encrypted or not
                $config['encrypt_name'] = TRUE;

                //thumbnail path
                $thumb_path = './uploads/logos/thumbs/';
                //store file info once uploaded
                $file_data = array();
                //check for errors
                $is_file_error = FALSE;
                //check if file was selected for upload
                if (!$_FILES) {
                    $is_file_error = TRUE;
                    $this->session->set_flashdata('error', 'Please select Logo image');
                    $this->addLogo();

                }
                //if file was selected then proceed to upload
                if (!$is_file_error) {
                    //load the preferences
                    $this->load->library('upload', $config);
                    //check file successfully uploaded. 'file_name' is the name of the input
                    if (!$this->upload->do_upload('file_name')) {
                        //if file upload failed then catch the errors
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        $this->addLogo();
                        $is_file_error = TRUE;
                    } else {
                        //store the file info
                        $file_data = $this->upload->data();
                        if (!is_file($thumb_path . $file_data['file_name'])) {
                            $config = array(
                                'source_image' => $file_data['full_path'], //get original image
                                'new_image' => $thumb_path,
                                'maintain_ratio' => true,
                                'width' => 137,
                                'height' => 26
                            );
                            $this->load->library('image_lib', $config); //load library
                            $this->image_lib->resize(); //do whatever specified in config
                        }
                    }
                }
                // There were errors, we have to delete the uploaded files
                if ($is_file_error) {
                    if ($file_data) {
                        $file = './uplods/logo/' . $file_data['file_name'];
                        if (file_exists($file)) {
                            unlink($file);
                        }
                        $thumb = $thumb_path . $file_data['file_name'];
                        if ($thumb) {
                            unlink($thumb);
                        }
                    }
                }
                if (!$is_file_error) {
                    //save the file info in the database

                    $logoInfo = array('title'=>$title, 'url' => $url, 'file_name' => $file_data['file_name'],  'file_orig_name' => $file_data['orig_name'],
                        'file_path' => $file_data['full_path'], 'description' => $description, 'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));

                    $this->load->model('admin/logo_model');
                    $result = $this->logo_model->addNewLogo($logoInfo);

                    if($result)
                    {
                        $this->session->set_flashdata('success', 'New Logo bas been created successfully');
                        redirect('/admin/logoListing');
                    } else {
                        //if file info save in database was not successful then delete from the destination folder
                        $file = './uplods/logos/' . $file_data['file_name'];
                        if (file_exists($file)) {
                            unlink($file);
                        }
                        $thumb = $thumb_path . $file_data['file_name'];
                        if ($thumb) {
                            unlink($thumb);
                        }
                        $this->session->set_flashdata('error', 'Logo creation failed.');
                        $this->addLogo();
                    }
                }

                redirect('/admin/addLogo');
            }
        }
    }


    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editLogo($logoId = NULL)
    {

        if($this->isAdmin() == TRUE )
        {
            $this->loadThis();
        }
        else
        {
            if($logoId == null)
            {
                redirect('/admin/logoListing');
            }


            $data['logoInfo'] = $this->logo_model->getLogoInfo($logoId);

            $this->global['pageTitle'] = 'Zak : Edit Logo';

            $this->loadViews("admin/editLogo", $this->global, $data, NULL);
        }
    }


    /**
     * This function is used to edit the user information
     */
    function editprocessLogo()
    {

        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $logoId = $this->input->post('logoId');

            $this->form_validation->set_rules('title','Logo Title','trim|max_length[128]|xss_clean');
            $this->form_validation->set_rules('description','Logo Description','trim|xss_clean');
            $this->form_validation->set_rules('url','Site URL','trim');


            if ($this->form_validation->run() == FALSE) {

                $this->editLogo($logoId);
            } else {
                $title = $this->input->post('title');
                $description = $this->input->post('description');
                $url = $this->input->post('url');

                $logoInfo = array();

                $logoInfo = array('title' => $title, 'url' => $url, 'description' => $description, 'updatedBy' => $this->vendorId,
                    'updatedDtm' => date('Y-m-d H:i:s'));
            }

            if (!$_FILES) {
                //set preferences
                //file upload destination
                $config['upload_path'] = './uploads/logo';
                //allowed file types. * means all types
                $config['allowed_types'] = '*';
                //allowed max file size. 0 means unlimited file size
                $config['max_size'] = '0';
                //max file name size
                $config['max_filename'] = '255';
                //whether file name should be encrypted or not
                $config['encrypt_name'] = TRUE;

                //thumbnail path
                $thumb_path = './uploads/logos/thumbs/';
                //store file info once uploaded
                $file_data = array();
                //check for errors
                $is_file_error = FALSE;
                //check if file was selected for upload

                //if file was selected then proceed to upload
                if (!$is_file_error) {
                    //load the preferences
                    $this->load->library('upload', $config);
                    //check file successfully uploaded. 'file_name' is the name of the input
                    if (!$this->upload->do_upload('file_name')) {
                        //if file upload failed then catch the errors
                        $this->session->set_flashdata('error', $this->upload->display_errors());

                        $is_file_error = TRUE;
                    } else {
                        //store the file info
                        $file_data = $this->upload->data();
                        if (!is_file($thumb_path . $file_data['file_name'])) {
                            $config = array(
                                'source_image' => $file_data['full_path'], //get original image
                                'new_image' => $thumb_path,
                                'maintain_ratio' => true,
                                'width' => 137,
                                'height' => 26
                            );
                            $this->load->library('image_lib', $config); //load library
                            $this->image_lib->resize(); //do whatever specified in config
                        }
                    }
                }
                // There were errors, we have to delete the uploaded files
                if ($is_file_error) {
                    if ($file_data) {
                        $file = './uplods/logos/' . $file_data['file_name'];
                        if (file_exists($file)) {
                            unlink($file);
                        }
                        $thumb = $thumb_path . $file_data['file_name'];
                        if ($thumb) {
                            unlink($thumb);
                        }
                    }
                    redirect('/admin/editLogo/'.$logoId);

                }
                if (!$is_file_error) {
                    //save the file info in the database

                    $logoInfo = array('title'=>$title, 'file_name' => $file_data['file_name'],  'file_orig_name' => $file_data['orig_name'],
                        'file_path' => $file_data['full_path'], 'description' => $description, 'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));

                    $this->load->model('admin/logo_model');
                    $result = $this->logo_model->editLogo($logoInfo, $logoId);

                    if($result)
                    {
                        $this->session->set_flashdata('success', 'Logo updated successfully');
                    } else {
                        //if file info save in database was not successful then delete from the destination folder
                        $file = './uplods/logos/' . $file_data['file_name'];
                        if (file_exists($file)) {
                            unlink($file);
                        }
                        $thumb = $thumb_path . $file_data['file_name'];
                        if ($thumb) {
                            unlink($thumb);
                        }
                        $this->session->set_flashdata('error', 'Logo updation failed');
                        redirect('/admin/editLogo/'.$logoId);
                    }
                }
            } else {
                $logoInfo = array('title'=>$title, 'description' => $description, 'url' => $url, 'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));

                $this->load->model('admin/logo_model');
                $result = $this->logo_model->editLogoDetails($logoInfo, $logoId);
                if($result)
                {
                    $this->session->set_flashdata('success', 'Logo updated successfully');
                    redirect('/admin/logoListing');
                } else {
                    $this->session->set_flashdata('error', 'Logo updation failed');
                    redirect('/admin/editLogo/'.$logoId);
                }
            }
            redirect('/admin/logoListing');
        }
    }

    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteLogo()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $logoId = $this->input->post('logoId');
            $logoInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));

            $result = $this->logo_model->deleteLogo($logoId, $logoInfo);

            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }


}

?>