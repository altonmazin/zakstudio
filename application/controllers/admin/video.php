<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Instagram (InstagramController)
 * Instagram Class to control all instagram related operations.
 * @author : Kamran Khan
 * @version : 1.1
 * @since : 17 December 2017
 */
class Video extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/video_model');
        $this->isLoggedIn();
    }

    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Zak : Manage_Videos';

        $this->loadViews("admin/manageVideos", $this->global, NULL , NULL);
    }

    /**
     * This function is used to load the user list
     */
    function videoListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('admin/video_model');

            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;

            $this->load->library('pagination');

            $count = $this->video_model->videoListingCount($searchText);

            $returns = $this->paginationCompress ( "admin/videoListing/", $count, 10 );

            $data['videoRecords'] = $this->video_model->videoListing($searchText, $returns["page"], $returns["segment"]);

            $this->global['pageTitle'] = 'Zak : Video Listing';

            $this->loadViews("admin/manageVideos", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addVideo()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('admin/video_model');

            $this->global['pageTitle'] = 'Zak : Add New Video';

            $this->loadViews("admin/addVideo", $this->global, '', NULL);
        }
    }



    /**
     * This function is used to add new user to the system
     */
    function processAddVideo()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('title','Video Title','trim|max_length[128]|xss_clean');
            $this->form_validation->set_rules('url','Site URL','trim|xss_clean');



            if($this->form_validation->run() == FALSE)
            {
                $this->addVideo();
            }
            else
            {
                $title = $this->input->post('title');
                $url = $this->input->post('url');

                    $videoInfo = array('title'=>$title, 'url' => $url,'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));

                    $this->load->model('admin/video_model');
                    $result = $this->video_model->addNewVideo($videoInfo);

                    if($result)
                    {
                        $this->session->set_flashdata('success', 'New Video bas been created successfully');
                        redirect('/admin/videoListing');
                    } else {
                        
                        $this->session->set_flashdata('error', 'Video creation failed.');
                        $this->addVideo();
                    }

                redirect('/admin/addVideo');
            }
        }
    }


    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editVideo($videoId = NULL)
    {

        if($this->isAdmin() == TRUE )
        {
            $this->loadThis();
        }
        else
        {
            if($videoId == null)
            {
                redirect('/admin/videoListing');
            }


            $data['videoInfo'] = $this->video_model->getvideoInfo($videoId);

            $this->global['pageTitle'] = 'Zak : Edit Video';

            $this->loadViews("admin/editVideo", $this->global, $data, NULL);
        }
    }


    /**
     * This function is used to edit the user information
     */
    function editprocessVideo()
    {

        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $videoId = $this->input->post('videoId');

            $this->form_validation->set_rules('title','Video Title','trim|max_length[128]|xss_clean');
            $this->form_validation->set_rules('url','Site URL','trim');


            if ($this->form_validation->run() == FALSE) {

                $this->editVideo($videoId);
            } else {
                $title = $this->input->post('title');
                $url = $this->input->post('url');

                $videoInfo = array();             

                $videoInfo = array('title'=>$title, 'url'=>$url, 'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));

                $this->load->model('admin/video_model');
                $result = $this->video_model->editVideo($videoInfo, $videoId);

                if($result)
                {
                    $this->session->set_flashdata('success', 'Video updated successfully');
                } else {
                   
                    $this->session->set_flashdata('error', 'Video updation failed');
                    redirect('/admin/editVideo/'.$videoId);
                }
            }               
            
            redirect('/admin/videoListing');
        }
    }

    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteVideo()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $videoId = $this->input->post('videoId');
            $videoInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));

            $result = $this->video_model->deleteVideo($videoId, $videoInfo);

            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }


}

?>