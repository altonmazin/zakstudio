<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Instagram (InstagramController)
 * Instagram Class to control all instagram related operations.
 * @author : Kamran Khan
 * @version : 1.1
 * @since : 17 December 2017
 */
class Ready extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/ready_model');
        $this->isLoggedIn();
    }


    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editReady()
    {

        if($this->isAdmin() == TRUE )
        {
            $this->loadThis();
        }
        else
        {
           $data['readyInfo'] = $this->ready_model->getReadyInfo();

            $this->global['pageTitle'] = 'Zak : Edit Ready';

            $this->loadViews("admin/editReady", $this->global, $data, NULL);
        }
    }


    /**
     * This function is used to edit the user information
     */
    function editprocessReady()
    {

        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $readyId = $this->input->post('readyId');

            $this->form_validation->set_rules('upper_text','Upper Text','trim|xss_clean');
            $this->form_validation->set_rules('subtitle_text','SubTitle Text','trim|xss_clean');
            $this->form_validation->set_rules('button_text','Button Text','trim|xss_clean');
            $this->form_validation->set_rules('title_text','Description Text','trim|xss_clean');



            if ($this->form_validation->run() == FALSE) {

                redirect('/admin/editReady');
            } else {

                $upper_text = $this->input->post('upper_text');
                $title_text = $this->input->post('title_text');
                $button_text = $this->input->post('button_text');
                $button_url = $this->input->post('button_url');
                $hide_text = $this->input->post('hide_text');
                $subtitle_text = $this->input->post('subtitle_text');

                $readyInfo = array();

                $readyInfo = array('upper_text' => $upper_text,'button_url' => $button_url, 'hide_text' => $hide_text, 'title_text' => $title_text,'subtitle_text' => $subtitle_text, 'button_text' => $button_text, 'updatedBy' => $this->vendorId,
                    'updatedDtm' => date('Y-m-d H:i:s'));

                if($_FILES['file_name']['name'] != '') {
                    //set preferences

                    //file upload destination
                    $config['upload_path'] = './uploads/pages/';
                    //allowed file types. * means all types
                    $config['allowed_types'] = '*';
                    //allowed max file size. 0 means unlimited file size
                    $config['max_size'] = '0';
                    //max file name size
                    $config['max_filename'] = '255';
                    //whether file name should be encrypted or not
                    $config['encrypt_name'] = TRUE;

                    //thumbnail path
                    $thumb_path = './uploads/pages/thumbs/';
                    //store file info once uploaded
                    $file_data = array();
                    //check for errors
                    $is_file_error = FALSE;
                    //check if file was selected for upload

                    //if file was selected then proceed to upload
                    if (!$is_file_error) {
                        //load the preferences
                        $this->load->library('upload', $config);
                        //check file successfully uploaded. 'file_name' is the name of the input
                        if (!$this->upload->do_upload('file_name')) {
                            //if file upload failed then catch the errors
                            $this->session->set_flashdata('error', $this->upload->display_errors());

                            $is_file_error = TRUE;
                        } else {
                            //store the file info
                            $file_data = $this->upload->data();
                            if (!is_file($thumb_path . $file_data['file_name'])) {
                                $config = array(
                                    'source_image' => $file_data['full_path'], //get original image
                                    'new_image' => $thumb_path,
                                    'maintain_ratio' => true,
                                    'height' => 470
                                );
                                $this->load->library('image_lib', $config); //load library
                                $this->image_lib->resize(); //do whatever specified in config
                            }
                        }
                    }
                    // There were errors, we have to delete the uploaded files
                    if ($is_file_error) {
                        if ($file_data) {
                            $file = './uploads/pages/' . $file_data['file_name'];
                            if (file_exists($file)) {
                                unlink($file);
                            }
                            $thumb = $thumb_path . $file_data['file_name'];
                            if ($thumb) {
                                unlink($thumb);
                            }
                        }
                        redirect('/admin/editReady');

                    }

                    if (!$is_file_error) {
                        //save the file info in the database

                        $readyInfo = array('upper_text' => $upper_text,'button_url' => $button_url, 'hide_text' => $hide_text, 'title_text' => $title_text, 'subtitle_text' => $subtitle_text, 'button_text' => $button_text,
                            'file_name' => $file_data['file_name'], 'file_orig_name' => $file_data['orig_name'],
                            'file_path' => $file_data['full_path'], 'createdBy' => $this->vendorId, 'createdDtm' => date('Y-m-d H:i:s'));


                        $this->load->model('admin/ready_model');
                        $result = $this->ready_model->editReady($readyInfo, $readyId);

                        if ($result) {
                            $this->session->set_flashdata('success', 'Section Detail Added Successfully');
                        } else {
                            //if file info save in database was not successful then delete from the destination folder
                            $file = './uploads/pages/' . $file_data['file_name'];
                            if (file_exists($file)) {
                                unlink($file);
                            }
                            $thumb = $thumb_path . $file_data['file_name'];
                            if ($thumb) {
                                unlink($thumb);
                            }
                            $this->session->set_flashdata('error', 'Section Detail update failed');
                            redirect('/admin/editReady');
                        }
                    }
                } else {
                    //save the file info in the database
                    $readyInfo = array('upper_text' => $upper_text,'button_url' => $button_url, 'hide_text' => $hide_text, 'title_text' => $title_text, 'subtitle_text' => $subtitle_text, 'button_text' => $button_text,
                        'createdBy' => $this->vendorId, 'createdDtm' => date('Y-m-d H:i:s'));


                    $this->load->model('admin/ready_model');
                    $result = $this->ready_model->editReady($readyInfo, $readyId);

                    if ($result) {
                        $this->session->set_flashdata('success', 'Section Detail Added Successfully');
                    } else {
                        $this->session->set_flashdata('error', 'Section Detail update failed');
                        redirect('/admin/editReady');
                    }


                }

            }


            redirect('/admin/pageListing');
        }
    }




}

?>