<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Page_model extends CI_Model
{

    public function getaboutsec() {
        $this->db->select("*");
        return $this->db->get("tbl_about_sec")->row_array();
    }

    public function getreadysec() {
        $this->db->select("*");
        return $this->db->get("tbl_ready_sec")->row_array();
    }

    public function getbusniesssec() {
        $this->db->select("*");
        $this->db->where('isDeleted', 0);
        return $this->db->get("tbl_logos")->result_array();
    }

    public function getAlbums() {

        $this->db->select('*');
        $this->db->from('tbl_albums');

        $this->db->where('isDeleted', 0);
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }

    /**
     * This function used to check the login credentials of the user
     * @param string $email : This is email of the user
     * @param string $password : This is encrypted password of the user
     * @return object $result : Information of user
     */
    /*function loginMe($email, $password)
    {
        $this->db->select('BaseTbl.userId, BaseTbl.password, BaseTbl.name, BaseTbl.roleId, Roles.role');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Roles','Roles.roleId = BaseTbl.roleId');
        $this->db->where('BaseTbl.email', $email);
        $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();

        $user = $query->result();

        if(!empty($user)){
            if(verifyHashedPassword($password, $user[0]->password)){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }*/


}

?>