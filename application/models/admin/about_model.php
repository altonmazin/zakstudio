<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class About_model extends CI_Model
{

    /**
     * This function used to get about information by id
     * @param number $aboutId : This is about id
     * @return array $result : This is about information
     */
    function getAboutInfo()
    {
        $this->db->select('aboutId, top_text,title_text,button_text, button_url, hide_text, description_text, file_name, createdDtm');
        $this->db->from('tbl_about_sec');
        $query = $this->db->get();
        return $query->result();
    }


    /**
     * This function is used to update the about information
     * @param array $aboutInfo : This is abouts updated information
     * @param number $aboutId : This is about id
     * @return {boolean} $result : TRUE/FALSE
     */
    function editAbout($aboutInfo, $aboutId)
    {
        $this->db->trans_start();
        $this->db->where('aboutId', $aboutId);
        $this->db->update('tbl_about_sec', $aboutInfo);
        $this->db->trans_complete();
        //check transaction status
        if ($this->db->trans_status() === FALSE) {
            $file_path = $aboutInfo->full_path;
            //delete the file from destination
            if (file_exists($file_path)) {
                unlink($file_path);
            }
            //delete thumbnail image
            $thumb_path = './uploads/pages/thumbs/';
            $thumb = $thumb_path . $aboutInfo->file_name;
            if ($thumb) {
                unlink($thumb);
            }
            //rollback transaction
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //commit the transaction
            $this->db->trans_commit();
            return TRUE;
        }


    }

}

