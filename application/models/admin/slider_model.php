<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Slider_model extends CI_Model
{
    /**
     * This function is used to get the slider listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function sliderListingCount()
    {
        $this->db->select('BaseTbl.sliderId, BaseTbl.sliderDesign, BaseTbl.createdDtm');
        $this->db->from('tbl_header_slider as BaseTbl');
        $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();
        return count($query->result());
    }

    /**
     * This function is used to get the slider listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function sliderListing($page, $segment)
    {
        $this->db->select('BaseTbl.sliderId, BaseTbl.sliderDesign, BaseTbl.status, BaseTbl.createdDtm');
        $this->db->from('tbl_header_slider as BaseTbl');

        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->limit($page, $segment);
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }



    /**
     * This function is used to check whether email sliderId is already exist or not
     * @param {number} $sliderId : This is slider id
     * @return {mixed} $result : This is searched result
     */
    function checkSliderExists($sliderId = 0)
    {
        $this->db->select("sliderDesign");
        $this->db->from("tbl_header_slider");
        $this->db->where("isDeleted", 0);
        $this->db->where("sliderId !=", $sliderId);

        $query = $this->db->get();

        return $query->result();
    }


    /**
     * This function is used to add new slider to system
     * @return number $insert_id : This is last inserted sliderId
     */
    function addNewSlider($sliderInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_header_slider', $sliderInfo);

        $insert_id = $this->db->insert_id();

        $this->db->trans_complete();
        //check transaction status
        if ($this->db->trans_status() === FALSE) {            
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //commit the transaction
            $this->db->trans_commit();
            return $insert_id;
        }


    }

    /**
     * This function is used to add new slider image to system
     * @return number $insert_id : This is last inserted sliderId
     */
    function addNewSliderImage($sliderImageInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_header_slider_images', $sliderImageInfo);

        $insert_id = $this->db->insert_id();

        $this->db->trans_complete();
        //check transaction status
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //commit the transaction
            $this->db->trans_commit();
            return TRUE;
        }
    }

    /**
     * This function used to get slider information by sliderId
     * @param number $sliderId : This is slider id
     * @return array $result : This is slider information
     */
    function getSliderInfo($sliderId)
    {
        $this->db->select('sliderId, sliderDesign, sliderOrder, status, createdDtm');
        $this->db->from('tbl_header_slider');
        $this->db->where('isDeleted', 0);
        $this->db->where('sliderId', $sliderId);
        $query = $this->db->get();

        return $query->result();
    }

    /**
     * This function used to get slider information by sliderId
     * @param number $sliderId : This is slider id
     * @return array $result : This is slider information
     */
    function getSliderDesign($sliderId)
    {
        $this->db->select('sliderDesign');
        $this->db->from('tbl_header_slider');
        $this->db->where('sliderId', $sliderId);
        $query = $this->db->get();
        $row = $query->row();
        return $row->sliderDesign;
    }

    /**
     * This function used to get slider information by sliderId
     * @param number $sliderId : This is slider id
     * @return array $result : This is slider information
     */
    function getAllSliderImages($sliderId)
    {
        $this->db->select('tbl_header_slider.sliderId, sliderDesign, sliderOrder, status, file_name');
        $this->db->from('tbl_header_slider');
        $this->db->join('tbl_header_slider_images', 'tbl_header_slider.sliderId = tbl_header_slider_images.sliderId');
        $this->db->where('tbl_header_slider.sliderId', $sliderId);
        $this->db->order_by('tbl_header_slider_images.imgOrder', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * This function used to get slider information by sliderId
     * @param number $sliderId : This is slider id
     * @return array $result : This is slider information
     */
    function getAllSliders()
    {
        $this->db->select('sliderId, sliderDesign, sliderOrder, status, createdDtm');
        $this->db->from('tbl_header_slider');
        $this->db->where('isDeleted', 0);
        $this->db->order_by('tbl_header_slider.sliderId', 'DESC');
        $query = $this->db->get();

        return $query->result();
    }




    /**
     * This function is used to update the slider information
     * @param array $sliderInfo : This is slider updated information
     * @param number $sliderId : This is slider id
     * @return boolean $result : TRUE / FALSE
     */
    function editSlider($sliderInfo, $sliderId)
    {
        $this->db->trans_start();
        $this->db->where('sliderId', $sliderId);
        $this->db->update('tbl_header_slider', $sliderInfo);
        $this->db->trans_complete();
        //check transaction status
        if ($this->db->trans_status() === FALSE) {           
            //rollback transaction
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //commit the transaction
            $this->db->trans_commit();
            return TRUE;
        }
    }

    /**
     * This function is used to update the slider information
     * @param array $sliderInfo : This is slider updated information
     * @param number $sliderId : This is slider id
     * @return boolean $result : TRUE / FALSE
     */
    function editSliderDetails($sliderInfo, $sliderId)
    {
        if($sliderId != '') {
            $this->db->where('sliderId', $sliderId);
            $this->db->update('tbl_header_slider', $sliderInfo);
            return true;
        } else {
            return false;
        }
    }

    /**
     * This function is used to delete the slider information
     * @param number $sliderId : This is slider id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteSlider($sliderId, $sliderInfo)
    {
        $this->db->where('sliderId', $sliderId);
        $this->db->update('tbl_header_slider', $sliderInfo);

        return $this->db->affected_rows();
    }

    /**
     * This function is used to delete the slider information
     * @param number $sliderId : This is slider id
     * @return boolean $result : TRUE / FALSE
     */
    function disableSlider($sliderId, $sliderInfo)
    {
        $this->db->where('sliderId', $sliderId);
        $this->db->update('tbl_header_slider', $sliderInfo);

        return $this->db->affected_rows();
    }

    /**
     * This function is used to delete the slider information
     * @param number $sliderId : This is slider id
     * @return boolean $result : TRUE / FALSE
     */
    function enableSlider($sliderId, $sliderInfo)
    {
        $this->db->where('sliderId', $sliderId);
        $this->db->update('tbl_header_slider', $sliderInfo);

        return $this->db->affected_rows();
    }


    /**
     * This function is used to delete the slider information
     * @param number $sliderId : This is slider id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteAllSliderImages($sliderId)
    {
        $this->db->where('sliderId', $sliderId);
        $this->db->delete('tbl_header_slider_images');
    }

}

