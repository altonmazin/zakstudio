<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Instagram (InstagramController)
 * Instagram Class to control all instagram related operations.
 * @author : Kamran Khan
 * @version : 1.1
 * @since : 17 December 2017
 */
class Album extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/album_model');
        $this->isLoggedIn();
    }

    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Zak : Manage Albums';

        $this->loadViews("admin/manageAlbums", $this->global, NULL , NULL);
    }

    /**
     * This function is used to load the user list
     */
    function albumListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('admin/album_model');

            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;

            $this->load->library('pagination');

            $count = $this->album_model->albumListingCount($searchText);

            $returns = $this->paginationCompress ( "admin/albumListing/", $count, 10 );

            $data['albumRecords'] = $this->album_model->albumListing($searchText, $returns["page"], $returns["segment"]);

            $this->global['pageTitle'] = 'Zak : Album Listing';

            $this->loadViews("admin/manageAlbums", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addAlbum()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('admin/album_model');

            $this->global['pageTitle'] = 'Zak : Add New Album';

            $this->loadViews("admin/addAlbum", $this->global, '', NULL);
        }
    }



    /**
     * This function is used to add new user to the system
     */
    function processAddAlbum()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('title','Album Title','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('description','Album Description','trim|xss_clean');

            if($this->form_validation->run() == FALSE)
            {
                $this->addAlbum();
            }
            else
            {
                $title = $this->input->post('title');
                $description = $this->input->post('description');

                   //file upload destination
                    $config['upload_path'] = './albumUploads/albumImages';
                    //allowed file types. * means all types
                    $config['allowed_types'] = '*';
                    //allowed max file size. 0 means unlimited file size
                    $config['max_size'] = '0';
                    //max file name size
                    $config['max_filename'] = '255';
                    //whether file name should be encrypted or not
                    $config['encrypt_name'] = TRUE;

                    //thumbnail path
                    $thumb_path = './albumUploads/albumThumbs/';
                    //store file info once uploaded
                    $file_data = array();
                    //check for errors
                    $is_file_error = FALSE;
                    //check if file was selected for upload
                    if (!$_FILES) {
                        $is_file_error = TRUE;
                        $this->session->set_flashdata('error', 'Please select album image');
                        $this->addAlbum();

                    }
                    //if file was selected then proceed to upload
                    if (!$is_file_error) {
                        //load the preferences
                        $this->load->library('upload', $config);
                        //check file successfully uploaded. 'file_name' is the name of the input
                        if (!$this->upload->do_upload('file_name')) {
                            //if file upload failed then catch the errors
                            $this->session->set_flashdata('error', $this->upload->display_errors());
                            $this->addAlbum();
                            $is_file_error = TRUE;
                        } else {
                            //store the file info
                            $file_data = $this->upload->data();
                            if (!is_file($thumb_path . $file_data['file_name'])) {
                                $config = array(
                                    'source_image' => $file_data['full_path'], //get original image
                                    'new_image' => $thumb_path,
                                    'maintain_ratio' => true,
                                    'width' => 300,
                                    'height' => 200
                                );
                                $this->load->library('image_lib', $config); //load library
                                $this->image_lib->resize(); //do whatever specified in config
                            }
                        }
                    }
                    // There were errors, we have to delete the uploaded files
                    if ($is_file_error) {
                        if ($file_data) {
                            $file = './albumUploads/albumImages/' . $file_data['file_name'];
                            if (file_exists($file)) {
                                unlink($file);
                            }
                            $thumb = $thumb_path . $file_data['file_name'];
                            if ($thumb) {
                                unlink($thumb);
                            }
                        }
                    }
                    if (!$is_file_error) {
                        //save the file info in the database

                        $albumInfo = array('title'=>$title, 'file_name' => $file_data['file_name'],  'file_orig_name' => $file_data['orig_name'],
                            'file_path' => $file_data['full_path'], 'description' => $description, 'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));

                        $this->load->model('admin/album_model');
                        $result = $this->album_model->addNewAlbum($albumInfo);

                        if($result)
                        {
                            $this->session->set_flashdata('success', 'New Album bas been created successfully');
                        } else {
                            //if file info save in database was not successful then delete from the destination folder
                            $file = './albumUploads/albumImages/' . $file_data['file_name'];
                            if (file_exists($file)) {
                                unlink($file);
                            }
                            $thumb = $thumb_path . $file_data['file_name'];
                            if ($thumb) {
                                unlink($thumb);
                            }
                            $this->session->set_flashdata('error', 'Album creation failed.');
                            $this->addAlbum();
                        }
                    }

                redirect('/admin/addAlbum');
            }
        }
    }


    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editAlbum($albumId = NULL)
    {

        if($this->isAdmin() == TRUE )
        {
            $this->loadThis();
        }
        else
        {
            if($albumId == null)
            {
                redirect('/admin/albumListing');
            }


            $data['albumInfo'] = $this->album_model->getAlbumInfo($albumId);

            $this->global['pageTitle'] = 'Zak : Edit Album';

            $this->loadViews("admin/editAlbum", $this->global, $data, NULL);
        }
    }


    /**
     * This function is used to edit the user information
     */
    function editprocessAlbum()
    {

        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $albumId = $this->input->post('albumId');

            $this->form_validation->set_rules('title', 'Album Title', 'trim|required|max_length[128]|xss_clean');

            if ($this->form_validation->run() == FALSE) {

                $this->editAlbum($albumId);
            } else {
                $title = $this->input->post('title');
                $description = $this->input->post('description');

                $albumInfo = array();

                $albumInfo = array('title' => $title, 'description' => $description, 'updatedBy' => $this->vendorId,
                    'updatedDtm' => date('Y-m-d H:i:s'));
            }

            if ($_FILES['file_name']['name'] != '') {
                echo 'it should be herer';
                //set preferences

                //file upload destination
                $config['upload_path'] = './albumUploads/albumImages';
                //allowed file types. * means all types
                $config['allowed_types'] = '*';
                //allowed max file size. 0 means unlimited file size
                $config['max_size'] = '0';
                //max file name size
                $config['max_filename'] = '255';
                //whether file name should be encrypted or not
                $config['encrypt_name'] = TRUE;

                //thumbnail path
                $thumb_path = './albumUploads/albumThumbs/';
                //store file info once uploaded
                $file_data = array();
                //check for errors
                $is_file_error = FALSE;
                //check if file was selected for upload

                //if file was selected then proceed to upload
                if (!$is_file_error) {
                    //load the preferences
                    $this->load->library('upload', $config);
                    //check file successfully uploaded. 'file_name' is the name of the input
                    if (!$this->upload->do_upload('file_name')) {
                        //if file upload failed then catch the errors
                        $this->session->set_flashdata('error', $this->upload->display_errors());

                        $is_file_error = TRUE;
                    } else {
                        //store the file info
                        $file_data = $this->upload->data();
                        if (!is_file($thumb_path . $file_data['file_name'])) {
                            $config = array(
                                'source_image' => $file_data['full_path'], //get original image
                                'new_image' => $thumb_path,
                                'maintain_ratio' => true,
                                'width' => 300,
                                'height' => 200
                            );
                            $this->load->library('image_lib', $config); //load library
                            $this->image_lib->resize(); //do whatever specified in config
                        }
                    }
                }
                // There were errors, we have to delete the uploaded files
                if ($is_file_error) {
                    if ($file_data) {
                        $file = './albumUploads/albumImages/' . $file_data['file_name'];
                        if (file_exists($file)) {
                            unlink($file);
                        }
                        $thumb = $thumb_path . $file_data['file_name'];
                        if ($thumb) {
                            unlink($thumb);
                        }
                    }
                    redirect('/admin/editAlbum/'.$albumId);

                }
                if (!$is_file_error) {
                    //save the file info in the database

                    $albumInfo = array('title'=>$title, 'file_name' => $file_data['file_name'],  'file_orig_name' => $file_data['orig_name'],
                        'file_path' => $file_data['full_path'], 'description' => $description, 'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));

                    $this->load->model('admin/album_model');
                    $result = $this->album_model->editAlbum($albumInfo, $albumId);

                    if($result)
                    {
                        $this->session->set_flashdata('success', 'Album updated successfully');
                    } else {
                        //if file info save in database was not successful then delete from the destination folder
                        $file = './albumUploads/albumImages/' . $file_data['file_name'];
                        if (file_exists($file)) {
                            unlink($file);
                        }
                        $thumb = $thumb_path . $file_data['file_name'];
                        if ($thumb) {
                            unlink($thumb);
                        }
                        $this->session->set_flashdata('error', 'Album updation failed');
                        redirect('/admin/editAlbum/'.$albumId);
                    }
                }
            } else {
                $albumInfo = array('title'=>$title, 'description' => $description, 'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));

                $this->load->model('admin/album_model');
                $result = $this->album_model->editAlbumDetails($albumInfo, $albumId);
                if($result)
                {
                    $this->session->set_flashdata('success', 'Album updated successfully');
                } else {
                    $this->session->set_flashdata('error', 'Album updation failed');
                    redirect('/admin/editAlbum/'.$albumId);
                }
            }
            redirect('/admin/editAlbum/'.$albumId);
        }
    }



    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteAlbum()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $albumId = $this->input->post('albumId');
            $albumInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));

            $result = $this->album_model->deleteAlbum($albumId, $albumInfo);

            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }


}

?>