<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Ready_model extends CI_Model
{

    /**
     * This function used to get ready information by id
     * @param number $readyId : This is ready id
     * @return array $result : This is ready information
     */
    function getReadyInfo()
    {
        $this->db->select('readyId, upper_text, subtitle_text,title_text,button_text,button_url, hide_text, file_name, createdDtm');
        $this->db->from('tbl_ready_sec');
        $query = $this->db->get();

        return $query->result();
    }


    /**
     * This function is used to update the ready information
     * @param array $readyInfo : This is readys updated information
     * @param number $readyId : This is ready id
     * @return {boolean} $result : TRUE/FALSE
     */
    function editReady($readyInfo, $readyId)
    {
        $this->db->trans_start();
        $this->db->where('readyId', $readyId);
        $this->db->update('tbl_ready_sec', $readyInfo);
        $this->db->trans_complete();
        //check transaction status
        if ($this->db->trans_status() === FALSE) {
            $file_path = $readyInfo->full_path;
            //delete the file from destination
            if (file_exists($file_path)) {
                unlink($file_path);
            }
            //delete thumbnail image
            $thumb_path = './uploads/pages/thumbs/';
            $thumb = $thumb_path . $readyInfo->file_name;
            if ($thumb) {
                unlink($thumb);
            }
            //rollback transaction
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //commit the transaction
            $this->db->trans_commit();
            return TRUE;
        }


    }

}

