<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Logo_model extends CI_Model
{
    /**
     * This function is used to get the logo listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function logoListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.logoId, BaseTbl.title, BaseTbl.url, BaseTbl.description, BaseTbl.file_name');
        $this->db->from('tbl_logos as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.title  LIKE '%".$searchText."%'
                           OR  BaseTbl.description  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();

        return count($query->result());
    }

    /**
     * This function is used to get the logo listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function logoListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.logoId, BaseTbl.title, BaseTbl.url, BaseTbl.description, BaseTbl.createdDtm, file_name');
        $this->db->from('tbl_logos as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.title LIKE '%".$searchText."%'
                            OR  BaseTbl.description  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->limit($page, $segment);
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }



    /**
     * This function is used to check whether email id is already exist or not
     * @param {string} $email : This is email id
     * @param {number} $logoId : This is logo id
     * @return {mixed} $result : This is searched result
     */
    function checkLogoExists($logoId = 0)
    {
        $this->db->select("title");
        $this->db->from("tbl_logos");
        $this->db->where("isDeleted", 0);
        $this->db->where("logoId !=", $logoId);

        $query = $this->db->get();

        return $query->result();
    }


    /**
     * This function is used to add new logo to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewLogo($logoInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_logos', $logoInfo);

        $insert_id = $this->db->insert_id();

        $this->db->trans_complete();
        //check transaction status
        if ($this->db->trans_status() === FALSE) {
            $file_path = $logoInfo->full_path;
            //delete the file from destination
            if (file_exists($file_path)) {
                unlink($file_path);
            }
            //delete thumbnail image
            $thumb_path = './logoUploads/logoThumbs/';
            $thumb = $thumb_path . $logoInfo->file_name;
            if ($thumb) {
                unlink($thumb);
            }
            //rollback transaction
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //commit the transaction
            $this->db->trans_commit();
            return TRUE;
        }


    }

    /**
     * This function used to get logo information by id
     * @param number $logoId : This is logo id
     * @return array $result : This is logo information
     */
    function getLogoInfo($logoId)
    {
        $this->db->select('logoId, title, description, url, file_name, createdDtm');
        $this->db->from('tbl_logos');
        $this->db->where('isDeleted', 0);
        $this->db->where('logoId', $logoId);
        $query = $this->db->get();

        return $query->result();
    }


    /**
     * This function is used to update the logo information
     * @param array $logoInfo : This is logos updated information
     * @param number $logoId : This is logo id
     */
    function editLogo($logoInfo, $logoId)
    {
        $this->db->trans_start();
        $this->db->where('logoId', $logoId);
        $this->db->update('tbl_logos', $logoInfo);
        $this->db->trans_complete();
        //check transaction status
        if ($this->db->trans_status() === FALSE) {
            $file_path = $logoInfo->full_path;
            //delete the file from destination
            if (file_exists($file_path)) {
                unlink($file_path);
            }
            //delete thumbnail image
            $thumb_path = './logoUploads/logoThumbs/';
            $thumb = $thumb_path . $logoInfo->file_name;
            if ($thumb) {
                unlink($thumb);
            }
            //rollback transaction
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //commit the transaction
            $this->db->trans_commit();
            return TRUE;
        }


    }

    /**
     * This function is used to update the logo information
     * @param array $logoInfo : This is logos updated information
     * @param number $logoId : This is logo id
     */
    function editLogoDetails($logoInfo, $logoId)
    {
        if($logoId != '') {
            $this->db->where('logoId', $logoId);
            $this->db->update('tbl_logos', $logoInfo);
            return true;
        } else {
            return false;
        }
    }

    /**
     * This function is used to delete the logo information
     * @param number $logoId : This is logo id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteLogo($logoId, $logoInfo)
    {
        $this->db->where('logoId', $logoId);
        $this->db->update('tbl_logos', $logoInfo);

        return $this->db->affected_rows();
    }

}

