<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Album_model extends CI_Model
{
    /**
     * This function is used to get the album listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function albumListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.albumId, BaseTbl.title, BaseTbl.description, BaseTbl.file_name');
        $this->db->from('tbl_albums as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.title  LIKE '%".$searchText."%'
                           OR  BaseTbl.description  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();

        return count($query->result());
    }

    /**
     * This function is used to get the album listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function albumListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.albumId, BaseTbl.title, BaseTbl.description, BaseTbl.createdDtm, file_name');
        $this->db->from('tbl_albums as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.title LIKE '%".$searchText."%'
                            OR  BaseTbl.description  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->limit($page, $segment);
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }



    /**
     * This function is used to check whether email id is already exist or not
     * @param {string} $email : This is email id
     * @param {number} $albumId : This is album id
     * @return {mixed} $result : This is searched result
     */
    function checkAlbumExists($albumId = 0)
    {
        $this->db->select("title");
        $this->db->from("tbl_albums");
        $this->db->where("isDeleted", 0);
        $this->db->where("albumId !=", $albumId);

        $query = $this->db->get();

        return $query->result();
    }


    /**
     * This function is used to add new album to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewAlbum($albumInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_albums', $albumInfo);

        $insert_id = $this->db->insert_id();

        $this->db->trans_complete();
        //check transaction status
        if ($this->db->trans_status() === FALSE) {
            $file_path = $albumInfo->full_path;
            //delete the file from destination
            if (file_exists($file_path)) {
                unlink($file_path);
            }
            //delete thumbnail image
            $thumb_path = './albumUploads/albumThumbs/';
            $thumb = $thumb_path . $albumInfo->file_name;
            if ($thumb) {
                unlink($thumb);
            }
            //rollback transaction
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //commit the transaction
            $this->db->trans_commit();
            return TRUE;
        }


    }

    /**
     * This function used to get album information by id
     * @param number $albumId : This is album id
     * @return array $result : This is album information
     */
    function getAlbumInfo($albumId)
    {
        $this->db->select('albumId, title, description, file_name, createdDtm');
        $this->db->from('tbl_albums');
        $this->db->where('isDeleted', 0);
        $this->db->where('albumId', $albumId);
        $query = $this->db->get();

        return $query->result();
    }

    public function getAlbums() {

        $this->db->select('*');
        $this->db->from('tbl_albums');

        $this->db->where('isDeleted', 0);
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }

    /**
     * This function used to get album information by id
     * @param number $albumId : This is album id
     * @return array $result : This is album information
     */
    function getAlbumImages($albumId)
    {
        $this->db->select('imageId, albumId, file_name, createdDtm');
        $this->db->from('tbl_album_images');
        $this->db->where('isDeleted', 0);
        $this->db->where('albumId', $albumId);
        $this->db->order_by('imageId', 'DESC');
        $this->db->limit(6,0);
        $query = $this->db->get();
      //  $sql = $this->db->last_query();
      //  echo $sql;exit;
        return $query->result();
    }

    /**
     * This function used to get album information by id
     * @param number $albumId : This is album id
     * @return array $result : This is album information
     */
    function getAllAlbumImages()
    {
        $this->db->select('imageId, albumId, file_name, createdDtm');
        $this->db->from('tbl_album_images');
        $this->db->where('isDeleted', 0);
        $this->db->order_by('imageId', 'DESC');
        $this->db->limit(6,0);
        $query = $this->db->get();
        //  $sql = $this->db->last_query();
        //  echo $sql;exit;
        return $query->result();
    }

    /**
     * This function used to get album information by id
     * @param number $albumId : This is album id
     * @return array $result : This is album information
     */
    function getAlbumImagesByLimit($albumId, $row, $rowperpage)
    {
        $this->db->select('imageId, albumId, file_name, createdDtm');
        $this->db->from('tbl_album_images');
        $this->db->where('isDeleted', 0);
        $this->db->where('albumId', $albumId);
        $this->db->order_by('imageId', 'DESC');
        $this->db->limit($rowperpage,$row);
        $query = $this->db->get();
        //  $sql = $this->db->last_query();
        //  echo $sql;
        return $query->result();
    }



    /**
     * This function used to get album information by id
     * @param number $albumId : This is album id
     * @return array $result : This is album information
     */
    function getAllAlbumImagesByLimit($row, $rowperpage)
    {
        $this->db->select('imageId, albumId, file_name, createdDtm');
        $this->db->from('tbl_album_images');
        $this->db->where('isDeleted', 0);
        $this->db->order_by('imageId', 'DESC');
        $this->db->limit($rowperpage,$row);
        $query = $this->db->get();
        //  $sql = $this->db->last_query();
        //  echo $sql;
        return $query->result();
    }



    /**
     * This function used to get album information by id
     * @param number $albumId : This is album id
     * @return array $result : This is album information
     */
    function countAlbumImages($albumId)
    {
        $this->db->select('imageId, albumId, file_name, createdDtm');
        $this->db->from('tbl_album_images');
        $this->db->where('isDeleted', 0);
        $this->db->where('albumId', $albumId);
        $query = $this->db->get();
        $records = $query->num_rows();
        return $records;
    }

    /**
     * This function used to get album information by id
     * @param number $albumId : This is album id
     * @return array $result : This is album information
     */
    function countAllAlbumImages()
    {
        $this->db->select('imageId, albumId, file_name, createdDtm');
        $this->db->from('tbl_album_images');
        $this->db->where('isDeleted', 0);
        $query = $this->db->get();
        $records = $query->num_rows();
        return $records;
    }


    /**
     * This function is used to update the album information
     * @param array $albumInfo : This is albums updated information
     * @param number $albumId : This is album id
     * @return {boolean} $result : TRUE/FALSE
     */
    function editAlbum($albumInfo, $albumId)
    {
        $this->db->trans_start();
        $this->db->where('albumId', $albumId);
        $this->db->update('tbl_albums', $albumInfo);
        $this->db->trans_complete();
        //check transaction status
        if ($this->db->trans_status() === FALSE) {
            $file_path = $albumInfo->full_path;
            //delete the file from destination
            if (file_exists($file_path)) {
                unlink($file_path);
            }
            //delete thumbnail image
            $thumb_path = './albumUploads/albumThumbs/';
            $thumb = $thumb_path . $albumInfo->file_name;
            if ($thumb) {
                unlink($thumb);
            }
            //rollback transaction
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //commit the transaction
            $this->db->trans_commit();
            return TRUE;
        }


    }

    /**
     * This function is used to update the album information
     * @param array $albumInfo : This is albums updated information
     * @param number $albumId : This is album id
     * @return {boolean} $result : TRUE/FALSE
     */
    function editAlbumDetails($albumInfo, $albumId)
    {
        if($albumId != '') {
            $this->db->where('albumId', $albumId);
            $this->db->update('tbl_albums', $albumInfo);
            return true;
        } else {
            return false;
        }
    }

    /**
     * This function is used to delete the album information
     * @param number $albumId : This is album id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteAlbum($albumId, $albumInfo)
    {
        $this->db->where('albumId', $albumId);
        $this->db->update('tbl_albums', $albumInfo);

        return $this->db->affected_rows();
    }

}

