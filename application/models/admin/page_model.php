<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Page_model extends CI_Model
{

    /**
     * This function is used to get the page listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function pageListing()
    {
        $this->db->select('BaseTbl.pageId, BaseTbl.title, BaseTbl.description, BaseTbl.createdDtm');
        $this->db->from('tbl_pages as BaseTbl');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }



    /**
     * This function is used to check whether email id is already exist or not
     * @param {string} $email : This is email id
     * @param {number} $pageId : This is page id
     * @return {mixed} $result : This is searched result
     */
    function checkPageExists($pageId = 0)
    {
        $this->db->select("title");
        $this->db->from("tbl_pages");
        $this->db->where("isDeleted", 0);
        $this->db->where("pageId !=", $pageId);

        $query = $this->db->get();

        return $query->result();
    }


    /**
     * This function used to get page information by id
     * @param number $pageId : This is page id
     * @return array $result : This is page information
     */
    function getPageInfo($pageId)
    {
        $this->db->select('pageId, title, description, createdDtm');
        $this->db->from('tbl_pages');
        $this->db->where('isDeleted', 0);
        $this->db->where('pageId', $pageId);
        $query = $this->db->get();

        return $query->result();
    }

}

