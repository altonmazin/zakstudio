<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Sliderimage_model extends CI_Model
{

    /**
     * This function is used to get the slider image listing count
     * @param number $sliderId : This is slider id
     * @return array $result : This is result
     */
    function sliderImageListing($sliderId)
    {
        $this->db->select('BaseTbl.imgId, BaseTbl.sliderId, BaseTbl.file_name, BaseTbl.imgOrder, BaseTbl.createdDtm');
        $this->db->from('tbl_header_slider_images as BaseTbl');

        $this->db->where('BaseTbl.sliderId', $sliderId);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }



    /**
     * This function is used to check whether sliderId is already exist or not
     * @param {number} $sliderId : This is slider id
     * @return {mixed} $result : This is searched result
     */
    function checkSliderImageExists($sliderId = 0)
    {
        $this->db->select('sliderId, file_name, imgOrder, createdDtm');
        $this->db->from("tbl_header_slider_images");
        $this->db->where("sliderId !=", $sliderId);
        $query = $this->db->get();

        return $query->result();
    }




    /**
     * This function used to get slider information by sliderId
     * @param number $sliderId : This is slider id
     * @return array $result : This is slider information
     */
    function getSliderImageInfo($imgId)
    {
        $this->db->select('imgId, sliderId, file_name, imgOrder, createdDtm');
        $this->db->from('tbl_header_slider_images');
        $this->db->where('imgId', $imgId);
        $query = $this->db->get();
        return $query->result();
    }


    /**
     * This function is used to update the slider information
     * @param array $sliderImageInfo : This is slider updated information
     * @param number $sliderId : This is slider id
     * @return boolean $result : TRUE / FALSE
     */
    function editSliderImage($sliderImageInfo, $imgId)
    {
        $this->db->trans_start();
        $this->db->where('imgId', $imgId);
        $this->db->update('tbl_header_slider_images', $sliderImageInfo);
        $this->db->trans_complete();
        //check transaction status
        if ($this->db->trans_status() === FALSE) {
            //rollback transaction
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //commit the transaction
            $this->db->trans_commit();
            return TRUE;
        }
    }

    /**
     * This function is used to update the slider information
     * @param array $sliderImageInfo : This is slider updated information
     * @param number $sliderId : This is slider id
     * @return boolean $result : TRUE / FALSE
     */
    function editSliderImageDetails($sliderImageInfo, $imgId)
    {
        if($imgId != '') {
            $this->db->where('imgId', $imgId);
            $this->db->update('tbl_header_slider_images', $sliderImageInfo);
            return true;
        } else {
            return false;
        }
    }

    /**
     * This function is used to delete the slider information
     * @param number $sliderId : This is slider id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteSliderImage($imgId, $sliderImageInfo)
    {
        $this->db->where('imgId', $imgId);
        $this->db->update('tbl_header_slider_images', $sliderImageInfo);

        return $this->db->affected_rows();
    }

}

