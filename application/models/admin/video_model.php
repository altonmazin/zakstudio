<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Video_model extends CI_Model
{
    /**
     * This function is used to get the video listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function videoListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.videoId, BaseTbl.title, BaseTbl.url');
        $this->db->from('tbl_videos as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.title  LIKE '%".$searchText."%'
                           OR  BaseTbl.url  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();

        return count($query->result());
    }

    /**
     * This function is used to get the video listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function videoListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.videoId, BaseTbl.title, BaseTbl.url, BaseTbl.createdDtm');
        $this->db->from('tbl_videos as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.title LIKE '%".$searchText."%'
                            OR  BaseTbl.url  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->limit($page, $segment);
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }

    /**
     * This function used to get video information by id
     * @return array $result : This is videp information
     */
    function getVideos()
    {
        $this->db->select('title, url');
        $this->db->from('tbl_videos');
        $this->db->where('isDeleted', 0);


        $query = $this->db->get();


        return $query->result();
    }

    /**
     * This function is used to check whether email id is already exist or not
     * @param {string} $email : This is email id
     * @param {number} $videoId : This is video id
     * @return {mixed} $result : This is searched result
     */
    function checkVideoExists($videoId = 0)
    {
        $this->db->select("title");
        $this->db->from("tbl_videos");
        $this->db->where("isDeleted", 0);
        $this->db->where("videoId !=", $videoId);

        $query = $this->db->get();

        return $query->result();
    }


    /**
     * This function is used to add new video to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewVideo($videoInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_videos', $videoInfo);

        $insert_id = $this->db->insert_id();

        $this->db->trans_complete();
        //check transaction status
        if ($this->db->trans_status() === FALSE) {

            //rollback transaction
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //commit the transaction
            $this->db->trans_commit();
            return TRUE;
        }


    }

    /**
     * This function used to get video information by id
     * @param number $videoId : This is video id
     * @return array $result : This is video information
     */
    function getVideoInfo($videoId)
    {
        $this->db->select('videoId, title, url, createdDtm');
        $this->db->from('tbl_videos');
        $this->db->where('isDeleted', 0);
        $this->db->where('videoId', $videoId);
        $query = $this->db->get();

        return $query->result();
    }


    /**
     * This function is used to update the video information
     * @param array $videoInfo : This is videos updated information
     * @param number $videoId : This is video id
     */
    function editVideo($videoInfo, $videoId)
    {
        $this->db->trans_start();
        $this->db->where('videoId', $videoId);
        $this->db->update('tbl_videos', $videoInfo);
        $this->db->trans_complete();
        //check transaction status
        if ($this->db->trans_status() === FALSE) {

            //rollback transaction
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //commit the transaction
            $this->db->trans_commit();
            return TRUE;
        }


    }

    /**
     * This function is used to update the video information
     * @param array $videoInfo : This is videos updated information
     * @param number $videoId : This is video id
     */
    function editVideoDetails($videoInfo, $videoId)
    {
        if($videoId != '') {
            $this->db->where('videoId', $videoId);
            $this->db->update('tbl_videos', $videoInfo);
            return true;
        } else {
            return false;
        }
    }

    /**
     * This function is used to delete the video information
     * @param number $videoId : This is video id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteVideo($videoId, $videoInfo)
    {
        $this->db->where('videoId', $videoId);
        $this->db->update('tbl_videos', $videoInfo);

        return $this->db->affected_rows();
    }

}

