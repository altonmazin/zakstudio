<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Instagram_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    /*function userListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.userId, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, Role.role');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get();

        return count($query->result());
    }*/

    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function instagramListing()
    {
        $this->db->select('BaseTbl.tagId, BaseTbl.tag');
        $this->db->from('tbl_instagramTags as BaseTbl');

        $this->db->where('BaseTbl.isDeleted', 0);

        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }




    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewInstagramTag($instagramInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_instagramTags', $instagramInfo);

        $insert_id = $this->db->insert_id();

        $this->db->trans_complete();

        return $insert_id;
    }

    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getInstagramInfo($tagInstagramId)
    {
        $this->db->select('tagId, tag');
        $this->db->from('tbl_instagramTags');
        $this->db->where('isDeleted', 0);

        $this->db->where('tagId', $tagInstagramId);
        $query = $this->db->get();

        return $query->result();
    }

    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getInstagramTag()
    {
        $this->db->select('tagId, tag');
        $this->db->from('tbl_instagramTags');
        $this->db->where('isDeleted', 0);
        $this->db->limit(1);


        $query = $this->db->get();


        return $query->result();
    }


    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function editInstagramTag($instagramTagInfo, $instagramTagId)
    {
        $this->db->where('tagId', $instagramTagId);
        $this->db->update('tbl_instagramTags', $instagramTagInfo);

        return TRUE;
    }

    /**
     * This function is used to delete the instagram information
     * @param number $tagId : This is tag id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteInstagramTag($instagramTagId, $instagramTagInfo)
    {

        $this->db->where('tagId', $instagramTagId);
        $this->db->where('isDeleted', 0);
        $this->db->update('tbl_instagramTags', $instagramTagInfo);

        return $this->db->affected_rows();
    }
}

